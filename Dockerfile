FROM maven:3.8.5-openjdk-17 AS build
RUN mkdir /project
COPY src /project/src
COPY pom.xml /project
RUN mvn -f /project/pom.xml package -DskipTests

FROM khipu/openjdk17-alpine:latest
RUN mkdir /app
COPY --from=build /project/target/furniture-market-be-0.0.1-SNAPSHOT.jar /app/furniture-market-be.jar
WORKDIR /app

RUN apk add --no-cache tzdata
ENV TZ=Asia/Ho_Chi_Minh

CMD java $JAVA_OPTS -jar furniture-market-be.jar

