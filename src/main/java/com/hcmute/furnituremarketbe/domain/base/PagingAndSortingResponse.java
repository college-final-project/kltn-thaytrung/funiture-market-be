package com.hcmute.furnituremarketbe.domain.base;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PagingAndSortingResponse<T> {
    List<T> content = new ArrayList<>();
    Integer currentPage;
    Integer pageSize;
    Integer totalRecords;
    Integer totalPages;
}
