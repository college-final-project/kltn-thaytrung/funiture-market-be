package com.hcmute.furnituremarketbe.domain.base;

import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.persistence.criteria.*;

import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ExtendEntityRepositoryBase<E> {
    private static final String INVALID_PARAMETER_MESSAGE = "Tham số không hợp lệ";

    private static final List<String> relatedFields = new ArrayList<>(List.of("category", "store", "product","buyer"));

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

    private static final Set<Class<?>> primitiveNumbers = Stream
            .of(int.class, long.class, float.class, double.class,
                    byte.class, short.class)
            .collect(Collectors.toSet());

    private static final Set<Class<?>> primitiveStrings = Stream
            .of(String.class, CharSequence.class)
            .collect(Collectors.toSet());

    private static final Set<Class<?>> primitiveDates = Stream
            .of(Date.class)
            .collect(Collectors.toSet());

    private static boolean isNumericType(Class<?> cls) {
        if (cls.isPrimitive()) {
            return primitiveNumbers.contains(cls);
        } else {
            return Number.class.isAssignableFrom(cls);
        }
    }

    private static boolean isListType(Class<?> cls) {
        return List.class.isAssignableFrom(cls);
    }

    private static boolean isStringType(Class<?> cls) {
        if (cls.isPrimitive()) {
            return primitiveStrings.contains(cls);
        } else {
            return CharSequence.class.isAssignableFrom(cls);
        }
    }

    private static boolean isBooleanType(Class<?> cls) {
        return cls == Boolean.class || cls == boolean.class;
    }

    private static boolean isDateType(Class<?> cls) {
        if (cls.isPrimitive()) {
            return primitiveDates.contains(cls);
        } else {
            return Date.class.isAssignableFrom(cls);
        }
    }

    private static Path retrievePath(Path root, String[] queryTokens, int length) {
        for (int i = 0; i < length; ++i) {
            root = root.get(queryTokens[i]);
        }
        return root;
    }

    private static boolean isRelatedField(String columnName) {
        return relatedFields.contains(columnName);
    }

    protected PagingAndSortingResponse<E> dynamicSearchEntity(
            EntityManager entityManager,
            Map<String, String> queries,
            CriteriaBuilder criteriaBuilder,
            CriteriaQuery<E> query,
            Root<E> root,
            Field[] fields
    ) {
        List<Predicate> predicates = new ArrayList<>();

        String sortColumn = "";
        String sortType = "";
        Integer currentPage = 0;
        Integer pageSize = 0;

        for (Map.Entry<String, String> pair : queries.entrySet()) {
            String key = pair.getKey();
            String value = pair.getValue();

            if (!(key.equals("currentPage") || key.equals("pageSize") || key.equals("sort"))) {
                String[] tokens = key.split("\\.");

                if (tokens.length < 2) {
                    throw ServiceExceptionFactory.badRequest()
                            .addMessage(INVALID_PARAMETER_MESSAGE);
                }

                String columnName = tokens[0];
                String columnFilter = tokens[tokens.length - 1];

                Optional<Field> optionalField = Arrays.stream(fields).filter(f -> f.getName().equals(columnName)).findFirst();

                if (optionalField.isEmpty()) {
                    System.out.println("not found field with name = " + columnName);
                    throw ServiceExceptionFactory.badRequest()
                            .addMessage(INVALID_PARAMETER_MESSAGE);
                }

                Field field = optionalField.get();

                switch (columnFilter) {
                    case "startsWith" -> {
                        if (!isStringType(field.getType()))
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);

                        Expression<String> expressionQueryValue = criteriaBuilder.literal(value + "%");
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(columnName)), criteriaBuilder.lower(expressionQueryValue)));
                    }
                    case "endsWith" -> {
                        if (!isStringType(field.getType()))
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);

                        Expression<String> expressionQueryValue = criteriaBuilder.literal("%" + value);
                        predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(columnName)), criteriaBuilder.lower(expressionQueryValue)));
                    }
                    case "equals" -> {
                        if (isRelatedField(columnName)) {
                            String[] values = value.split(",");
                            if (values.length != 2)
                                throw ServiceExceptionFactory.badRequest()
                                        .addMessage(INVALID_PARAMETER_MESSAGE);
                            Join<?, ?> join = root.join(columnName, JoinType.INNER);
                            Expression<String> expressionQueryValue = criteriaBuilder.literal(values[1]);
                            predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(join.get(values[0])), criteriaBuilder.lower(expressionQueryValue)));
                        }
                        else {
                            if (isStringType(field.getType())) {
                                Expression<String> expressionQueryValue = criteriaBuilder.literal(value);
                                predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(columnName)), criteriaBuilder.lower(expressionQueryValue)));
                            } else if (isNumericType(field.getType())) {
                                predicates.add(criteriaBuilder.equal(root.get(columnName), value));
                            } else if (isDateType(field.getType())) {
                                try {
                                    predicates.add(criteriaBuilder.equal(root.<Date>get(columnName), this.dateFormat.parse(value)));
                                } catch (ParseException e) {
                                    throw ServiceExceptionFactory.badRequest().withData(e);
                                }
                            } else if (field.getType().isEnum()) {
                                predicates.add(criteriaBuilder.equal(criteriaBuilder.lower(root.get(columnName)), value));
                            } else if (isBooleanType(field.getType())) {
                                boolean booleanValue = Boolean.parseBoolean(value);
                                predicates.add(criteriaBuilder.equal(root.get(columnName), booleanValue));
                            }
                            else {
                                Path path = retrievePath(root, tokens, tokens.length - 1);
                                predicates.add(criteriaBuilder.equal(path, value));
                            }
                        }
                    }
                    case "contains" -> {
                        if (isRelatedField(columnName)) {
                            String[] values = value.split(",");
                            if (values.length != 2)
                                throw ServiceExceptionFactory.badRequest()
                                        .addMessage(INVALID_PARAMETER_MESSAGE);
                            Join<?, ?> join = root.join(columnName, JoinType.INNER);
                            Expression<String> expressionQueryValue = criteriaBuilder.literal("%" + values[1] + "%");
                            predicates.add(criteriaBuilder.like(criteriaBuilder.lower(join.get(values[0])), criteriaBuilder.lower(expressionQueryValue)));
                        }
                        else {
                            if (isStringType(field.getType())) {
                                Expression<String> expressionQueryValue = criteriaBuilder.literal("%" + value + "%");
                                predicates.add(criteriaBuilder.like(criteriaBuilder.lower(root.get(columnName)), criteriaBuilder.lower(expressionQueryValue)));
                            }
                            else if (isListType(field.getType())) {
                                predicates.add(criteriaBuilder.like(
                                        criteriaBuilder.function("concat", String.class,
                                                criteriaBuilder.literal("%"),
                                                root.get(columnName),
                                                criteriaBuilder.literal("%")),
                                        "%" + value + "%"
                                ));
                            }
                            else
                                throw ServiceExceptionFactory.badRequest()
                                        .addMessage(INVALID_PARAMETER_MESSAGE);
                        }
                    }
                    case "min" -> {
                        if (isStringType(field.getType()))
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);

                        if (isNumericType(field.getType())) {
                            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(columnName), Double.parseDouble(value)));
                        }
                        else if (isDateType(field.getType())) {
                            try {
                                predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(columnName), this.dateFormat.parse(value)));
                            } catch (ParseException e) {
                                throw ServiceExceptionFactory.badRequest().withData(e);
                            }
                        } else {
                            predicates.add(criteriaBuilder.greaterThanOrEqualTo(root.get(columnName), new Date(value)));
                        }
                    }
                    case "max" -> {
                        if (isStringType(field.getType()))
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);

                        if (isNumericType(field.getType())) {
                            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(columnName), Double.parseDouble(value)));
                        }
                        else if (isDateType(field.getType())){
                            try {
                                predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(columnName), this.dateFormat.parse(value)));
                            } catch (ParseException e) {
                                throw ServiceExceptionFactory.badRequest().withData(e);
                            }
                        }
                        else {
                            predicates.add(criteriaBuilder.lessThanOrEqualTo(root.get(columnName), new Date(value)));
                        }
                    }
                    case "between" -> {
                        if (isStringType(field.getType()))
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);
                        if (isNumericType(field.getType())) {
                            String[] values = value.split(",");
                            if (values.length != 2) {
                                throw ServiceExceptionFactory.badRequest()
                                        .addMessage(INVALID_PARAMETER_MESSAGE);
                            }

                            predicates.add(criteriaBuilder.between(root.get(columnName), Double.parseDouble(values[0]), Double.parseDouble(values[1])));
                        }
                        else if (isDateType(field.getType())){
                            try {
                                String[] values = value.split(",");
                                if (values.length != 2) {
                                    throw ServiceExceptionFactory.badRequest()
                                            .addMessage(INVALID_PARAMETER_MESSAGE);
                                }

                                predicates.add(criteriaBuilder.between(root.get(columnName), this.dateFormat.parse(values[0]), this.dateFormat.parse(values[1])));
                            } catch (ParseException e) {
                                throw ServiceExceptionFactory.badRequest().withData(e);
                            }
                        }
                        else {
                            throw ServiceExceptionFactory.badRequest()
                                    .addMessage(INVALID_PARAMETER_MESSAGE);
                        }
                    }
                }
            }
            else {
                if (key.equals("currentPage"))
                    currentPage = Integer.parseInt(value);
                else if (key.equals("sort"))
                {
                    String[] sort = value.split("\\.");
                    sortColumn = sort[0];
                    sortType = sort[sort.length - 1];
                }
                else
                    pageSize = Integer.parseInt(value);
            }
        }

        currentPage = currentPage != 0 ? currentPage : 0;
        pageSize = pageSize != 0 ? pageSize : 12;

        query.where(criteriaBuilder.and(predicates.toArray(Predicate[]::new)));

        if (!sortType.equals("")) {
            if (sortType.equalsIgnoreCase("DESC"))
                query.orderBy(criteriaBuilder.desc(root.get(sortColumn)));
            else if (sortType.equalsIgnoreCase("ASC"))
                query.orderBy(criteriaBuilder.asc(root.get(sortColumn)));
            else
                throw ServiceExceptionFactory.badRequest()
                        .addMessage(INVALID_PARAMETER_MESSAGE);
        }

        TypedQuery<E> typedQuery = entityManager.createQuery(query);
        List<E> entityList1 = typedQuery.getResultList();

        Double pages = Double.valueOf(typedQuery.getResultList().size() / Double.valueOf(pageSize));
        Integer totalPages = pages.intValue();
        if (pages != totalPages.doubleValue())
            totalPages += 1;

        typedQuery.setFirstResult(currentPage * pageSize);
        typedQuery.setMaxResults(pageSize);

        List<E> entityList2 = typedQuery.getResultList();



        return new PagingAndSortingResponse<E>(entityList2,currentPage,pageSize,entityList1.size(),totalPages);
    }

}
