package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.SystemCategory;
import lombok.Data;

@Data
public class SystemCategoryResponse {
    private String id;
    private String name;
    private String image;

    public SystemCategoryResponse(SystemCategory category) {
        this.id = category.getId();
        this.name = category.getName();
        this.image = category.getImage();
    }
}
