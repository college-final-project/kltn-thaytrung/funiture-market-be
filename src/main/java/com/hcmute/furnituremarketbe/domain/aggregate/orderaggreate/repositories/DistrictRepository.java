package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.District;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DistrictRepository extends JpaRepository<District, Integer> {
    @Query("SELECT d FROM District d WHERE d.provinceCity.id=:id")
    List<District> getDistrictByProvinceId(@Param("id") Integer id);

    @Query("SELECT d FROM District d WHERE d.id=:id")
    District getDistrictById(@Param("id") Integer id);

    @Query("SELECT d FROM District d WHERE d.name=:name")
    District getDistrictByName(@Param("name") String name);
}
