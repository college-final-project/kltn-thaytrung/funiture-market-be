package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateRefundComplaintRequest;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface RefundComplaintService {
    ResponseBaseAbstract getAllRefundComplaints();

    ResponseBaseAbstract getRefundByDone(Boolean done);

    ResponseBaseAbstract createRefundComplaint(CreateRefundComplaintRequest request, List<MultipartFile> images);

    ResponseBaseAbstract markAsDone(String refundComplaintId);
}
