package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Data
public class RealTimeAnnounce {
    private String createdAt;
    private String message;

    public RealTimeAnnounce(ZonedDateTime createdAt, String message) {
        this.createdAt = zonedDateTimeToString(createdAt);
        this.message = message;
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
