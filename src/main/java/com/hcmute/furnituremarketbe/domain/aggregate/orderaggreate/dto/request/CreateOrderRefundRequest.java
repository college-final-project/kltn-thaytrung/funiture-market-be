package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

@Data
public class CreateOrderRefundRequest {
    private String reason;
    private String orderId;
}
