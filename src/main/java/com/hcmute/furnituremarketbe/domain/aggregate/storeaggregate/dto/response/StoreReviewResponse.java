package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import lombok.Data;

import java.util.Date;

@Data
public class StoreReviewResponse {
    private String id;
    private String content;
    private boolean isReply;
    private Integer star;
    private Date createdAt;
    private UserResponse reviewer;
    private ShortProductResponse productInfo;

    public StoreReviewResponse(StoreReview storeReview) {
        this.id = storeReview.getId();
        this.content = storeReview.getContent();
        this.star = storeReview.getStar();
        this.isReply = storeReview.getReply() != null;
        this.createdAt = storeReview.getCreatedAt();
    }
}
