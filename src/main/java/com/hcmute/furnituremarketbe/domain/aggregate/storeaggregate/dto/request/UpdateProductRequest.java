package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateProductRequest {
    @JsonIgnore
    private String id;
    @JsonIgnore
    private String storeId;
    private String name;
    private Double price;
    private Double salePrice;
    private String description;
    private Integer weight;
    private Integer height;
    private Integer length;
    private Integer width;
    private String material;
    private Integer inStock;
    private Boolean onSale;
    private Boolean onDisplay;
    private Boolean used;
    private Boolean featured;
    private String systemCategoryId;
    private String storeCategoryId;
}
