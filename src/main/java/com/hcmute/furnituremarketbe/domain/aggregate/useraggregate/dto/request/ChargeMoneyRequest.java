package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import lombok.Data;

@Data
public class ChargeMoneyRequest {
    private String vnpOtp;
    private long amount;
}
