package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.DeliveryAddress;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface DeliveryAddressRepository extends JpaRepository<DeliveryAddress, String> {
    @Query("SELECT d FROM DeliveryAddress d WHERE d.user.id=:id")
    List<DeliveryAddress> getAllDeliveryAddressByUserId(@Param("id") String id);

    @Query("SELECT d FROM DeliveryAddress d WHERE d.id=:id")
    DeliveryAddress getDeliveryAddressById(@Param("id") String id);
}
