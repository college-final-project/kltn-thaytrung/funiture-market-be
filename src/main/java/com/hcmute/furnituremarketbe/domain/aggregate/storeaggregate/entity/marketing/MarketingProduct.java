package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.MarketingPacket;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerFavouriteProduct;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class MarketingProduct {
    @Id
    private String id;

    @Enumerated(EnumType.STRING)
    private MarketingPacket packet;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "keywords", columnDefinition = "TEXT")
    private List<String> keywords = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "store_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Store store;

    @OneToMany(mappedBy = "marketingProduct", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Product> products;
}