package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class SystemCategory {
    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Column(name = "image")
    private String image;
}
