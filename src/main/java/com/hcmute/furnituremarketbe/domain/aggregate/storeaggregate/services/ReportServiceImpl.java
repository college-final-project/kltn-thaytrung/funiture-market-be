package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateReportStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ReportExplanationResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ReportResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ShortProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.ReportExplanation;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ReportExplanationRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ReportRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class ReportServiceImpl implements ReportService {
    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private ReportExplanationRepository reportExplanationRepository;

    @Autowired
    private AnnounceService announceService;

    @Override
    public ResponseBaseAbstract createReport(CreateReportRequest request) {
        Report report = new Report();
        report.setId(UUID.randomUUID().toString());
        report.setReason(request.getReason());
        report.setDescription(request.getDescription());
        report.setStatus(ReportStatus.PENDING);
        report.setReporterName(request.getReporterName());
        if (request.getType() == ReportType.PRODUCT_REPORT) {
            Product product = this.productRepository.getProductById(request.getObjectId());
            if (product == null)
                throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+request.getObjectId());
            report.setProduct(product);
        }
        else if (request.getType() == ReportType.STORE_REPORT) {
            Store store = this.storeRepository.getStoreById(request.getObjectId());
            if (store == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy cửa hàng với id = "+request.getObjectId());
            report.setStore(store);
        }
        else
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Loại báo cáo không hợp lệ");
        report.setType(request.getType());
        this.reportRepository.save(report);
        ReportResponse response = getReportResponse(report);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(response)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllReports() {
        List<Report> reports = this.reportRepository.findAll();
        List<ReportResponse> responses = new ArrayList<>();
        for (Report report : reports) {
            ReportResponse response = getReportResponse(report);
            responses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getReportById(String id) {
        Report report = this.reportRepository.getReportById(id);
        if (report == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy báo cáo với id = "+id);
        ReportResponse response = getReportResponse(report);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllStoreProductReport(User seller) {
        List<Report> reports = this.reportRepository.getReportByType(ReportType.PRODUCT_REPORT);
        List<Report> storeReports = reports.stream().filter(r -> {
            Product product = r.getProduct();
            Store store = product.getStore();
            if (store.getId().equals(seller.getId()))
                return true;
            else
                return false;
        }).toList();
        List<ReportResponse> responses = new ArrayList<>();
        for (Report report : storeReports) {
            ReportResponse response = getReportResponse(report);
            responses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateReportStatus(UpdateReportStatusRequest request) {
        String id = request.getId();
        Report report = this.reportRepository.getReportById(id);
        if (report == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy báo cáo với id = "+id);
        report.setStatus(request.getStatus());
        report.setUpdatedAt(new Date());
        this.reportRepository.save(report);
        if (report.getStatus() == ReportStatus.ACCEPTED) {
            String message = "Giải trình báo cáo của bạn không được chấp nhận, cửa hàng của bạn sẽ bị xử lý!";
            this.announceService.onDoneReport(report, message);
            if (report.getType() == ReportType.PRODUCT_REPORT) {
                Product product = report.getProduct();
                product.setHidden(true);
                this.productRepository.save(product);
            }
            if (report.getType() == ReportType.STORE_REPORT) {
                Store store = report.getStore();
                store.setQualified(false);
                this.storeRepository.save(store);
            }
        }
        if (report.getStatus() == ReportStatus.UNACCEPTED) {
            String message = "Giải trình báo cáo của bạn đã được chấp thuận, cảm ơn bạn đã hợp tác!";
            this.announceService.onDoneReport(report, message);
        }
        if (report.getStatus() == ReportStatus.PROCESSING)
            this.announceService.onProcessedReport(report);
        ReportResponse response = getReportResponse(report);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(response)
                .returnUpdated();
    }

    private ReportResponse getReportResponse(Report report) {
        ReportResponse response = new ReportResponse(report);
        if (report.getType() == ReportType.PRODUCT_REPORT) {
            ShortProductResponse productResponse = new ShortProductResponse(report.getProduct());
            productResponse.setThumbnail(this.fileStorageService.getImageUrl(productResponse.getName()));
            response.setObjectInfo(productResponse);
        }
        else {
            Store store = report.getStore();
            Map<String, String> objectInfo = new HashMap<>();
            objectInfo.put("id", store.getId());
            objectInfo.put("storeName", store.getShopName());
            objectInfo.put("logo", this.fileStorageService.getImageUrl(store.getLogo()));
            response.setObjectInfo(objectInfo);
        }
        List<ReportExplanation> reportExplanations = this.reportExplanationRepository.getReportExplanationByReportId(report.getId());
        List<ReportExplanationResponse> explanations = new ArrayList<>();
        for (ReportExplanation reportExplanation : reportExplanations) {
            ReportExplanationResponse explanationResponse = getReportExplanationResponse(reportExplanation);
            explanations.add(explanationResponse);
        }
        response.setExplanations(explanations);
        return response;
    }

    private ReportExplanationResponse getReportExplanationResponse(ReportExplanation reportExplanation) {
        ReportExplanationResponse explanationResponse = new ReportExplanationResponse(reportExplanation);
        List<String> images = explanationResponse.getImages();
        if (images.size() != 0) {
            List<String> imageUrls = images.stream().map(image -> this.fileStorageService.getImageUrl(image)).toList();
            explanationResponse.setImages(imageUrls);
        }
        return explanationResponse;
    }
}
