package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.WithdrawStatus;
import lombok.Data;

@Data
public class UpdateWithdrawMoneyStatus {
    @JsonIgnore
    private String id;
    private WithdrawStatus status;
}
