package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.*;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.*;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.PaymentType;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.*;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.OrderStoreResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.TransactionHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private VoucherUsageRepository voucherUsageRepository;

    @Autowired
    private VoucherOrderRepository voucherOrderRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private AnnounceService announceService;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    private final List<OrderStatus> orderStatuses = Arrays.stream(OrderStatus.values()).toList();

    @Override
    public ResponseBaseAbstract createOrder(User buyer, BuyerOrderRequest request) {
        List<Order> orders = new ArrayList<>();
        if (request.getPaymentType() == PaymentType.VIA_WALLET) {
            Double total = getTotalOfOrders(request.getOrderList());
            Wallet wallet = this.walletRepository.getByEmail(buyer.getEmail());
            if (wallet.getValue() < total)
                throw ServiceExceptionFactory.badRequest()
                    .addMessage("Ví không đủ tiền để thanh toán");
        }
        for (CreateOrderRequest req : request.getOrderList()) {
            Order order = saveSingleOrder(buyer, req, request.getDeliveryAddressId(), request.getPaymentType());
            orders.add(order);
        }
        List<OrderResponse> orderResponseList = getOrderResponses(orders);

        for (OrderResponse response : orderResponseList) {
            this.announceService.onUpdatedOrder(response);
        }

        return SuccessResponse.builder()
                .addMessage("Tạo đơn hàng thành công")
                .setData(orderResponseList)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllOrderByStore(String storeId) {
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+storeId);
        List<Order> orders = store.getOrders();
        List<SellerOrderResponse> orderResponseList = getSellerOrderResponses(orders);
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(orderResponseList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllOrder() {
        List<Order> orders = this.orderRepository.findAll();
        List<OrderResponse> orderResponseList = getOrderResponses(orders);
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(orderResponseList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchOrder(Map<String, String> queries) {
        PagingAndSortingResponse<Order> orderPage = this.orderRepository.searchOrder(queries);
        List<Order> orders = orderPage.getContent();
        List<OrderResponse> orderResponseList = getOrderResponses(orders);
        PagingAndSortingResponse<OrderResponse> data = new PagingAndSortingResponse<>(
                orderResponseList,
                orderPage.getCurrentPage(),
                orderPage.getPageSize(),
                orderPage.getTotalRecords(),
                orderPage.getTotalPages()
        );
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    private List<OrderResponse> getOrderResponses(List<Order> orders) {
        List<OrderResponse> orderResponseList = orders.stream().map(OrderResponse::new).toList();
        for (int i = 0;i < orders.size();i++) {
            List<OrderItem> list1 = this.orderItemRepository.getOrderItemByOrder(orders.get(i).getId());
            OrderStoreResponse storeInfo = new OrderStoreResponse(orders.get(i).getStore());
            storeInfo.setLogo(this.fileStorageService.getImageUrl(storeInfo.getLogo()));
            orderResponseList.get(i).setStoreInfo(storeInfo);
            if (list1 != null && list1.size() != 0) {
                List<OrderItemResponse> list2 = list1.stream().map(OrderItemResponse::new).toList();
                for (OrderItemResponse response : list2) {
                    response.setProductThumbnail(this.fileStorageService.getImageUrl(response.getProductThumbnail()));
                }
                orderResponseList.get(i).setResponses(list2);
            }
        }
        return orderResponseList;
    }

    private List<SellerOrderResponse> getSellerOrderResponses(List<Order> orders) {
        List<SellerOrderResponse> orderResponseList = orders.stream().map(SellerOrderResponse::new).toList();
        for (int i = 0;i < orders.size();i++) {
            List<OrderItem> list1 = this.orderItemRepository.getOrderItemByOrder(orders.get(i).getId());
            OrderStoreResponse storeInfo = new OrderStoreResponse(orders.get(i).getStore());
            storeInfo.setLogo(this.fileStorageService.getImageUrl(storeInfo.getLogo()));
            orderResponseList.get(i).setStoreInfo(storeInfo);
            BuyerDeliveryAddressResponse buyerInfo = new BuyerDeliveryAddressResponse(orders.get(i).getDeliveryAddress());
            orderResponseList.get(i).setBuyerInfo(buyerInfo);
            if (list1 != null && list1.size() != 0) {
                List<OrderItemResponse> list2 = list1.stream().map(OrderItemResponse::new).toList();
                for (OrderItemResponse response : list2) {
                    response.setProductThumbnail(this.fileStorageService.getImageUrl(response.getProductThumbnail()));
                }
                orderResponseList.get(i).setResponses(list2);
            }
        }
        return orderResponseList;
    }

    @Override
    public ResponseBaseAbstract getAllOrderByUserId(User user, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Order> orderPage = this.orderRepository.getOrderByUserId(user.getId(),pageable);
        List<Order> orderList = orderPage.getContent();
        List<OrderResponse> orderResponseList = getOrderResponses(orderList);
        PagingAndSortingResponse<OrderResponse> data =
                new PagingAndSortingResponse<>(orderResponseList, currentPage, pageSize, (int)orderPage.getTotalElements(), orderPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateOrderStatus(UpdateOrderStatusRequest request) {
        Order order = this.orderRepository.getOrderById(request.getOrderId());
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = "+request.getOrderId());

        if (order.getStatus() == OrderStatus.COMPLETED || order.getStatus() == OrderStatus.CANCELLED) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Không đổi trạng thái được nữa!");
        }
        else
        {
            order.setStatus(request.getStatus());
        }
        int firstIndex = this.orderStatuses.indexOf(request.getStatus());
        List<String> updateDates = order.getUpdatedAt();
        if (updateDates.size() >= 6) {
            updateDates.set(firstIndex, zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))));
            order.setUpdatedAt(updateDates);
        }

        if (request.getStatus() == OrderStatus.SHIPPING)
        {
            order.setPaid(true);
        }
        this.orderRepository.save(order);

        if (order.getStatus() == OrderStatus.COMPLETED) {
            Store store = order.getStore();
            User seller = this.userRepository.getUserById(store.getId());
            Wallet wallet = this.walletRepository.getByEmail(seller.getEmail());
            wallet.setValue(wallet.getValue() + (order.getTotal() - order.getVoucherDiscount())*0.97);
            this.walletRepository.save(wallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setValue((order.getTotal() - order.getVoucherDiscount())*0.97);
            history.setType(TransactionType.ORDER_INCOME);
            history.setWallet(wallet);
            this.transactionHistoryRepository.save(history);
        }

        List<OrderItem> orderItemList = order.getOrderItems();
        List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(OrderItemResponse::new).toList();
        OrderResponse response = new OrderResponse(order);
        response.setResponses(orderItemResponses);
        this.announceService.onUpdatedOrder(response);
        return SuccessResponse.builder()
                .addMessage("Cập nhật trạng thái đơn hàng thành công!")
                .setData(response)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getByOrderStatus(User user, OrderStatus status, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Order> orderPage = null;
        if (user.getRole() == UserRole.BUYER)
            orderPage = this.orderRepository.getBuyerOrderByStatus(status, user.getId(), pageable);
        else if (user.getRole() == UserRole.SELLER)
            orderPage = this.orderRepository.getStoreOrderByStatus(status, user.getId(), pageable);
        List<Order> orders = orderPage.getContent();
        List<OrderResponse> orderResponses = getOrderResponses(orders);
        PagingAndSortingResponse<OrderResponse> data =
                new PagingAndSortingResponse<>(orderResponses, currentPage, pageSize, (int)orderPage.getTotalElements(), orderPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getOrderDetail(String orderId) {
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + orderId);
        OrderDetailResponse response = new OrderDetailResponse(order);
        DeliveryAddress d = order.getDeliveryAddress();
        response.setDeliveryAddress(new BuyerDeliveryAddressResponse(d));
        List<OrderItem> orderItemList = order.getOrderItems();
        Double total = orderItemList.stream().mapToDouble(OrderItem::getTotal).sum();
        response.setTotal(total);
        List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(OrderItemResponse::new).toList();
        for (OrderItemResponse resp : orderItemResponses) {
            resp.setProductThumbnail(this.fileStorageService.getImageUrl(resp.getProductThumbnail()));
        }
        response.setOrderItemList(orderItemResponses);
        List<String> updateDates = order.getUpdatedAt();
        List<StatusWithDate> statusWithDates = new ArrayList<>();
        for (int i = 0;i<updateDates.size();i++) {
            StatusWithDate statusWithDate = new StatusWithDate(this.orderStatuses.get(i), updateDates.get(i));
            statusWithDates.add(statusWithDate);
        }
        response.setStatusWithDates(statusWithDates);

        OrderStoreResponse storeInfo = new OrderStoreResponse(order.getStore());
        storeInfo.setLogo(this.fileStorageService.getImageUrl(storeInfo.getLogo()));
        response.setStoreInfo(storeInfo);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract cancelOrder(String orderId) {
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + orderId);
        int firstIndex = this.orderStatuses.indexOf(order.getStatus());
        if (firstIndex >= 2)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Trạng thái của đơn hàng không hợp lệ");
        order.setStatus(OrderStatus.CANCELLED);
        List<String> updateDates = order.getUpdatedAt();
        if (updateDates.size() >= 6)
        {
            updateDates.set(4, zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))));
            order.setUpdatedAt(updateDates);
        }

        this.orderRepository.save(order);
        List<OrderItem> orderItemList = order.getOrderItems();
        List<OrderItemResponse> orderItemResponses = orderItemList.stream().map(OrderItemResponse::new).toList();
        for (OrderItem item : orderItemList) {
            Product product = item.getProduct();
            product.setSold(product.getSold() - item.getQuantity());
            product.setInStock(product.getInStock() + item.getQuantity());
            this.productRepository.save(product);
        }
        User buyer = order.getUser();
        List<Voucher> voucherList = this.voucherOrderRepository.getVoucherByOrder(orderId);
        if (voucherList != null && voucherList.size() != 0) {
            for (Voucher v : voucherList) {
                v.setTotalTimes(v.getTotalTimes() + 1);
                this.voucherRepository.save(v);
                VoucherUsage voucherUsage = this.voucherUsageRepository.getByVoucherIdAndBuyerId(v.getId(), buyer.getId());
                voucherUsage.setRemainingTimes(voucherUsage.getRemainingTimes() + 1);
                this.voucherUsageRepository.save(voucherUsage);
            }
        }
        // update buyer wallet
        Wallet buyerWallet = this.walletRepository.getByEmail(buyer.getEmail());
        if (buyerWallet != null && order.isPaid()) {
            buyerWallet.setValue(buyerWallet.getValue() + (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee()));
            this.walletRepository.save(buyerWallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setType(TransactionType.ORDER_CANCEL);
            history.setValue(order.getTotal() - order.getVoucherDiscount() + order.getShippingFee());
            history.setWallet(buyerWallet);
            this.transactionHistoryRepository.save(history);
        }

        // update store wallet
        Store store = order.getStore();
        User seller = this.userRepository.getUserById(store.getId());
        Wallet sellerWallet = this.walletRepository.getByEmail(seller.getEmail());
        if (sellerWallet != null) {
            sellerWallet.setValue(sellerWallet.getValue() - (order.getTotal() - order.getVoucherDiscount())*0.97);
            this.walletRepository.save(sellerWallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setType(TransactionType.ORDER_CANCEL);
            history.setValue((order.getTotal() - order.getVoucherDiscount())*0.97);
            history.setWallet(sellerWallet);
            this.transactionHistoryRepository.save(history);
        }


        OrderResponse response = new OrderResponse(order);
        response.setResponses(orderItemResponses);
        this.announceService.onUpdatedOrder(response);
        return SuccessResponse.builder()
                .addMessage("Hủy đơn hàng thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public void refundOrder(String orderId) {
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + orderId);
        if (order.getStatus() != OrderStatus.COMPLETED)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Trạng thái của đơn hàng không hợp lệ");
        order.setStatus(OrderStatus.REFUNDED);
        List<String> updateDates = order.getUpdatedAt();
        if (updateDates.size() > 6)
        {
            updateDates.set(6, zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))));
            order.setUpdatedAt(updateDates);
        }

        this.orderRepository.save(order);
        List<OrderItem> orderItemList = order.getOrderItems();
        for (OrderItem item : orderItemList) {
            Product product = item.getProduct();
            product.setSold(product.getSold() - item.getQuantity());
            product.setInStock(product.getInStock() + item.getQuantity());
            this.productRepository.save(product);
        }
        User buyer = order.getUser();
        List<Voucher> voucherList = this.voucherOrderRepository.getVoucherByOrder(orderId);
        if (voucherList != null && voucherList.size() != 0) {
            for (Voucher v : voucherList) {
                v.setTotalTimes(v.getTotalTimes() + 1);
                this.voucherRepository.save(v);
                VoucherUsage voucherUsage = this.voucherUsageRepository.getByVoucherIdAndBuyerId(v.getId(), buyer.getId());
                voucherUsage.setRemainingTimes(voucherUsage.getRemainingTimes() + 1);
                this.voucherUsageRepository.save(voucherUsage);
            }
        }
        Wallet buyerWallet = this.walletRepository.getByEmail(buyer.getEmail());
        if (buyerWallet != null && order.isPaid()) {
            buyerWallet.setValue(buyerWallet.getValue() + (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee()));
            this.walletRepository.save(buyerWallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setType(TransactionType.ORDER_REFUND);
            history.setValue(order.getTotal() - order.getVoucherDiscount() + order.getShippingFee());
            history.setWallet(buyerWallet);
            this.transactionHistoryRepository.save(history);
        }
        Store store = order.getStore();
        User seller = this.userRepository.getUserById(store.getId());
        Wallet sellerWallet = this.walletRepository.getByEmail(seller.getEmail());
        if (sellerWallet != null) {
            sellerWallet.setValue(sellerWallet.getValue() - (order.getTotal() - order.getVoucherDiscount())*0.97);
            this.walletRepository.save(sellerWallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setType(TransactionType.ORDER_REFUND);
            history.setValue((order.getTotal() - order.getVoucherDiscount())*0.97);
            history.setWallet(sellerWallet);
            this.transactionHistoryRepository.save(history);
        }
        OrderResponse response = new OrderResponse(order);
        this.announceService.onUpdatedOrder(response);
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }

    private Order saveSingleOrder(User buyer, CreateOrderRequest request, String deliveryAddressId, PaymentType paymentType) {
        // Save order
        Double total=0.0;
        for (CreateOrderItemRequest item : request.getOrderItemList()) {
            Product product = this.productRepository.getProductById(item.getProductId());
            if (product == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy sản phẩm với id = "+item.getProductId());
            Double price = product.getSalePrice();
            total += price * item.getQuantity();
        }
        List<String> updateDates = new ArrayList<>(List.of(
                zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))),
                zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))), " ", " ", " ", " ", " "));
        Order order = new Order();
        order.setId(UUID.randomUUID().toString());
        order.setUser(buyer);
        DeliveryAddress address = this.deliveryAddressRepository.getDeliveryAddressById(deliveryAddressId);
        order.setDeliveryAddress(address);
        order.setTotal(total);
        order.setShippingFee(Double.valueOf(request.getShippingFee()));
        order.setVoucherDiscount(request.getTotalDiscount());
        String storeId = request.getStoreId();
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+storeId);
        order.setStore(store);
        if (paymentType == PaymentType.VIA_WALLET) {
            order.setPaid(true);
            order.setStatus(OrderStatus.TO_SHIP);
        }
        else if (paymentType == PaymentType.COD) {
            order.setPaid(false);
            order.setStatus(OrderStatus.TO_SHIP);
        }
        else
        {
            order.setPaid(false);
            order.setStatus(OrderStatus.UNPAID);
            updateDates = new ArrayList<>(List.of(
                    zonedDateTimeToString(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"))),
                    " ", " ", " ", " ", " ", " "));
        }
        order.setUpdatedAt(updateDates);
        this.orderRepository.save(order);

        // Update money of buyer wallet
        if (paymentType == PaymentType.VIA_WALLET) {
            Wallet wallet = this.walletRepository.getByEmail(buyer.getEmail());
            if (wallet != null) {
                if ((wallet.getValue() - (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee())) < 0)
                    throw ServiceExceptionFactory.badRequest()
                        .addMessage("Số tiền trong ví không đủ để thanh toán");
                wallet.setValue(wallet.getValue() - (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee()));
                this.walletRepository.save(wallet);
                TransactionHistory history = new TransactionHistory();
                history.setId(UUID.randomUUID().toString());
                history.setType(TransactionType.PAY);
                history.setValue(order.getTotal() - order.getVoucherDiscount() + order.getShippingFee());
                history.setWallet(wallet);
                this.transactionHistoryRepository.save(history);
            }
        }

        // Save order item
        for (CreateOrderItemRequest item : request.getOrderItemList()) {
            OrderItem orderItem = new OrderItem();
            orderItem.setId(UUID.randomUUID().toString());
            // update product
            Product product = this.productRepository.getProductById(item.getProductId());
            product.setSold(product.getSold() + item.getQuantity());
            if (product.getInStock() <= 0) {
                product.setInStock(0);
                throw ServiceExceptionFactory.badRequest().addMessage("Sản phẩm đã hết hàng !");
            }
            product.setInStock(product.getInStock() - item.getQuantity());
            this.productRepository.save(product);

            orderItem.setProduct(product);
            orderItem.setQuantity(item.getQuantity());
            Double price = product.getSalePrice();
            orderItem.setTotal(item.getQuantity() * price);
            orderItem.setProductName(product.getName());
            orderItem.setProductThumbnail(product.getThumbnail());
            orderItem.setProductPrice(price);
            orderItem.setProductMaterial(product.getMaterial());
            orderItem.setOrder(order);
            this.orderItemRepository.save(orderItem);
        }

        // Update voucher usage, voucher order.
        if (request.getVoucherIdList() != null && request.getVoucherIdList().size() != 0) {
            for (String voucherId : request.getVoucherIdList()) {
                Voucher voucher = this.voucherRepository.getVoucherById(voucherId);
                if (voucher == null)
                    throw ServiceExceptionFactory.notFound()
                            .addMessage("Không tìm thấy voucher với id = "+voucherId);
                voucher.setTotalTimes(voucher.getTotalTimes()-1);
                this.voucherRepository.save(voucher);
                VoucherUsage voucherUsage = this.voucherUsageRepository.getByVoucherIdAndBuyerId(voucherId, buyer.getId());
                if (voucherUsage != null) {
                    voucherUsage.setRemainingTimes(voucherUsage.getRemainingTimes() - 1);
                    this.voucherUsageRepository.save(voucherUsage);
                }
                else {
                    VoucherUsage voucherUsageEntity = new VoucherUsage();
                    voucherUsageEntity.setId(UUID.randomUUID().toString());
                    voucherUsageEntity.setVoucher(voucher);
                    voucherUsageEntity.setBuyer(buyer);
                    voucherUsageEntity.setRemainingTimes(voucher.getBuyerLimit()-1);
                    this.voucherUsageRepository.save(voucherUsageEntity);
                }
                VoucherOrder voucherOrder = new VoucherOrder();
                voucherOrder.setId(UUID.randomUUID().toString());
                voucherOrder.setOrder(order);
                voucherOrder.setVoucher(voucher);
                this.voucherOrderRepository.save(voucherOrder);
            }
        }
        return order;
    }

    @Override
    public ResponseBaseAbstract payAddition(User buyer, String orderId) {
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = "+orderId);
        if (order.isPaid())
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Đơn hàng đã được thanh toán");
        else {
            order.setPaid(true);
            order.setStatus(OrderStatus.TO_SHIP);
            this.orderRepository.save(order);
        }
        Wallet wallet = this.walletRepository.getByEmail(buyer.getEmail());
        if (wallet != null) {
            if ((wallet.getValue() - (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee())) < 0)
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Số tiền trong ví không đủ để thanh toán");
            wallet.setValue(wallet.getValue() - (order.getTotal() - order.getVoucherDiscount() + order.getShippingFee()));
            this.walletRepository.save(wallet);
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setType(TransactionType.PAY);
            history.setValue(order.getTotal() - order.getVoucherDiscount() + order.getShippingFee());
            history.setWallet(wallet);
            this.transactionHistoryRepository.save(history);
        }
        return SuccessResponse.builder()
                .addMessage("Thanh toán bổ sung thành công")
                .returnUpdated();
    }

    private Double getTotalOfOrders(List<CreateOrderRequest> orderList) {
        Double finalTotal = 0.0;
        for (CreateOrderRequest o : orderList) {
            Double total = 0.0;
            for (CreateOrderItemRequest item : o.getOrderItemList()) {
                Product product = this.productRepository.getProductById(item.getProductId());
                if (product == null)
                    throw ServiceExceptionFactory.notFound()
                            .addMessage("Không tìm thấy sản phẩm với id = "+item.getProductId());
                Double price = product.getSalePrice();
                total += price * item.getQuantity();
            }
            total = total + o.getShippingFee() - o.getTotalDiscount();
            finalTotal += total;
        }
        return finalTotal;
    }
}
