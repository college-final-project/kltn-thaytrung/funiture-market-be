package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateOrderRequest {
    private String storeId;
    private List<CreateOrderItemRequest> orderItemList = new ArrayList<>();
    private Double totalDiscount;
    private Long shippingFee;
    private List<String> voucherIdList = new ArrayList<>();
}