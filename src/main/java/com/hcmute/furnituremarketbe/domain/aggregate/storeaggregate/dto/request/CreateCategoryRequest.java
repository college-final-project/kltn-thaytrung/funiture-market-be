package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class CreateCategoryRequest {
    private String name;
}
