package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums;

public enum MarketingPacket {
    BASIC,
    MEDIUM,
    PREMIUM
}
