package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.CreateWalletRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.ChargeMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.WithdrawMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface WalletService {
    ResponseBaseAbstract chargeMoney(User user, ChargeMoneyRequest request);

    ResponseBaseAbstract createWallet(User user, CreateWalletRequest request);

    ResponseBaseAbstract getWallet(User user);
}
