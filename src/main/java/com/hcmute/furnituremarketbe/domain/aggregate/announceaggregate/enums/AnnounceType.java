package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums;

public enum AnnounceType {
    VOUCHER,
    ORDER,
    ACCOUNT,
    REPORT
}
