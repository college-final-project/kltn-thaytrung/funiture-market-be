package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.entity.Message;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.enums.ChatMessageOneToOneType;
import lombok.Data;

import java.text.SimpleDateFormat;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Data
public class ChatMessageResponse {
    private String senderId;
    private String receiverId;
    private String content;

    private String messageId;
    private ChatMessageOneToOneType type;
    private boolean isRead;
    private String createdAt;

    public ChatMessageResponse(String senderId, String receiverId, Message message) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.messageId = message.getId();
        this.content = message.getContent();
        this.createdAt = zonedDateTimeToString(message.getCreatedAt());
        this.isRead = message.getIsRead();
        if (message.getContent().startsWith("img:")) {
            this.type = ChatMessageOneToOneType.IMAGE;
            this.content = message.getContent().replaceFirst("img:", "");
        } else {
            this.type = ChatMessageOneToOneType.MESSAGE;
        }
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
