package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class ReportExplanation {
    @Id
    private String id;

    @Column(name = "created_at")
    private Date createdAt = new Date();

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "images", columnDefinition = "TEXT")
    private List<String> images = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "report_id")
    private Report report;
}
