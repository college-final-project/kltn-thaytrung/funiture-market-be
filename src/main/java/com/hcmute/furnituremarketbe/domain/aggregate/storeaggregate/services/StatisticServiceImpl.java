package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;


import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderItem;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.*;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.MarketingProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StatisticService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class StatisticServiceImpl implements StatisticService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private MarketingProductService marketingProductService;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseBaseAbstract getStoreStatistic(String storeId, Integer month, Integer year) {
        StatisticResponse data = new StatisticResponse();
        List<NumOfOrderByDayResponse> responses = new ArrayList<>();
        List<IncomeByDayResponse> responses1 = new ArrayList<>();
        if (month != null && year != null) {
            Double incomeOfShippingOrders = 0.0;
            Double income = 0.0;
            Double incomeOfCompletedOrders = 0.0;
            Date firstDate = getFirstDateOfMonth(year, month);
            Date lastDate = getLastDateOfMonth(year, month);
            data.setNumOfOrderByMonth(this.orderRepository.getNumOfOrderByMonth(lastDate, firstDate, storeId));
            Date currentDate = new Date();
            int currentMonth = getMonthFromDateUsingLocalDate(currentDate);
            List<Date> dates;
            List<Order> orderList;
            if (month == currentMonth){
                dates = getDatesBetween(firstDate, currentDate);
                orderList = this.orderRepository.getOrderByMonth(currentDate,firstDate, storeId);
            }
            else {
                dates = getDatesBetween(firstDate, lastDate);
                orderList = this.orderRepository.getOrderByMonth(lastDate,firstDate, storeId);
            }
            for (Date date : dates) {
                Double incomeByDay=0.0;
                NumOfOrderByDayResponse response = new NumOfOrderByDayResponse();
                Integer amount = this.orderRepository.getNumOfOrderByDay(date, storeId);
                response.setDate(date);
                response.setAmount(amount);
                responses.add(response);

                IncomeByDayResponse response1 = new IncomeByDayResponse();
                List<Order> orders = this.orderRepository.getOrderByDay(date, storeId);
                List<Order> completedOrders = orders.stream()
                        .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED))
                        .toList();
                if (completedOrders.size() != 0)
                    incomeByDay = completedOrders.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount())*0.97).sum();
                response1.setDate(date);
                response1.setIncomeByDay(incomeByDay);
                responses1.add(response1);
            }
            data.setNumOfOrderByDays(responses);
            data.setIncomeByDays(responses1);

            if (orderList.size() > 0)
                income = orderList.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount())*0.97).sum();

            List<Order> shippingOrdersByMonth = orderList.stream()
                    .filter(order -> order.getStatus().equals(OrderStatus.SHIPPING))
                    .toList();
            if (shippingOrdersByMonth.size() > 0)
                incomeOfShippingOrders = shippingOrdersByMonth.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount())*0.97).sum();

            List<Order> completedOrdersByMonth = orderList.stream()
                    .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED))
                    .toList();
            if (completedOrdersByMonth.size() != 0) {
                Map<String, Integer> productStatistic = new HashMap<>();
                List<SoldByProductResponse> soldByProductResponses = new ArrayList<>();
                incomeOfCompletedOrders = completedOrdersByMonth.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount())*0.97).sum();
                for (Order o : completedOrdersByMonth) {
                    List<OrderItem> orderItemList = o.getOrderItems();
                    for (OrderItem orderItem : orderItemList) {
                        productStatistic.putIfAbsent(orderItem.getProduct().getId(),0);
                    }
                    for (OrderItem orderItem : orderItemList) {
                        String productId = orderItem.getProduct().getId();
                        Integer soldCount = productStatistic.get(productId);
                        soldCount += orderItem.getQuantity();
                        productStatistic.replace(productId, soldCount);
                    }
                }
                for (Map.Entry<String, Integer> entry : productStatistic.entrySet()) {
                    String productId = entry.getKey();
                    Integer sold = entry.getValue();
                    Product product = this.productRepository.getProductById(productId);
                    assert product != null;
                    String productName = product.getName();
                    SoldByProductResponse soldByProductResponse = new SoldByProductResponse(productId, productName, sold);
                    soldByProductResponses.add(soldByProductResponse);
                }
                String maxSoldProductId = findMaxKey(productStatistic);
                Product maxSoldProductByMonth = this.productRepository.getProductById(maxSoldProductId);
                ShortProductResponse productOfMonth = new ShortProductResponse(maxSoldProductByMonth);
                productOfMonth.setThumbnail(this.fileStorageService.getImageUrl(productOfMonth.getThumbnail()));
                data.setProductOfTheMonth(productOfMonth);
                data.setSoldByProductResponses(soldByProductResponses);
            }
            else {
                data.setProductOfTheMonth(null);
                data.setSoldByProductResponses(new ArrayList<>());
            }
            data.setNumOfShippingOrderByMonth(shippingOrdersByMonth.size());
            data.setNumOfCompletedOrderByMonth(completedOrdersByMonth.size());
            data.setIncomeOfShippingOrders(incomeOfShippingOrders);
            data.setIncomeOfCompletedOrders(incomeOfCompletedOrders);
            data.setTotalIncome(income);
            Integer numberOfSoldProduct = 0;
            for (Order order : completedOrdersByMonth) {
                List<OrderItem> orderItemList = order.getOrderItems();
                if (orderItemList.size() != 0)
                {
                    Integer totalQuantity = orderItemList.stream().mapToInt(OrderItem::getQuantity).sum();
                    numberOfSoldProduct += totalQuantity;
                }
            }
            data.setSoldProduct(numberOfSoldProduct);
        }
        else {
            data.setTotalIncome(0.0);
            data.setIncomeOfShippingOrders(0.0);
            data.setNumOfOrderByDays(responses);
            data.setIncomeByDays(responses1);
            data.setNumOfOrderByMonth(0);
            data.setSoldProduct(0);
            data.setNumOfShippingOrderByMonth(0);
            data.setNumOfCompletedOrderByMonth(0);
            data.setSoldByProductResponses(new ArrayList<>());
            data.setProductOfTheMonth(null);
        }

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreOrderIncome(String storeId, Integer month, Integer year) {
        List<StoreOrderIncomeResponse> data = new ArrayList<>();
        if (month != null && year != null) {
            Date firstDate = getFirstDateOfMonth(year, month);
            Date lastDate = getLastDateOfMonth(year, month);
            Date currentDate = new Date();
            int currentMonth = getMonthFromDateUsingLocalDate(currentDate);
            List<Order> orderList;
            if (month == currentMonth){
                orderList = this.orderRepository.getOrderByMonth(currentDate,firstDate, storeId);
            }
            else {
                orderList = this.orderRepository.getOrderByMonth(lastDate,firstDate, storeId);
            }
            List<Order> completedOrdersByMonth = orderList.stream()
                    .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED))
                    .toList();
            for (Order order : completedOrdersByMonth) {
                StoreOrderIncomeResponse response = new StoreOrderIncomeResponse(order);
                data.add(response);
            }
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAdminStatistic(Integer month, Integer year) {
        AdminStatisticResponse response = new AdminStatisticResponse();
        Double orderIncome = 0.0;
        List<NumOfOrderByDayResponse> orderByDays = new ArrayList<>();
        List<IncomeByDayResponse> incomeByDays = new ArrayList<>();
        Integer numOfBuyer = this.userRepository.getUserByRole(UserRole.BUYER).size();
        Integer numOfStore = this.userRepository.getUserByRole(UserRole.SELLER).size();
        response.setNumOfBuyer(numOfBuyer);
        response.setNumOfStore(numOfStore);
        MarketingStatisticResponse marketing = this.marketingProductService.getMarketingProductStatistic(month, year);
        response.setMarketing(marketing);
        if (month != null && year != null) {
            Date firstDate = getFirstDateOfMonth(year, month);
            Date lastDate = getLastDateOfMonth(year, month);
            response.setNumOfOrderByMonth(this.orderRepository.getAdminNumOfOrderByMonth(lastDate, firstDate));
            Date currentDate = new Date();
            int currentMonth = getMonthFromDateUsingLocalDate(currentDate);
            List<Date> dates;
            List<Order> orderList;
            if (month == currentMonth){
                dates = getDatesBetween(firstDate, currentDate);
                orderList = this.orderRepository.getAdminOrderByMonth(currentDate,firstDate);
            }
            else {
                dates = getDatesBetween(firstDate, lastDate);
                orderList = this.orderRepository.getAdminOrderByMonth(lastDate,firstDate);
            }
            for (Date date : dates) {
                Double income = 0.0;
                NumOfOrderByDayResponse numOfOrderByDay = new NumOfOrderByDayResponse();
                Integer amount = this.orderRepository.getAdminNumOfOrderByDay(date);
                numOfOrderByDay.setDate(date);
                numOfOrderByDay.setAmount(amount);
                orderByDays.add(numOfOrderByDay);

                IncomeByDayResponse incomeByDay = new IncomeByDayResponse();
                List<Order> orders = this.orderRepository.getAdminOrderByDay(date);
                List<Order> completedOrders = orders.stream()
                        .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED))
                        .toList();
                if (completedOrders.size() != 0)
                    income = completedOrders.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount()) * 0.03).sum();
                incomeByDay.setIncomeByDay(income);
                incomeByDay.setDate(date);
                incomeByDays.add(incomeByDay);
            }
            response.setOrderByDays(orderByDays);
            response.setIncomeByDays(incomeByDays);

            List<Order> completedOrdersByMonth = orderList.stream()
                    .filter(order -> order.getStatus().equals(OrderStatus.COMPLETED))
                    .toList();
            if (completedOrdersByMonth.size() != 0) {
                orderIncome = completedOrdersByMonth.stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount()) * 0.03).sum();
            }
            response.setOrderIncome(orderIncome);
        }
        else {
            response.setOrderIncome(0.0);
            response.setOrderByDays(orderByDays);
            response.setIncomeByDays(incomeByDays);
            response.setNumOfOrderByMonth(0);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }


    private static Date getFirstDateOfMonth(int year, int month) {
        LocalDate localDate = LocalDate.of(year, month, 1);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private static Date getLastDateOfMonth(int year, int month) {
        LocalDate lastDayOfMonth = LocalDate.of(year, month, 1)
                .plusMonths(1)
                .minusDays(1);
        return Date.from(lastDayOfMonth.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static List<Date> getDatesBetween(Date startDate, Date endDate) {
        List<Date> dates = new ArrayList<>();
        LocalDate startLocalDate = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endLocalDate = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        while (!startLocalDate.isAfter(endLocalDate)) {
            dates.add(java.sql.Date.valueOf(startLocalDate));
            startLocalDate = startLocalDate.plusDays(1);
        }
        return dates;
    }

    private static int getMonthFromDateUsingLocalDate(Date date) {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.getMonthValue();
    }

    private static String findMaxKey(Map<String, Integer> map) {
        String maxKey = "";
        Integer maxValue = Integer.MIN_VALUE;
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            Integer currentValue = entry.getValue();
            if (currentValue > maxValue) {
                maxValue = currentValue;
                maxKey = entry.getKey();
            }
        }
        return maxKey;
    }

}
