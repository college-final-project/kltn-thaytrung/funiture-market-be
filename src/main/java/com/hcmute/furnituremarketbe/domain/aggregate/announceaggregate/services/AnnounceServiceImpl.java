package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto.*;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.entity.Announce;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.repositories.AnnounceRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.OrderResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.VoucherResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ReportResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.WithdrawalResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Withdrawal;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.WithdrawStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.ConnectionRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class AnnounceServiceImpl implements AnnounceService {
    @Autowired
    private AnnounceRepository announceRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Override
    public void onUpdatedOrder(OrderResponse orderResponse) {
        String id = orderResponse.getId();
        Order order = this.orderRepository.getOrderById(id);
        String message = "";
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy đơn hàng với id = " + id);
        switch (orderResponse.getStatus()) {
            case UNPAID -> {
                message =  String.format("Đơn hàng \"%s\" của bạn đã được tạo, vui lòng thanh toán",id);
            }
            case TO_SHIP -> {
                message = String.format("Đơn hàng \"%s\" của bạn đã sẵn sàng để giao",id);
            }
            case SHIPPING -> {
                message = String.format("Đơn hàng \"%s\" của bạn đang được vận chuyển",id);
            }
            case COMPLETED -> {
                message = String.format("Đơn hàng \"%s\" của bạn đã được giao",id);
            }
            case CANCELLED -> {
                message = String.format("Đơn hàng \"%s\" của bạn đã được hủy",id);
            }
            case FAILED_DELIVERY -> {
                message = String.format("Đơn hàng \"%s\" giao thất bại",id);
            }
            case REFUNDED -> {
                message = String.format("Đơn hàng \"%s\" đã được hoàn tiền",id);
            }
        }
        User user = order.getUser();
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.ORDER);
        announce.setContent(new ArrayList<>(List.of(id,message)));
        announce.setUser(user);
        this.announceRepository.save(announce);

        try {
            String endPoint = "/ws/secured/announce/user/" + user.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public ResponseBaseAbstract getAnnounceByType(User user, AnnounceType type, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Announce> announcePage = this.announceRepository.getUserAnnounceByType(type, user.getId(), pageable);
        List<Announce> announces = announcePage.getContent();
        List<AnnounceResponse> responses = announces.stream().map(AnnounceResponse::new).toList();
        PagingAndSortingResponse<AnnounceResponse> data =
                new PagingAndSortingResponse<>(responses, currentPage, pageSize, (int)announcePage.getTotalElements(), announcePage.getTotalPages());
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract markAsRead(String announceId) {
        Announce announce = this.announceRepository.getAnnounceById(announceId);
        if (announce == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy thông báo với id = " + announceId);
        announce.setSeen(true);
        this.announceRepository.save(announce);
        return SuccessResponse.builder()
                .addMessage("Đánh dấu đã đọc thông báo thành công")
                .setData(new AnnounceResponse(announce))
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract markAsReadForAll(User user) {
        Pageable pageable = PageRequest.of(0,999);
        List<Announce> announces = this.announceRepository.getAnnounceByUser(user.getId(), pageable).getContent();
        for (Announce announce : announces) {
            announce.setSeen(true);
            this.announceRepository.save(announce);
        }
        List<AnnounceResponse> responses = announces.stream().map(AnnounceResponse::new).toList();
        return SuccessResponse.builder()
                .addMessage("Đánh dấu đã đọc tất cả thông báo thành công")
                .setData(responses)
                .returnUpdated();
    }

    @Override
    public void onCreatedVoucher(VoucherResponse voucherResponse, Store store) {
        List<User> users = this.userRepository.findAll();
        List<User> filterList = users.stream().filter(u -> this.connectionRepository.existConnection(store.getId(), u.getId())).toList();
        String message = "Bạn vừa nhận được mã giảm giá từ cửa hàng " + store.getShopName();
        for (User user : filterList) {
            String id = voucherResponse.getId();
            Announce announce = new Announce();
            announce.setId(UUID.randomUUID().toString());
            announce.setSeen(false);
            announce.setType(AnnounceType.VOUCHER);
            announce.setContent(new ArrayList<>(List.of(id,message)));
            announce.setUser(user);
            this.announceRepository.save(announce);
            try {
                String endPoint = "/ws/secured/announce/user/" + user.getId();
                RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
                this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onUpdatedUserAccount(User user, String message) {
        String id = user.getId();
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.ACCOUNT);
        announce.setContent(new ArrayList<>(List.of(id, message)));
        announce.setUser(user);
        this.announceRepository.save(announce);
        try {
            String endPoint = "/ws/secured/announce/user/" + user.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProcessedReport(Report report) {
        Store store = report.getStore();
        Product product = report.getProduct();
        if (store == null)
            store = product.getStore();
        User seller = this.userRepository.getUserById(store.getId());
        String message = "";
        if (report.getType() == ReportType.PRODUCT_REPORT)
            message = "Bạn nhận được yêu cầu giải trình do sản phẩm của cửa hàng bạn bị báo cáo!";
        else
            message = "Bạn nhận được yêu cầu giải trình do cửa hàng bạn bị báo cáo!";
        String id = report.getId();
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.REPORT);
        announce.setContent(new ArrayList<>(List.of(id, message)));
        announce.setUser(seller);
        this.announceRepository.save(announce);
        try {
            String endPoint = "/ws/secured/announce/user/" + seller.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDoneReport(Report report, String message) {
        Store store = report.getStore();
        Product product = report.getProduct();
        if (store == null)
            store = product.getStore();
        User seller = this.userRepository.getUserById(store.getId());
        String id = report.getId();
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.REPORT);
        announce.setContent(new ArrayList<>(List.of(id, message)));
        announce.setUser(seller);
        this.announceRepository.save(announce);
        try {
            String endPoint = "/ws/secured/announce/user/" + seller.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpdatedWithdrawal(Withdrawal withdrawal) {
        Wallet wallet = withdrawal.getWallet();
        User user = wallet.getUser();
        String id = withdrawal.getId();
        String message = withdrawal.getStatus() == WithdrawStatus.DONE
                ? "Tiền đã rút thành công về tài khoản của bạn"
                : "Yêu cầu rút tiền thất bại, vui lòng kiểm tra các thông tin đã cung cấp!";
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.ACCOUNT);
        announce.setContent(new ArrayList<>(List.of(id, message)));
        announce.setUser(user);
        this.announceRepository.save(announce);
        try {
            String endPoint = "/ws/secured/announce/user/" + user.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onDeclineOrderRefund(OrderRefund refund) {
        Order order = refund.getOrder();
        String id = order.getId();
        String message = String.format("Yêu cầu hoàn tiền đơn hàng \"%s\" bị từ chối",id);
        User user = order.getUser();
        Announce announce = new Announce();
        announce.setId(UUID.randomUUID().toString());
        announce.setSeen(false);
        announce.setType(AnnounceType.ORDER);
        announce.setContent(new ArrayList<>(List.of(id,message)));
        announce.setUser(user);
        this.announceRepository.save(announce);
        try {
            String endPoint = "/ws/secured/announce/user/" + user.getId();
            RealTimeAnnounce realTimeAnnounce = new RealTimeAnnounce(announce.getCreatedAt(), message);
            this.simpMessagingTemplate.convertAndSend(endPoint, realTimeAnnounce);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}
