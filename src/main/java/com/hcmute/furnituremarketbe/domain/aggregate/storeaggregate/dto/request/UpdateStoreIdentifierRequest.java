package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateStoreIdentifierRequest {
    private String identifierCode;
    private String identifierFullName;
    private MultipartFile identifierFrontImg;
    private MultipartFile identifierBackImg;
}
