package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.ReportExplanation;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ReportExplanationResponse {
    private String id;
    private String description;
    private List<String> images;
    private String reportId;
    private Date createdAt;

    public ReportExplanationResponse(ReportExplanation reportExplanation) {
        this.id = reportExplanation.getId();
        this.description = reportExplanation.getDescription();
        this.images = reportExplanation.getImages();
        this.reportId = reportExplanation.getReport() != null ? reportExplanation.getReport().getId() : null;
        this.createdAt = reportExplanation.getCreatedAt();
    }
}
