package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend.ProductExtendRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product, String>, ProductExtendRepository {
    @Query("SELECT p FROM Product p WHERE p.category.name=:name")
    Page<Product> getProductByCategoryName(@Param("name") String name, Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.id=:id")
    Product getProductById(@Param("id") String id);

    @Query("SELECT p FROM Product p WHERE p.isFeatured = true")
    Page<Product> getFeaturedProductPage(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.name=:name")
    Product getProductByName(@Param("name") String name);

    @Query("SELECT p FROM Product p WHERE p.sold>1000 AND p.hidden=false")
    Page<Product> getMostBuyingProducts(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE SIZE(p.buyerFavouriteProducts) >= 4 AND p.hidden = false")
    Page<Product> getMostFavouriteProducts(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE p.marketingProduct.id=:id")
    List<Product> getProductsByMarketingId(@Param("id") String id);

    @Query(value = "SELECT * FROM product ORDER BY RAND()", nativeQuery = true)
    Page<Product> fetchRandomProduct(Pageable pageable);

    @Query("SELECT p FROM Product p WHERE FUNCTION('DATE', p.marketingProduct.startDate) <= FUNCTION('DATE', :givenDate) AND FUNCTION('DATE', p.marketingProduct.endDate) >= FUNCTION('DATE', :givenDate) AND p.hidden=false")
    Page<Product> findProductsByMarketing(@Param("givenDate") Date givenDate, Pageable pageable);
}
