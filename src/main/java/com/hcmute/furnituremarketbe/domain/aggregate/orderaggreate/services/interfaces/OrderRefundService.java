package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateOrderRefundRequest;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface OrderRefundService {
    ResponseBaseAbstract getOrderRefundByStore(String storeId);

    ResponseBaseAbstract getStoreOrderRefundByAccepted(String storeId, Boolean accepted);

    ResponseBaseAbstract createOrderRefund(CreateOrderRefundRequest request, List<MultipartFile> images);

    ResponseBaseAbstract doneOrderRefund(String orderRefundId, Boolean accepted);
}
