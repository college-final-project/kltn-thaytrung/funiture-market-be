package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.ConnectionResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Connection;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.ConnectionRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.ConnectionService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class ConnectionServiceImpl implements ConnectionService {
    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseBaseAbstract getFollowers(String userId) {
        User user = this.userRepository.getUserById(userId);
        if (user == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy người dùng với id = "+userId);
        List<User> followerList = this.connectionRepository.getFollowers(userId);
        List<UserResponse> responses = followerList.stream().map(follower -> new UserResponse(follower)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getFollowingPeople(String userId) {
        User user = this.userRepository.getUserById(userId);
        if (user == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy người dùng với id = "+userId);
        List<User> followingPeopleList = this.connectionRepository.getFollowingPeople(userId);
        List<UserResponse> responses = followingPeopleList.stream().map(followingPeople -> new UserResponse(followingPeople)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    // id is id of person who you want to follow
    @Override
    public ResponseBaseAbstract toggle(User user, String id) {
        User toFollowPerson = this.userRepository.getUserById(id);
        if (toFollowPerson == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy người dùng với id = "+id);
        Connection existConnection = this.connectionRepository.getConnectionByUserAndFollower(id, user.getId());
        if (existConnection != null) {
            existConnection.setDeletedAt(new Date());
            this.connectionRepository.save(existConnection);
            return SuccessResponse.builder()
                    .addMessage("Unfollow thành công")
                    .returnUpdated();
        }
        else {
            Connection connection = new Connection();
            connection.setId(UUID.randomUUID().toString());
            connection.setFollower(user);
            connection.setUser(toFollowPerson);
            this.connectionRepository.save(connection);
            return SuccessResponse.builder()
                    .addMessage("Follow thành công")
                    .setData(new ConnectionResponse(connection))
                    .returnCreated();
        }
    }
}
