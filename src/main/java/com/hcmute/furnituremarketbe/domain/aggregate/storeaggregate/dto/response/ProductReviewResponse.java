package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import lombok.Data;

import java.util.Date;

@Data
public class ProductReviewResponse {
    private String id;
    private String content;
    private String reply;
    private Integer star;
    private Date createdAt;
    private String storeName;
    private UserResponse reviewer;

    public ProductReviewResponse(StoreReview storeReview) {
        this.id = storeReview.getId();
        this.content = storeReview.getContent();
        this.reply = storeReview.getReply();
        this.star = storeReview.getStar();
        this.createdAt = storeReview.getCreatedAt();
        this.storeName = storeReview.getStore().getShopName();
    }
}
