package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import lombok.Data;

import java.util.Date;

@Data
public class UserResponse {

    private String id;
    private String fullName;
    private String email;
    private String phone;
    private String avatar;
    private Date birthday;
    private boolean active = true;
    private UserGender gender;
    private UserRole role;
    private boolean emailConfirmed;

    public UserResponse(User user) {
        this.id = user.getId();
        this.fullName = user.getFullName();
        this.email = user.getEmail();
        this.phone = user.getPhone();
        this.birthday = user.getBirthday();
        this.avatar = user.getAvatar();
        this.active = user.getActive();
        this.gender = user.getGender();
        this.role = user.getRole();
        this.emailConfirmed = user.getEmailConfirmed() != null ? user.getEmailConfirmed() : false;
    }
}
