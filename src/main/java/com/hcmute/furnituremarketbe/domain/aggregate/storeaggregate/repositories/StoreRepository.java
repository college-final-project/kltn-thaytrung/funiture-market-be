package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend.StoreExtendRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface StoreRepository extends JpaRepository<Store, String>, StoreExtendRepository {
    @Query("SELECT s FROM Store s WHERE s.id=:id")
    Store getStoreById(@Param("id") String id);
}
