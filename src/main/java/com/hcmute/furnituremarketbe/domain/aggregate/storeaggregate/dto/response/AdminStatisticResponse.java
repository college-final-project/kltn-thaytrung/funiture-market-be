package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class AdminStatisticResponse {
    private Double orderIncome;
    private Integer numOfOrderByMonth;
    private List<NumOfOrderByDayResponse> orderByDays;
    private List<IncomeByDayResponse> incomeByDays;
    private Integer numOfBuyer;
    private Integer numOfStore;
    private MarketingStatisticResponse marketing;
}
