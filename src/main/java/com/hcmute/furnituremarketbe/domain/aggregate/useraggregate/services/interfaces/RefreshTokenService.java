package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.TokenResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;

public interface RefreshTokenService {
    TokenResponse generateLoginTokens(User user);

    TokenResponse renewToken(String refreshTokenId);

    void deleteTokenWhenLogout(String refreshToken);
}
