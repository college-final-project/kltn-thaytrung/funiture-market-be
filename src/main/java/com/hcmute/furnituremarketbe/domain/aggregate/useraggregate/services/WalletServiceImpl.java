package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.cache.VnpOtpCache;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.CreateWalletRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.ChargeMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.WithdrawMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.WalletResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.TransactionHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.WalletService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class WalletServiceImpl implements WalletService {
    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private VnpOtpCache vnpOtpCache;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Override
    public ResponseBaseAbstract chargeMoney(User user, ChargeMoneyRequest request) {
        String email = user.getEmail();
        Wallet wallet = this.walletRepository.getByEmail(email);
        if (wallet == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tồn tại người dùng hoặc người dùng chưa tạo ví");
        String vnpOtp = request.getVnpOtp();
        long amount = request.getAmount();

        if (vnpOtp == null)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Trường otp trống");

        if (!vnpOtp.equals(this.vnpOtpCache.getOtpCode(email))) {
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");
        }

        wallet.setValue(wallet.getValue() + amount);
        this.walletRepository.save(wallet);

        this.vnpOtpCache.delete(email);

        TransactionHistory history = new TransactionHistory();
        history.setId(UUID.randomUUID().toString());
        history.setType(TransactionType.CHARGE);
        history.setValue((double)amount);
        history.setWallet(wallet);
        this.transactionHistoryRepository.save(history);

        return SuccessResponse.builder()
                .addMessage("Nạp tiền vào ví thành công")
                .setData(new WalletResponse(wallet))
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract createWallet(User user, CreateWalletRequest request) {
        Wallet userWallet = this.walletRepository.getByEmail(user.getEmail());
        if (userWallet != null)
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Người dùng đã có ví");

        Wallet wallet = new Wallet();
        wallet.setId(UUID.randomUUID().toString());
        wallet.setValue(request.getAmount() != null ? request.getAmount() : 0);
        wallet.setUser(user);
        this.walletRepository.save(wallet);
        if (request.getAmount() == null)
            return SuccessResponse.builder()
                    .addMessage("Tạo ví thành công, vui lòng nạp tiền vào ví")
                    .setData(new WalletResponse(wallet))
                    .returnCreated();
        return SuccessResponse.builder()
                .addMessage("Tạo ví thành công")
                .setData(new WalletResponse(wallet))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getWallet(User user) {
        Wallet wallet = this.walletRepository.getByEmail(user.getEmail());
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(wallet != null ? new WalletResponse(wallet) : null)
                .returnGetOK();
    }
}
