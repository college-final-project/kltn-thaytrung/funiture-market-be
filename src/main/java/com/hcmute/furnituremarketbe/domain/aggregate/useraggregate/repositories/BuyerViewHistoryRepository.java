package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerViewHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BuyerViewHistoryRepository extends JpaRepository<BuyerViewHistory, String> {
    @Query("SELECT b FROM BuyerViewHistory b WHERE b.buyer.id=:buyerId AND b.product.id=:productId")
    BuyerViewHistory getBuyerViewHistoryByBuyerIdAndProductId(@Param("buyerId") String buyerId, @Param("productId") String productId);

    @Query("SELECT b FROM BuyerViewHistory b WHERE b.product.id=:id")
    List<BuyerViewHistory> getViewHistoryByProductId(@Param("id") String id);
}
