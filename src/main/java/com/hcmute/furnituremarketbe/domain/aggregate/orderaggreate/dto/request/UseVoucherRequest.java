package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class UseVoucherRequest {
    private String productId;
    private Double total;
    private List<String> voucherIds = new ArrayList<>();
}
