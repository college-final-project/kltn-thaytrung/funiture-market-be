package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums;

public enum UserGender {
    MALE,
    FEMALE
}
