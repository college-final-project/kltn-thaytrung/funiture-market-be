package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class StoreExtendRepositoryImpl extends ExtendEntityRepositoryBase<Store> implements StoreExtendRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<Store> searchStore(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Store> criteriaQuery = criteriaBuilder.createQuery(Store.class);
        Root<Store> storeRoot = criteriaQuery.from(Store.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                storeRoot,
                Store.class.getDeclaredFields()
        );
    }
}
