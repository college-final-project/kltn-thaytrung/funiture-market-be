package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

@Data
public class CreateOrderItemRequest {
    private String productId;
    private Integer quantity;
}
