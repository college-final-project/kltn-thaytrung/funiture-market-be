package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
public class ProductReport {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
}
