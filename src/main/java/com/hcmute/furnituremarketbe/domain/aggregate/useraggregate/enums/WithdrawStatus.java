package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums;

public enum WithdrawStatus {
    PROCESSING,
    DONE,
    FAIL
}
