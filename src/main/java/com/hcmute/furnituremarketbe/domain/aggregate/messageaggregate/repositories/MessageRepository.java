package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.entity.Message;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MessageRepository extends JpaRepository<Message, String> {
    @Query("SELECT m FROM Message m WHERE m.id=:id AND m.deletedAt is null")
    Message getMessageById(@Param("id") String id);

    @Query("SELECT m FROM Message m WHERE ((m.sender.id=:senderId AND m.receiver.id=:receiverId) OR (m.sender.id=:receiverId AND m.receiver.id=:senderId)) AND m.deletedAt is null")
    List<Message> getAllMessagesBetweenTwoPeople(@Param("senderId") String senderId, @Param("receiverId") String receiverId);

    @Query("SELECT DISTINCT u FROM Message m INNER JOIN User u ON (m.sender.id = u.id OR m.receiver.id = u.id) WHERE ((m.sender.id = :userId  OR m.receiver.id = :userId) AND u.id <> :userId AND  m.deletedAt is null )")
    List<User> getAllOpponents(@Param("userId") String userId);
}
