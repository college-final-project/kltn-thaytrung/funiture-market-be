package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class OrderExtendRepositoryImpl extends ExtendEntityRepositoryBase<Order> implements OrderExtendRepository {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<Order> searchOrder(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Order> criteriaQuery = criteriaBuilder.createQuery(Order.class);
        Root<Order> orderRoot = criteriaQuery.from(Order.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                orderRoot,
                Order.class.getDeclaredFields()
        );
    }
}
