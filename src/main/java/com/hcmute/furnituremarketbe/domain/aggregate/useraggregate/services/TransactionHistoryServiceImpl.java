package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.TransactionHistoryResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.TransactionHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.TransactionHistoryService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TransactionHistoryServiceImpl implements TransactionHistoryService {
    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Override
    public ResponseBaseAbstract getAllTransactionHistory() {
        List<TransactionHistory> transactionHistoryList = this.transactionHistoryRepository.findAll();
        List<TransactionHistoryResponse> data = new ArrayList<>();
        for (TransactionHistory history : transactionHistoryList) {
            Wallet wallet = history.getWallet();
            User user = wallet.getUser();
            TransactionHistoryResponse response = new TransactionHistoryResponse(history, user);
            data.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllTransactionHistoryByUser(User user) {
        Wallet wallet = this.walletRepository.getByEmail(user.getEmail());
        if (wallet == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tồn tại người dùng hoặc người dùng chưa tạo ví");
        List<TransactionHistory> transactionHistoryList = this.transactionHistoryRepository.getTransactionHistoryByWallet(wallet.getId());
        List<TransactionHistoryResponse> data = transactionHistoryList.stream().map(t -> new TransactionHistoryResponse(t, user)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }
}
