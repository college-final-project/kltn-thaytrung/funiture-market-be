package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Data
@Table(name = "voucher_product")
public class VoucherProduct {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "voucher_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Voucher voucher;

    @ManyToOne
    @JoinColumn(name = "product_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Product product;
}
