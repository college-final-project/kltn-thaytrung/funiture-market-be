package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateMarketingProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.IncomeByDayResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.MarketingProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.MarketingStatisticResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.MarketingPacket;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.MarketingProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.MarketingProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.TransactionHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Service
public class MarketingProductServiceImpl implements MarketingProductService {
    @Autowired
    private MarketingProductRepository marketingProductRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Override
    public ResponseBaseAbstract createMarketingProduct(User seller, CreateMarketingProductRequest request) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        if (request.getProductIds().size() == 0 || request.getProductIds() == null || request.getProductIds().size() > 2) {
            throw ServiceExceptionFactory.badRequest().addMessage("Số lượng sản phẩm đăng ký marketing không hợp lệ");
        }

        Double total = getTotalOfMarketing(request);
        Wallet wallet = this.walletRepository.getByEmail(seller.getEmail());
        if (wallet.getValue() < total)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Ví không đủ tiền để thanh toán");

        MarketingProduct marketingProduct = new MarketingProduct();
        marketingProduct.setId(UUID.randomUUID().toString());
        marketingProduct.setStartDate(new Date());
        MarketingPacket packet = request.getPacket();
        switch (packet) {
            case BASIC -> marketingProduct.setEndDate(plusToGivenDate(marketingProduct.getStartDate(), 3));
            case MEDIUM -> marketingProduct.setEndDate(plusToGivenDate(marketingProduct.getStartDate(), 5));
            case PREMIUM -> marketingProduct.setEndDate(plusToGivenDate(marketingProduct.getStartDate(), 7));
            default -> throw ServiceExceptionFactory.badRequest().addMessage("Gói marketing không hợp lệ");
        }
        marketingProduct.setKeywords(request.getKeywords());
        marketingProduct.setPacket(packet);
        marketingProduct.setStore(store);
        this.marketingProductRepository.save(marketingProduct);

        for (String productId : request.getProductIds()) {
            Product product = this.productRepository.getProductById(productId);
            if (product == null)
                throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+productId);
            product.setMarketingProduct(marketingProduct);
            this.productRepository.save(product);
        }
        MarketingProductResponse response = new MarketingProductResponse(marketingProduct);
        List<Product> applyProducts = this.productRepository.getProductsByMarketingId(marketingProduct.getId());


        wallet.setValue(wallet.getValue() - total);
        this.walletRepository.save(wallet);
        TransactionHistory history = new TransactionHistory();
        history.setId(UUID.randomUUID().toString());
        history.setType(TransactionType.PAY);
        history.setValue(total);
        history.setWallet(wallet);
        this.transactionHistoryRepository.save(history);
        List<String> productIds = applyProducts.stream().map(Product::getId).toList();
        response.setProductIds(productIds);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(response)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllMarketingProduct(Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<MarketingProduct> productPage = this.marketingProductRepository.findAll(pageable);
        List<MarketingProduct> products = productPage.getContent();
        List<MarketingProductResponse> productResponses = new ArrayList<>();
        for (int i = 0;i < products.size();i++) {
            MarketingProductResponse response = new MarketingProductResponse(products.get(i));
            List<String> productIds = products.get(i).getProducts().stream().map(Product::getId).toList();
            response.setProductIds(productIds);
            productResponses.add(response);
        }
        PagingAndSortingResponse<MarketingProductResponse> data = new PagingAndSortingResponse<>(productResponses,currentPage, pageSize, productResponses.size(), productPage.getTotalPages());
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreMarketingProducts(User seller) {
        List<MarketingProduct> marketingProducts = this.marketingProductRepository.getMarketingProductByStore(seller.getId());
        List<MarketingProductResponse> productResponses = new ArrayList<>();
        for (int i = 0;i < marketingProducts.size();i++) {
            MarketingProductResponse response = new MarketingProductResponse(marketingProducts.get(i));
            List<String> productIds = marketingProducts.get(i).getProducts().stream().map(Product::getId).toList();
            response.setProductIds(productIds);
            productResponses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(productResponses)
                .returnGetOK();
    }

    @Override
    public MarketingStatisticResponse getMarketingProductStatistic(Integer month, Integer year) {
        MarketingStatisticResponse response = new MarketingStatisticResponse();
        List<IncomeByDayResponse> incomeByDays = new ArrayList<>();
        if (month != null && year != null) {
            Double income = 0.0;
            Date firstDate = getFirstDateOfMonth(year, month);
            Date lastDate = getLastDateOfMonth(year, month);
            Date currentDate = new Date();
            int currentMonth = getMonthFromDateUsingLocalDate(currentDate);
            List<Date> dates;
            List<MarketingProduct> products;
            if (month == currentMonth) {
                dates = getDatesBetween(firstDate, currentDate);
                products = this.marketingProductRepository.getMarketingProductOfMonth(currentDate, firstDate);
            }
            else {
                dates = getDatesBetween(firstDate, lastDate);
                products = this.marketingProductRepository.getMarketingProductOfMonth(lastDate, firstDate);
            }
            income = getIncomeOfProducts(products);
            for (Date date : dates) {
                Double incomeByDay = 0.0;
                IncomeByDayResponse incomeByDayResponse = new IncomeByDayResponse();
                List<MarketingProduct> productsOfDay = this.marketingProductRepository.getMarketingProductByDay(date);
                incomeByDay = getIncomeOfProducts(productsOfDay);
                incomeByDayResponse.setIncomeByDay(incomeByDay);
                incomeByDayResponse.setDate(date);
                incomeByDays.add(incomeByDayResponse);
            }
            response.setIncome(income);
            response.setIncomeByDays(incomeByDays);
        }
        else {
            response.setIncome(0.0);
            response.setIncomeByDays(incomeByDays);
        }
        return response;
    }

    private Double getTotalOfMarketing(CreateMarketingProductRequest request) {
        List<Product> applyProducts = new ArrayList<>();
        for (String productId : request.getProductIds()) {
            Product product = this.productRepository.getProductById(productId);
            if (product == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy sản phẩm với id = "+productId);
            applyProducts.add(product);
        }
        Double value = 0.0;
        if (request.getPacket() == MarketingPacket.BASIC)
            value = 300000.0;
        else if (request.getPacket() == MarketingPacket.MEDIUM)
            value = 500000.0;
        else if (request.getPacket() == MarketingPacket.PREMIUM)
            value = 700000.0;
        return value * applyProducts.size();
    }

    private Double getIncomeOfProducts(List<MarketingProduct> products) {
        Double income = products.stream().mapToDouble(p -> {
            Double price;
            if (p.getPacket() == MarketingPacket.BASIC)
                price = 300000.0;
            else if(p.getPacket() == MarketingPacket.MEDIUM)
                price = 500000.0;
            else
                price = 700000.0;
            return price * p.getProducts().size();
        }).sum();
        return  income;
    }

    private static Date getFirstDateOfMonth(int year, int month) {
        LocalDate localDate = LocalDate.of(year, month, 1);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    private static Date getLastDateOfMonth(int year, int month) {
        LocalDate lastDayOfMonth = LocalDate.of(year, month, 1)
                .plusMonths(1)
                .minusDays(1);
        return Date.from(lastDayOfMonth.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static List<Date> getDatesBetween(Date startDate, Date endDate) {
        List<Date> dates = new ArrayList<>();
        LocalDate startLocalDate = startDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        LocalDate endLocalDate = endDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        while (!startLocalDate.isAfter(endLocalDate)) {
            dates.add(java.sql.Date.valueOf(startLocalDate));
            startLocalDate = startLocalDate.plusDays(1);
        }
        return dates;
    }

    private static int getMonthFromDateUsingLocalDate(Date date) {
        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
        return localDate.getMonthValue();
    }

    private Date plusToGivenDate(Date givenDate, Integer numDays) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(givenDate);
        calendar.add(Calendar.DAY_OF_MONTH, numDays);
        Date newDate = calendar.getTime();
        return newDate;
    }
}
