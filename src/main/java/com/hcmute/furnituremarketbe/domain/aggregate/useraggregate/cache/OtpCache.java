package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionBase;
import org.springframework.http.HttpStatus;

import java.util.SplittableRandom;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class OtpCache {
    private int otpLength;

    LoadingCache<String, String> loadingCache;

    public OtpCache(long expiredAfterMins, int otpLength) {
        loadingCache = CacheBuilder
                .newBuilder()
                .expireAfterWrite(expiredAfterMins, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return null;
                    }
                });
        this.otpLength = otpLength;
    }

    public String create(String email) {
        String otp = this.generateOtp(otpLength);
        loadingCache.put(email, otp);
        return otp;
    }

    public String getOtpCode(String email) {
        try {
            var otp = loadingCache.get(email);
            if (otp != null)
                return otp;
            else
                return "";
        }catch (ExecutionException e) {
            throw new ServiceExceptionBase(HttpStatus.INTERNAL_SERVER_ERROR).addMessage(e.getMessage());
        }
    }

    public static String generateOtp(int length) {
        StringBuilder generatedOTP = new StringBuilder();
        SplittableRandom splittableRandom = new SplittableRandom();

        for (int i = 0; i < length; i++) {

            int randomNumber = splittableRandom.nextInt(0, 9);
            generatedOTP.append(randomNumber);
        }
        return generatedOTP.toString();
    }

    public void delete(String email) {
        loadingCache.invalidate(email);
    }
}
