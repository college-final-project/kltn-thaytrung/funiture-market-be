package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateSystemCategoryRequest {
    @JsonIgnore
    private String id;
    private String name;
    private MultipartFile image;
}
