package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdateWithdrawMoneyStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.WithdrawMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.WithdrawalResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Withdrawal;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.WithdrawStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.TransactionHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WithdrawalRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.WithdrawalService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class WithdrawalServiceImpl implements WithdrawalService {
    @Autowired
    private WithdrawalRepository withdrawalRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private TransactionHistoryRepository transactionHistoryRepository;

    @Autowired
    private AnnounceService announceService;

    @Override
    public ResponseBaseAbstract getAllWithdrawal() {
        List<Withdrawal> withdrawals = this.withdrawalRepository.findAll();
        List<WithdrawalResponse> responses = withdrawals.stream().map(WithdrawalResponse::new).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createWithdrawMoneyRequest(User user, WithdrawMoneyRequest request) {
        Wallet wallet = this.walletRepository.getByEmail(user.getEmail());
        Withdrawal withdrawal = new Withdrawal();
        withdrawal.setId(UUID.randomUUID().toString());
        if (wallet.getValue() < request.getValue())
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Ví của bạn hiện tại không đủ để rút số tiền trên");
        withdrawal.setValue(request.getValue());
        withdrawal.setAccountNumber(request.getAccountNumber());
        withdrawal.setBankName(request.getBankName());
        withdrawal.setOwnerName(request.getOwnerName());
        withdrawal.setStatus(WithdrawStatus.PROCESSING);
        withdrawal.setWallet(wallet);
        this.withdrawalRepository.save(withdrawal);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(new WithdrawalResponse(withdrawal))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract updateWithdrawalStatus(UpdateWithdrawMoneyStatus request) {
        Withdrawal withdrawal = this.withdrawalRepository.getWithdrawalById(request.getId());
        if (withdrawal == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy yêu cầu rút tiền với id ="+request.getId());
        withdrawal.setStatus(request.getStatus());
        withdrawal.setUpdatedAt(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
        this.withdrawalRepository.save(withdrawal);
        if (request.getStatus() == WithdrawStatus.DONE) {
            Wallet wallet = withdrawal.getWallet();
            wallet.setValue(wallet.getValue() - withdrawal.getValue());
            TransactionHistory history = new TransactionHistory();
            history.setId(UUID.randomUUID().toString());
            history.setValue(withdrawal.getValue());
            history.setType(TransactionType.WITHDRAW);
            history.setWallet(wallet);
            this.transactionHistoryRepository.save(history);
            this.announceService.onUpdatedWithdrawal(withdrawal);
        }
        if (request.getStatus() == WithdrawStatus.FAIL) {
            this.announceService.onUpdatedWithdrawal(withdrawal);
        }
        return SuccessResponse.builder()
                .addMessage("Cập nhật trạng thái yêu cầu thành công")
                .setData(new WithdrawalResponse(withdrawal))
                .returnUpdated();
    }
}
