package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import lombok.Data;

@Data
public class ShortProductResponse {
    private String id;
    private String name;
    private String thumbnail;

    public ShortProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.thumbnail = product.getThumbnail();
    }
}
