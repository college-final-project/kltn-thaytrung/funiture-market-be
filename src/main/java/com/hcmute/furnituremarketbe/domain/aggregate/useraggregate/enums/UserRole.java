package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums;

public enum UserRole {
    ADMIN,
    ADMIN_ORDER,
    ADMIN_REPORT,
    ADMIN_CS,
    ADMIN_MARKETING,
    BUYER,
    SELLER
}
