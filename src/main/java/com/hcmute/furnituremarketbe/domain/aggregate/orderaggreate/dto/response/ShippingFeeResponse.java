package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import lombok.Data;

@Data
public class ShippingFeeResponse {
    private Long fee;

    public ShippingFeeResponse(Long fee) {
        this.fee = fee;
    }
}
