package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface VoucherRepository extends JpaRepository<Voucher, String> {
    @Query("SELECT v FROM Voucher v WHERE v.id=:id")
    Voucher getVoucherById(@Param("id") String id);

    @Query("SELECT v FROM Voucher v WHERE" +
            " FUNCTION('DATE', v.startDate) > FUNCTION('DATE', :givenDate) OR FUNCTION('DATE', v.endDate) < FUNCTION('DATE', :givenDate)")
    List<Voucher> getExpiredVoucher(@Param("givenDate") Date givenDate);
}
