package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class ShippingFeeCalculateRequest {
    private String deliveryAddressId;
    private List<OrderItemRequest> orderItemRequests = new ArrayList<>();
}
