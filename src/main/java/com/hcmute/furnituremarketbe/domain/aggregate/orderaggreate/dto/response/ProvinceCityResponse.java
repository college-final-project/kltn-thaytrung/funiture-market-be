package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.ProvinceCity;
import lombok.Data;

@Data
public class ProvinceCityResponse {
    private Integer id;
    private String name;
    private Integer ghnProvinceId;

    public ProvinceCityResponse(ProvinceCity p) {
        this.id = p.getId();
        this.name = p.getName();
        this.ghnProvinceId = p.getGhnProvinceId();
    }
}
