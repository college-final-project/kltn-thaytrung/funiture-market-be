package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "buyer_favourite_product")
public class BuyerFavouriteProduct {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    private User buyer;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;
}
