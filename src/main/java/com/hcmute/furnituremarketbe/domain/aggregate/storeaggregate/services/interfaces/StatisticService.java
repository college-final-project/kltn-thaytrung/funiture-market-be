package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;


public interface StatisticService {
    ResponseBaseAbstract getStoreStatistic(String storeId, Integer month, Integer year);

    ResponseBaseAbstract getStoreOrderIncome(String storeId, Integer month, Integer year);

    ResponseBaseAbstract getAdminStatistic(Integer month, Integer year);
}
