package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class StatisticResponse {
    private Integer soldProduct;
    private Double totalIncome;
    private Integer numOfOrderByMonth;
    private Integer numOfCompletedOrderByMonth;
    private Integer numOfShippingOrderByMonth;
    private Double incomeOfShippingOrders;
    private Double incomeOfCompletedOrders;
    private ShortProductResponse productOfTheMonth;
    private List<SoldByProductResponse> soldByProductResponses;
    private List<NumOfOrderByDayResponse> numOfOrderByDays;
    private List<IncomeByDayResponse> incomeByDays;
}
