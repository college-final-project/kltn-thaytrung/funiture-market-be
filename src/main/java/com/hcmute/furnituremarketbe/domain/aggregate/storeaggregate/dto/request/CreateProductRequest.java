package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;
import net.minidev.json.annotate.JsonIgnore;

@Data
public class CreateProductRequest {
    private String name;
    private Double price;
    private Double salePrice;
    private String description;
    private Integer weight;
    private Integer height;
    private Integer length;
    private Integer width;
    private String material;
    private Integer inStock;
    private boolean featured;
    private boolean used;
    private String systemCategoryId;
    private String storeCategoryId;
    @JsonIgnore
    private String storeId;
}
