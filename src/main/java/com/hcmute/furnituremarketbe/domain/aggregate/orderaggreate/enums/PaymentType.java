package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums;

public enum PaymentType {
    VIA_WALLET,
    COD
}
