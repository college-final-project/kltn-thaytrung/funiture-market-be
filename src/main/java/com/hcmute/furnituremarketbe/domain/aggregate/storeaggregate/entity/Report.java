package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class Report {
    @Id
    private String id;

    @Column(name = "created_at")
    private Date createdAt = new Date();

    @Column(name = "updated_at")
    private Date updatedAt = new Date();

    @Column(name = "deleted_at")
    private Date deletedAt;

    @Column(name = "reason", columnDefinition = "TEXT")
    private String reason;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Enumerated(EnumType.STRING)
    private ReportStatus status;

    @Enumerated(EnumType.STRING)
    private ReportType type;

    @Column(name = "reporter_name")
    private String reporterName;

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "store_id")
    private Store store;
}
