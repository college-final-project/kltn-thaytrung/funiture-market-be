package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Date;
import java.util.UUID;

@Entity
@Data
public class RefreshToken {
    @Id
    private String id;

    @Column(name = "family_id")
    private String familyId;

    @Column(name = "activated")
    private boolean activated;

    @Column(name = "expiration_date")
    private Date expirationDate;

    @ManyToOne
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;
}
