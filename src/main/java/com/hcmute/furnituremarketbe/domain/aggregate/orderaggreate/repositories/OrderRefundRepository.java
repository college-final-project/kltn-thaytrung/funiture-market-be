package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderRefundRepository extends JpaRepository<OrderRefund, String> {
    @Query("SELECT o FROM OrderRefund o WHERE o.id=:id")
    OrderRefund getOrderRefundById(@Param("id") String id);

    @Query("SELECT orf FROM OrderRefund orf JOIN orf.order o WHERE o.store.id=:id")
    List<OrderRefund> findByStoreId(@Param("id") String id);

    @Query("SELECT orf FROM OrderRefund orf JOIN orf.order o WHERE o.store.id=:id AND orf.accepted=:accepted")
    List<OrderRefund> getStoreOrderRefundByAccepted(@Param("id") String id, @Param("accepted") boolean accepted);
}
