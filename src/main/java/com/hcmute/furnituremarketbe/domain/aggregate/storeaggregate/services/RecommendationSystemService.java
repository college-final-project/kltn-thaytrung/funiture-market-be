package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.controller.exception.CommonRuntimeException;
import org.apache.http.client.utils.URIBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URISyntaxException;
import java.util.List;

@Service
public class RecommendationSystemService {
    @Value("${com.hcmute.furnituremarketbe.recommendation-service.url}")
    private String serviceUrl;

    @Value("${com.hcmute.furnituremarketbe.recommendation-service.url.implicit}")
    private String impRecommendApi;

    @Value("${com.hcmute.furnituremarketbe.recommendation-service.url.explicit}")
    private String expRecommendApi;

    public List<String> getRecommendationProducts(String buyerId, Integer offset, Integer limit, boolean isExplicit) {
        RestTemplate restTemplate = new RestTemplate();
        URIBuilder builder;
        try {
            builder = new URIBuilder(serviceUrl);
        } catch (URISyntaxException e) {
            throw new CommonRuntimeException(serviceUrl + " is not a valid URI");
        }
        if (isExplicit)
            builder.setPath(expRecommendApi);
        else
            builder.setPath(impRecommendApi);
        builder.addParameter("user_id", buyerId);
        if (offset != null)
            builder.addParameter("offset", offset.toString());
        if (limit != null)
            builder.addParameter("limit", limit.toString());
        return restTemplate.exchange(builder.toString(), HttpMethod.GET, null, new ParameterizedTypeReference<List<String>>() {}).getBody();
    }
}
