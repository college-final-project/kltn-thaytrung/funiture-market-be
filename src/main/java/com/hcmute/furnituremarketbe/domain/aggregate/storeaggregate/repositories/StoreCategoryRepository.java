package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StoreCategoryRepository extends JpaRepository<StoreCategory, String> {
    @Query("SELECT c FROM StoreCategory c WHERE c.id=:id")
    StoreCategory getCategoryById(@Param("id") String id);

    @Query("SELECT c FROM StoreCategory c WHERE c.store.id=:id")
    List<StoreCategory> getCategoryByStoreId(@Param("id") String id);
}
