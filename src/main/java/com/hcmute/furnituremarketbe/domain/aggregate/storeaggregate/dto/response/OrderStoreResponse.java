package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import lombok.Data;

@Data
public class OrderStoreResponse {
    private String id;
    private String name;
    private String logo;

    public OrderStoreResponse(Store store) {
        this.id = store.getId();
        this.name = store.getShopName();
        this.logo = store.getLogo();
    }
}
