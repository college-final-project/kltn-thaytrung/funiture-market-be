package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class ProvinceCity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "ghn_province_id")
    private Integer ghnProvinceId;
}
