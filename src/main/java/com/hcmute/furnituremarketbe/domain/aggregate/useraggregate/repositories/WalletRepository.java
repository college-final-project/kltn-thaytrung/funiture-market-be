package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface WalletRepository extends JpaRepository<Wallet, String> {
    @Query("SELECT w FROM Wallet w WHERE w.user.email=:email")
    Wallet getByEmail(@Param("email") String email);
}
