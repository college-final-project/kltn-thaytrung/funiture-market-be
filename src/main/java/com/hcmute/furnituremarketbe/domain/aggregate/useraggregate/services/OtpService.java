package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.cache.OtpCache;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.SendOtpViaEmailRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.SendViaEmailType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OtpService {
    @Autowired
    private MailService mailService;

    @Autowired
    OtpCache otpCache;

    @Autowired
    private UserRepository userRepository;

    public ResponseBaseAbstract sendOtpViaEmail(SendOtpViaEmailRequest request) {
        if (!this.userRepository.existsByEmail(request.getEmail())) {
            throw new ServiceExceptionFactory().notFound()
                    .addMessage("Không tìm thấy người dùng với địa chỉ email này");
        }
        User user = this.userRepository.getUserByEmail(request.getEmail());
        sendOtp(user, request.getType());

        return SuccessResponse.builder().addMessage("OTP đã được gửi").returnGetOK();
    }

    private void sendOtp(User user, SendViaEmailType type) {
        String otp = otpCache.create(user.getEmail());
        mailService.sendMailConfirmCode(type, user, otp);
    }
}
