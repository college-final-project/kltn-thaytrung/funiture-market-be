package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums;

public enum RepliedValue {
    TRUE,
    FALSE
}
