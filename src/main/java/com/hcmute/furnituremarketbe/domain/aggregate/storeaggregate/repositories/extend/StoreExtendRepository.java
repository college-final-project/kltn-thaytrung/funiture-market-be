package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;

import java.util.Map;

public interface StoreExtendRepository {
    PagingAndSortingResponse<Store> searchStore(Map<String, String> queries);
}
