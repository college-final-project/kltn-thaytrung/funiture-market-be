package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "tbl_order")
public class Order {
    @Id
    private String id;

    @Column(name = "created_at")
    private Date createdAt = new Date();

    @Convert(converter = ListStringConverter.class)
    @Column(name = "updated_at", columnDefinition = "TEXT")
    private List<String> updatedAt = new ArrayList<>();

    @Enumerated(EnumType.STRING)
    private OrderStatus status;

    @Column(name = "paid")
    private boolean paid;

    @Column(name="total")
    private Double total;

    @Column(name = "voucher_discount")
    private Double voucherDiscount;

    @Column(name = "shipping_fee")
    private Double shippingFee;

    @ManyToOne
    @JoinColumn(name="user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @ManyToOne
    @JoinColumn(name="store_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Store store;

    @ManyToOne
    @JoinColumn(name = "delivery_address_id")
    private DeliveryAddress deliveryAddress;

    @OneToMany(mappedBy = "order", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<OrderItem> orderItems;
}
