package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReportRepository extends JpaRepository<Report, String> {
    @Query("SELECT r FROM Report r WHERE r.id=:id AND r.deletedAt is null")
    Report getReportById(@Param("id") String id);

    @Query("SELECT r FROM Report r WHERE r.type=:type AND r.deletedAt is null")
    List<Report> getReportByType(@Param("type")ReportType type);
}
