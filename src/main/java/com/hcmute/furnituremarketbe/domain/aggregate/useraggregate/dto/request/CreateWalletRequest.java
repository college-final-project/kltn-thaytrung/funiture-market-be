package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import lombok.Data;

@Data
public class CreateWalletRequest {
    private Double amount;
}
