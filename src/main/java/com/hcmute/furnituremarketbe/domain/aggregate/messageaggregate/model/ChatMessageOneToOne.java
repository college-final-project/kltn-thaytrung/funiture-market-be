package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.entity.Message;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.enums.ChatMessageOneToOneType;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.time.ZonedDateTime;
import java.util.Date;

@Data
@AllArgsConstructor
public class ChatMessageOneToOne {
    private String senderId;
    private String receiverId;
    private String message;

    private String messageId;
    private ChatMessageOneToOneType type;
    private String randomHash;
    private boolean isRead;
    private ZonedDateTime createdAt;
    private String image;

    public ChatMessageOneToOne() {

    }

    public ChatMessageOneToOne(String senderId, String receiverId, Message message) {
        this.senderId = senderId;
        this.receiverId = receiverId;
        this.messageId = message.getId();
        this.message = message.getContent();
        this.createdAt = message.getCreatedAt();
        this.isRead = message.getIsRead();
        if (message.getContent().startsWith("img:")) {
            this.type = ChatMessageOneToOneType.IMAGE;
            this.image = message.getContent().replaceFirst("img:", "");
        } else {
            this.type = ChatMessageOneToOneType.MESSAGE;
        }
    }
    public void setMessageAsRead() {
        this.isRead = true;
    }
}
