package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums;

public enum ReportType {
    PRODUCT_REPORT,
    STORE_REPORT
}
