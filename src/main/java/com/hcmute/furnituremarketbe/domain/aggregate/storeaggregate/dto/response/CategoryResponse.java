package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreCategory;
import lombok.Data;

@Data
public class CategoryResponse {
    private String id;
    private String name;

    public CategoryResponse(StoreCategory category) {
        this.id = category.getId();
        this.name = category.getName();
    }
}
