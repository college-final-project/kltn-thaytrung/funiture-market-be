package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import lombok.Data;

import java.util.Date;

@Data
public class RegisterSellerRequest {
    private String shopName;
    private String ownerName;
    private Date birthday;
    private UserGender gender;
    private String email;
    private String phone;
    private String password;
    private String address;
    private String deliveryMethod;
}
