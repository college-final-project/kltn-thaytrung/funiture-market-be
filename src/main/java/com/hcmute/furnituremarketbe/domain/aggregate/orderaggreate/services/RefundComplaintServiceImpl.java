package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateRefundComplaintRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.OrderRefundResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.RefundComplaintResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.RefundComplaint;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderRefundRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.RefundComplaintRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.RefundComplaintService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class RefundComplaintServiceImpl implements RefundComplaintService {
    @Autowired
    private RefundComplaintRepository refundComplaintRepository;

    @Autowired
    private OrderRefundRepository orderRefundRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseBaseAbstract getAllRefundComplaints() {
        List<RefundComplaint> refundComplaints = this.refundComplaintRepository.findAll();
        List<RefundComplaintResponse> responses = refundComplaints.stream().map(this::getRefundComplaintResponse).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getRefundByDone(Boolean done) {
        List<RefundComplaint> refundComplaints = this.refundComplaintRepository.getRefundByDone(done);
        List<RefundComplaintResponse> responses = refundComplaints.stream().map(this::getRefundComplaintResponse).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createRefundComplaint(CreateRefundComplaintRequest request, List<MultipartFile> images) {
        String orderRefundId = request.getOrderRefundId();
        OrderRefund orderRefund = this.orderRefundRepository.getOrderRefundById(orderRefundId);
        if (orderRefund == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy yêu cầu hoàn tiền với id = "+ orderRefundId);
        RefundComplaint refundComplaint = new RefundComplaint();
        refundComplaint.setId(UUID.randomUUID().toString());
        refundComplaint.setDone(false);
        refundComplaint.setReason(request.getReason());
        refundComplaint.setOrderRefund(orderRefund);
        List<String> imageNames = images.stream().map(i -> {
            try {
                return this.fileStorageService.save(i);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }).toList();
        refundComplaint.setImages(imageNames);
        this.refundComplaintRepository.save(refundComplaint);

        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(getRefundComplaintResponse(refundComplaint))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract markAsDone(String refundComplaintId) {
        RefundComplaint refundComplaint = this.refundComplaintRepository.getRefundComplaintById(refundComplaintId);
        if (refundComplaint == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy khiếu nại yêu cầu hoàn đơn nào với id = "+ refundComplaintId);
        refundComplaint.setDone(true);
        this.refundComplaintRepository.save(refundComplaint);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(getRefundComplaintResponse(refundComplaint))
                .returnUpdated();
    }

    private RefundComplaintResponse getRefundComplaintResponse(RefundComplaint refundComplaint) {
        OrderRefundResponse orderRefundResponse = new OrderRefundResponse(refundComplaint.getOrderRefund());
        List<String> imageUrl1 = orderRefundResponse.getImages().stream().map(i -> this.fileStorageService.getImageUrl(i)).toList();
        orderRefundResponse.setImages(imageUrl1);
        RefundComplaintResponse response = new RefundComplaintResponse(refundComplaint, orderRefundResponse);
        List<String> imageUrl2 = response.getImages().stream().map(i -> this.fileStorageService.getImageUrl(i)).toList();
        response.setImages(imageUrl2);
        return response;
    }
}
