package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class StoreReview {
    @Id
    private String id;

    @Column(name = "content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "star")
    private Integer star;

    @Column(name = "reply", columnDefinition = "TEXT")
    private String reply;

    @Column(name = "created_at")
    private Date createdAt = new Date();

    @ManyToOne
    @JoinColumn(name = "product_id")
    private Product product;

    @ManyToOne
    @JoinColumn(name = "store_id")
    private Store store;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    private User buyer;
}
