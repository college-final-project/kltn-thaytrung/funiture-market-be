package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

@Data
public class UpdateDefaultAddressRequest {
    private String defaultAddressId;
}
