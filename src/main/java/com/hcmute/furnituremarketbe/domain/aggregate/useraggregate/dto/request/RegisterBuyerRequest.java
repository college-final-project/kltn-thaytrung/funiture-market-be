package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import lombok.Data;

import java.util.Date;

@Data
public class RegisterBuyerRequest {
    private String email;
    private String phone;
    private Date birthday;
    private String password;
    private String fullName;
    private UserGender gender;
}
