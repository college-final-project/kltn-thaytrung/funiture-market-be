package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.CommuneWardTown;
import lombok.Data;

@Data
public class WardResponse {
    private Integer id;
    private String name;
    private Integer ghnWardCode;

    public WardResponse(CommuneWardTown c) {
        this.id = c.getId();
        this.name = c.getName();
        this.ghnWardCode = getGhnWardCode();
    }
}
