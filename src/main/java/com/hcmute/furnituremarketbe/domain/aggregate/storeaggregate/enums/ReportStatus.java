package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums;

public enum ReportStatus {
    PENDING,
    PROCESSING,
    ACCEPTED,
    UNACCEPTED
}
