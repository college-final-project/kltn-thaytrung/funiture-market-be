package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherUsage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface VoucherUsageRepository extends JpaRepository<VoucherUsage, String> {
    @Query("SELECT v FROM VoucherUsage v WHERE v.voucher.id=:voucherId AND v.buyer.id=:buyerId")
    VoucherUsage getByVoucherIdAndBuyerId(@Param("voucherId") String voucherId, @Param("buyerId") String buyerId);
}
