package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.BuyerOrderRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateOrderRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateOrderStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

import java.util.Map;

public interface OrderService {
    ResponseBaseAbstract createOrder(User buyer, BuyerOrderRequest request);

    ResponseBaseAbstract getAllOrderByStore(String storeId);

    ResponseBaseAbstract getAllOrder();

    ResponseBaseAbstract searchOrder(Map<String, String> queries);

    ResponseBaseAbstract getAllOrderByUserId(User user, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract updateOrderStatus(UpdateOrderStatusRequest request);

    ResponseBaseAbstract getByOrderStatus(User user, OrderStatus status, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getOrderDetail(String orderId);

    ResponseBaseAbstract cancelOrder(String orderId);

    void refundOrder(String orderId);

    ResponseBaseAbstract payAddition(User buyer, String orderId);
}
