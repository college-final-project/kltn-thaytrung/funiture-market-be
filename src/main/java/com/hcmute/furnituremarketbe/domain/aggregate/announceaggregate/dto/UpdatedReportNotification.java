package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ReportResponse;
import lombok.Data;

@Data
public class UpdatedReportNotification {
    private ReportResponse response;
    private String message;

    public UpdatedReportNotification(ReportResponse response, String message) {
        this.response = response;
        this.message = message;
    }
}
