package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;

import java.util.Map;

public interface MarketingProductExtendRepository {
    PagingAndSortingResponse<MarketingProduct> searchMarketingProduct(Map<String, String> queries);
}
