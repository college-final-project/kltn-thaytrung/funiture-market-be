package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import lombok.Data;

import java.util.List;

@Data
public class MarketingStatisticResponse {
    private Double income;
    private List<IncomeByDayResponse> incomeByDays;
}
