package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportExplanationRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ReportExplanationResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.ReportExplanation;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ReportExplanationRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ReportRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportExplanationService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

@Service
public class ReportExplanationServiceImpl implements ReportExplanationService {
    @Autowired
    private ReportRepository reportRepository;

    @Autowired
    private ReportExplanationRepository reportExplanationRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseBaseAbstract createReportExplanation(CreateReportExplanationRequest request, List<MultipartFile> images) {
        ReportExplanation reportExplanation = new ReportExplanation();
        reportExplanation.setId(UUID.randomUUID().toString());
        reportExplanation.setDescription(request.getDescription());

        String reportId = request.getReportId();
        Report report = this.reportRepository.getReportById(reportId);
        if (report == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy báo cáo với id = "+reportId);
        reportExplanation.setReport(report);
        List<String> imageNames = images.stream().map(image -> {
            try {
                return this.fileStorageService.save(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return " ";
        }).toList();
        reportExplanation.setImages(imageNames);
        this.reportExplanationRepository.save(reportExplanation);
        ReportExplanationResponse response = getReportExplanationResponse(reportExplanation);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(response)
                .returnCreated();
    }

    private ReportExplanationResponse getReportExplanationResponse(ReportExplanation reportExplanation) {
        ReportExplanationResponse response = new ReportExplanationResponse(reportExplanation);
        List<String> imageUrls = response.getImages().stream().map(image -> this.fileStorageService.getImageUrl(image)).toList();
        response.setImages(imageUrls);
        return response;
    }
}
