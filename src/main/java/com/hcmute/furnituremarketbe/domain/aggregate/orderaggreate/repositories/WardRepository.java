package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.CommuneWardTown;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WardRepository extends JpaRepository<CommuneWardTown, Integer> {
    @Query("SELECT w FROM CommuneWardTown w WHERE w.district.id=:id")
    List<CommuneWardTown> getCommuneWardTownByDistrictId(@Param("id") Integer id);

    @Query("SELECT w FROM CommuneWardTown w WHERE w.id=:id")
    CommuneWardTown getTownById(@Param("id") Integer id);
}
