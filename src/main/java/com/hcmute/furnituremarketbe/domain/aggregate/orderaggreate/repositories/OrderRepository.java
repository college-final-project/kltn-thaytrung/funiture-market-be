package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.extend.OrderExtendRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface OrderRepository extends JpaRepository<Order, String>, OrderExtendRepository {
    @Query("SELECT o FROM Order o WHERE o.user.id=:id ORDER BY o.createdAt DESC")
    Page<Order> getOrderByUserId(@Param("id") String id, Pageable pageable);

    @Query("SELECT o FROM Order o WHERE o.id=:id")
    Order getOrderById(@Param("id") String id);

    @Query("SELECT o FROM Order o WHERE o.status=:status AND o.user.id=:id ORDER BY o.createdAt DESC")
    Page<Order> getBuyerOrderByStatus(@Param("status") OrderStatus status, @Param("id") String id, Pageable pageable);

    @Query("SELECT o FROM Order o WHERE o.status=:status AND o.store.id=:id ORDER BY o.createdAt DESC")
    Page<Order> getStoreOrderByStatus(@Param("status") OrderStatus status, @Param("id") String id, Pageable pageable);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate) AND o.store.id=:storeId")
    Integer getNumOfOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate, @Param("storeId") String storeId);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate) AND o.store.id=:storeId")
    Integer getNumOfOrderByDay(@Param("givenDate")Date givenDate, @Param("storeId") String storeId);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate) AND o.store.id=:storeId")
    List<Order> getOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate, @Param("storeId") String storeId);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate) AND o.store.id=:storeId")
    List<Order> getOrderByDay(@Param("givenDate")Date givenDate, @Param("storeId") String storeId);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate)")
    Integer getAdminNumOfOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', o.createdAt) >= FUNCTION('DATE', :startDate) ")
    List<Order> getAdminOrderByMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate);

    @Query("SELECT COUNT(o) FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate)")
    Integer getAdminNumOfOrderByDay(@Param("givenDate")Date givenDate);

    @Query("SELECT o FROM Order o WHERE FUNCTION('DATE', o.createdAt) = FUNCTION('DATE', :givenDate)")
    List<Order> getAdminOrderByDay(@Param("givenDate")Date givenDate);
}
