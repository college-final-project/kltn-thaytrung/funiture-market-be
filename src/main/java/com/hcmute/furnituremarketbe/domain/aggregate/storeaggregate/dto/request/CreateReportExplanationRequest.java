package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class CreateReportExplanationRequest {
    private String description;
    private String reportId;
}

