package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.VoucherType;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ShortProductResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class VoucherResponse {
    private String id;
    private String name;
    private VoucherType type;
    private String code;
    private Date startDate;
    private Date endDate;
    private Double maxDiscount;
    private Double minValue;
    private Integer totalTimes;
    private Integer buyerLimit;
    private boolean hidden;
    private Boolean usable;
    private List<ShortProductResponse> productList = new ArrayList<>();

    public VoucherResponse(Voucher voucher) {
        this.id = voucher.getId();
        this.name = voucher.getName();
        this.type = voucher.getType();
        this.code = voucher.getCode();
        this.startDate = voucher.getStartDate();
        this.endDate = voucher.getEndDate();
        this.maxDiscount = voucher.getMaxDiscount();
        this.minValue = voucher.getMinValue();
        this.totalTimes = voucher.getTotalTimes();
        this.buyerLimit = voucher.getBuyerLimit();
        this.hidden = voucher.isHidden();
    }
}
