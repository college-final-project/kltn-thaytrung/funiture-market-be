package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.SendViaEmailType;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionBase;
import freemarker.template.Configuration;
import freemarker.template.Template;
import jakarta.mail.internet.MimeMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class MailService {
    @Autowired
    private JavaMailSender mailSender;

    @Value("${spring.mail.username}")
    private String senderMail;

    @Value("${com.hcmute.furnituremarketbe.security.otp.expired-after-mins}")
    private long expiredAfterMins;

    @Autowired
    private Configuration freeMakerConfiguration;

    public void sendMailConfirmCode(SendViaEmailType type, User user, String otp) {
        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Map<String, Object> model = new HashMap<>();
            model.put("fullName", user.getFullName());
            model.put("otp", otp);
            model.put("expiredAfterMins", expiredAfterMins);

            Template t = freeMakerConfiguration.getTemplate("email-template.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(user.getEmail());
            helper.setText(html, true);
            switch (type) {
                case REGISTER_USER -> {
                    helper.setSubject("[Furniture Market] MÃ XÁC NHẬN ĐĂNG KÝ TÀI KHOẢN NGƯỜI DÙNG");
                }
                case REGISTER_SELLER -> {
                    helper.setSubject("[Furniture Market] MÃ XÁC NHẬN ĐĂNG KÝ TÀI KHOẢN NGƯỜI BÁN");
                }
                case RESET_PASSWORD -> {
                    helper.setSubject("[Furniture Market] MÃ XÁC NHẬN LẤY LẠI MẬT KHẨU");
                }
            }

            helper.setFrom(senderMail);

            mailSender.send(message);

        } catch (Exception e) {
            throw new ServiceExceptionBase(HttpStatus.INTERNAL_SERVER_ERROR)
                    .addMessage("Gửi mã xác nhận thất bại: (" + e.getMessage() + ")");
        }
    }

    public void sendNewPassword(User user, String newPassword) {
        MimeMessage message = mailSender.createMimeMessage();

        try {
            MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                    StandardCharsets.UTF_8.name());

            Map<String, Object> model = new HashMap<>();
            model.put("fullName", user.getFullName());
            model.put("password", newPassword);

            Template t = freeMakerConfiguration.getTemplate("reset-password.ftl");
            String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, model);

            helper.setTo(user.getEmail());
            helper.setText(html, true);
            helper.setSubject("[Furniture Market] MẬT KHẨU MỚI CỦA NGƯỜI DÙNG");
            helper.setFrom(senderMail);

            mailSender.send(message);

        } catch (Exception e) {
            throw new ServiceExceptionBase(HttpStatus.INTERNAL_SERVER_ERROR)
                    .addMessage("Gửi mật khẩu mới thất bại: (" + e.getMessage() + ")");
        }
    }
}
