package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Connection;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ConnectionRepository extends JpaRepository<Connection, String> {
    @Query("SELECT u FROM Connection c INNER JOIN User u ON c.follower.id = u.id WHERE c.user.id=:id AND c.deletedAt is null")
    List<User> getFollowers(@Param("id") String id);

    @Query("SELECT COUNT(u) FROM Connection c INNER JOIN User u ON c.follower.id = u.id WHERE c.user.id=:id AND c.deletedAt is null")
    Integer getNumOfFollowerOfUser(@Param("id") String id);

    @Query("SELECT u FROM Connection c INNER JOIN User u ON c.user.id = u.id WHERE c.follower.id=:id AND c.deletedAt is null")
    List<User> getFollowingPeople(@Param("id") String id);

    @Query("SELECT COUNT(u) FROM Connection c INNER JOIN User u ON c.user.id = u.id WHERE c.follower.id=:id AND c.deletedAt is null")
    Integer getNumOfFollowingAccount(@Param("id") String id);

    @Query("SELECT c FROM Connection c WHERE c.user.id=:userId AND c.follower.id=:followerId AND c.deletedAt is null")
    Connection getConnectionByUserAndFollower(@Param("userId") String userId, @Param("followerId") String followerId);

    @Query("SELECT CASE WHEN COUNT(c) > 0 THEN TRUE ELSE FALSE END FROM Connection c WHERE c.user.id=:userId AND c.follower.id=:followerId AND c.deletedAt is null")
    boolean existConnection(@Param("userId") String userId, @Param("followerId") String followerId);
}
