package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;

import java.util.Map;

public interface ProductExtendRepository {
    PagingAndSortingResponse<Product> searchProduct(Map<String, String> queries);
}
