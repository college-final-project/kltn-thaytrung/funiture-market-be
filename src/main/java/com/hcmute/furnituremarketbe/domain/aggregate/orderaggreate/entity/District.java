package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import jakarta.persistence.*;
import lombok.Data;

import java.util.Date;

@Entity
@Data
public class District {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "ghn_district_id")
    private Integer ghnDistrictId;

    @ManyToOne
    @JoinColumn(name = "province_city_id")
    private ProvinceCity provinceCity;
}
