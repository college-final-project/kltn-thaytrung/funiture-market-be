package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface UserService{
    ResponseBaseAbstract login(LoginRequest request, HttpServletResponse response);

    ResponseBaseAbstract getListRole();

    ResponseBaseAbstract logout(String refreshTokenId, HttpServletRequest request);

    ResponseBaseAbstract toggleUserStatus(String userId);

    ResponseBaseAbstract createAdminAccount(CreateAdminAccountRequest request);

    ResponseBaseAbstract registerBuyerAccount(RegisterBuyerRequest request);

    ResponseBaseAbstract registerSellerAccount(RegisterSellerRequest request);

    ResponseBaseAbstract refresh(String refreshTokenId,
                                 HttpServletRequest request, HttpServletResponse response);

    ResponseBaseAbstract getUserProfile(User user);

    ResponseBaseAbstract updateProfile(User user, UpdateProfileRequest request);

    ResponseBaseAbstract changePassword(User user, UpdatePasswordRequest request);

    ResponseBaseAbstract updateAvatar(User user, MultipartFile newAvatar) throws IOException;

    ResponseBaseAbstract confirmUserEmail(EmailConfirmationRequest request);

    ResponseBaseAbstract updateEmail(User user, UpdateEmailRequest request);

    ResponseBaseAbstract restorePassword(EmailConfirmationRequest request);

    ResponseBaseAbstract getAllUsers();
}
