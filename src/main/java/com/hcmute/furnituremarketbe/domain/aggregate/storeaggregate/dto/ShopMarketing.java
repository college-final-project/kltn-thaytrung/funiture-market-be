package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import lombok.Data;

import java.util.List;

@Data
public class ShopMarketing {
    private String id;
    private String logo;
    private Integer soldAmount;
    private Double income;
    private Double avgRating;

    public ShopMarketing(Store store) {
        assert store.getProducts() != null && store.getProducts().size() != 0;
        this.id = store.getId();
        this.logo = store.getLogo();
        this.soldAmount = store.getProducts().stream().mapToInt(Product::getSold).sum();
        this.income = store.getOrders().stream().mapToDouble(o -> (o.getTotal() - o.getVoucherDiscount() + o.getShippingFee())).sum();
        this.avgRating = getAvgRating(store);
    }

    private Double getAvgRating(Store store) {
        List<Product> productList = store.getProducts();
        Double totalAvgRating = 0.0;
        Integer count = 0;
        for (Product product : productList) {
            if (product.getReviewAmount() != 0) {
                Double productAvgRating = product.getTotalReviewPoint().doubleValue() / product.getReviewAmount();
                totalAvgRating += productAvgRating;
                count += 1;
            }
        }
        return totalAvgRating / count;
    }
}
