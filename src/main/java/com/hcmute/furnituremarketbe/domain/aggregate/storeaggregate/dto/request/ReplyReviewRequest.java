package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class ReplyReviewRequest {
    private String reviewId;
    private String replyContent;
}
