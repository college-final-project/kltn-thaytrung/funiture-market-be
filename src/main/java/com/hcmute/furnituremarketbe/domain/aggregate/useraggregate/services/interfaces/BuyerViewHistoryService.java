package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import jakarta.transaction.Transactional;

public interface BuyerViewHistoryService {
    @Transactional
    void updateBuyerViewHistory(String buyerId, String productId);
}
