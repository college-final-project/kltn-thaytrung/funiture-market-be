package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.VoucherResponse;
import lombok.Data;

@Data
public class CreatedVoucherNotification {
    private VoucherResponse voucherResponse;
    private String message;

    public CreatedVoucherNotification(VoucherResponse voucherResponse, String message) {
        this.voucherResponse = voucherResponse;
        this.message = message;
    }
}
