package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.District;
import lombok.Data;

@Data
public class DistrictResponse {
    private Integer id;
    private String name;
    private Integer ghnDistrictId;

    public DistrictResponse(District d) {
        this.id = d.getId();
        this.name = d.getName();
        this.ghnDistrictId = d.getGhnDistrictId();
    }
}
