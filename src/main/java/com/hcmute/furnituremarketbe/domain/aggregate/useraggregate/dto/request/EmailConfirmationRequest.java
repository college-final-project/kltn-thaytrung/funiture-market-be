package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import lombok.Data;

@Data
public class EmailConfirmationRequest {
    private String email;
    private String otpCode;
}
