package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;

import java.util.Map;

public interface OrderExtendRepository {
    PagingAndSortingResponse<Order> searchOrder(Map<String, String> queries);
}
