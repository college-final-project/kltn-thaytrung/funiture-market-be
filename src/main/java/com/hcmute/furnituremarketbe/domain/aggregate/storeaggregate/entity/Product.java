package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerFavouriteProduct;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Product {
    @Id
    private String id;

    @Column(name = "created_at")
    private Date createdAt = new Date();

    @Column(name="name")
    private String name;

    @Column(name = "on_display")
    private boolean onDisplay;

    @Column(name="on_sale")
    private boolean onSale;

    @Column(name = "used")
    private boolean used;

    @Column(name="price")
    private Double price;

    @Column(name="sale_price")
    private Double salePrice;

    @Column(name="description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "weight")
    private Integer weight;

    @Column(name = "height")
    private Integer height;

    @Column(name = "length")
    private Integer length;

    @Column(name = "width")
    private Integer width;

    @Column(name = "material")
    private String material;

    @Column(name = "sold")
    private Integer sold;

    @Column(name = "in_stock")
    private Integer inStock;

    @Column(name = "is_featured")
    private boolean isFeatured;

    @Column(name = "hidden")
    private boolean hidden;

    @Column(name = "total_review_point")
    private Integer totalReviewPoint;

    @Column(name = "review_amount")
    private Integer reviewAmount;

    @Column(name = "avg_point")
    private Double avgPoint;

    @Column(name = "thumbnail")
    private String thumbnail;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "images", columnDefinition = "TEXT")
    private List<String> images = new ArrayList<>();

    @Convert(converter = ListStringConverter.class)
    @Column(name = "store_categories", columnDefinition = "TEXT")
    private List<String> storeCategories = new ArrayList<>();

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "category_id")
    private SystemCategory category;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "store_id")
    private Store store;

    @ManyToOne
    @JoinColumn(name = "marketing_product_id")
    private MarketingProduct marketingProduct;

    @OneToMany(mappedBy = "product", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<BuyerFavouriteProduct> buyerFavouriteProducts;
}
