package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class GHNFeeCalRequest {
    private Integer fromDistrictId;
    private Integer serviceTypeId;
    private Integer toDistrictId;
    private String toWardCode;
    private Integer height;
    private Integer length;
    private Integer weight;
    private Integer width;
    private Integer insuranceValue;
}
