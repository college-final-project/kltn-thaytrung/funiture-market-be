package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportStatus;
import lombok.Data;

@Data
public class UpdateReportStatusRequest {
    @JsonIgnore
    private String id;
    private ReportStatus status;
}
