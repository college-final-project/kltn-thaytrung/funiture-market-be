package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateDeliveryAddressRequest {
    @JsonIgnore
    private String id;
    private String detailAddress;
    private String receiver;
    private String phone;
    private Integer townId;
}
