package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class MarketingProductExtendRepositoryImpl extends ExtendEntityRepositoryBase<MarketingProduct> implements MarketingProductExtendRepository{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<MarketingProduct> searchMarketingProduct(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<MarketingProduct> criteriaQuery = criteriaBuilder.createQuery(MarketingProduct.class);
        Root<MarketingProduct> mProductRoot = criteriaQuery.from(MarketingProduct.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                mProductRoot,
                MarketingProduct.class.getDeclaredFields()
        );
    }
}
