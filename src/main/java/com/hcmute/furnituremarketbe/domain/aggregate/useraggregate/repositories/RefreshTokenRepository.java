package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.RefreshToken;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RefreshTokenRepository extends JpaRepository<RefreshToken, String> {
    @Query("SELECT r FROM RefreshToken r WHERE r.id=:id")
    RefreshToken getRefreshTokenById(@Param("id") String id);

    @Query("SELECT r FROM RefreshToken r WHERE r.familyId=:familyId")
    List<RefreshToken> getAllTokenByFamilyId(@Param("familyId") String familyId);

    @Query("SELECT r FROM RefreshToken r WHERE FUNCTION('DATE', r.expirationDate) < FUNCTION('DATE', :givenDate)")
    List<RefreshToken> getByExpirationDateBefore(@Param("givenDate") Date givenDate);
}
