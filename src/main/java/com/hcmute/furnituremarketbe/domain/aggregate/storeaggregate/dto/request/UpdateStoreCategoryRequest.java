package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class UpdateStoreCategoryRequest {
    @JsonIgnore
    private String id;
    private String name;
}
