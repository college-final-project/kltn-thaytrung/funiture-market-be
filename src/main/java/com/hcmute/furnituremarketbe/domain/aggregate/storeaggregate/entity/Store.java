package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Store {
    @Id
    private String id;

    @Column(name = "shop_name")
    private String shopName;

    @Column(name = "logo")
    private String logo;

    @Column(name = "address")
    private String address;

    @Column(name = "delivery_method")
    private String deliveryMethod;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "tax", columnDefinition = "TEXT")
    private List<String> tax = new ArrayList<>();

    @Convert(converter = ListStringConverter.class)
    @Column(name = "identifier", columnDefinition = "TEXT")
    private List<String> identifier = new ArrayList<>();

    @Column(name = "break_status")
    private boolean breakStatus;

    @Column(name = "nfollower")
    private Integer nfollower;

    @Column(name = "top_banner")
    private String topBanner;

    @Column(name = "info_banner")
    private String infoBanner;

    @Column(name = "qualified")
    private boolean qualified;

    @OneToOne
    @JoinColumn(name = "user_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @OneToMany(mappedBy = "store", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<StoreCategory> categories;

    @OneToMany(mappedBy = "store", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Product> products;

    @OneToMany(mappedBy = "store", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Voucher> vouchers;

    @OneToMany(mappedBy = "store", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<Order> orders;
}
