package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface BuyerFavouriteProductService {
    ResponseBaseAbstract toggleFavouriteProduct(String buyerId, String productId);

    ResponseBaseAbstract getWishListByBuyer(String buyerId);
}
