package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.VoucherRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class VoucherScheduleTask {
    @Autowired
    private VoucherRepository voucherRepository;

    @Scheduled(cron = "0 0 0 * * *")
    public void updateVoucherHidden() {
        Date givenDate = new Date();
        List<Voucher> vouchers = this.voucherRepository.getExpiredVoucher(givenDate);
        for (Voucher v : vouchers) {
            v.setHidden(true);
            this.voucherRepository.save(v);
        }
    }
}
