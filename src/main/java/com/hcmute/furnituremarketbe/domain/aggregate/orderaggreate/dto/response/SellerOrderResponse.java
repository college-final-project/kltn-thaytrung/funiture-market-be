package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;


import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.OrderStoreResponse;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SellerOrderResponse {
    private String id;
    private OrderStatus status;
    private Double total;
    private Boolean paid;
    private BuyerDeliveryAddressResponse buyerInfo;
    List<OrderItemResponse> responses = new ArrayList<>();
    private OrderStoreResponse storeInfo;

    public SellerOrderResponse(Order order) {
        this.id = order.getId();
        this.status = order.getStatus();
        this.total = order.getTotal() - order.getVoucherDiscount() + order.getShippingFee();
        this.paid = order.isPaid();
    }
}
