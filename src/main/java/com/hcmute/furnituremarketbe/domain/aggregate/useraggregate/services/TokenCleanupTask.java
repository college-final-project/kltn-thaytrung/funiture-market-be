package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.RefreshToken;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

@Component
public class TokenCleanupTask {
    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteExpiredTokens() {
        Date currentDate = new Date();
        List<RefreshToken> expiredTokens = this.refreshTokenRepository.getByExpirationDateBefore(currentDate);
        this.refreshTokenRepository.deleteAll(expiredTokens);
    }
}
