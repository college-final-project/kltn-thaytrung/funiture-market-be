package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.BuyerDeliveryAddressResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.DistrictResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.ProvinceCityResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.WardResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.CommuneWardTown;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.DeliveryAddress;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.District;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.ProvinceCity;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.DeliveryAddressRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.DistrictRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.ProvinceRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.WardRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.DeliveryAddressService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionBase;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class DeliveryAddressServiceImpl implements DeliveryAddressService {
    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DistrictRepository districtRepository;

    @Autowired
    private ProvinceRepository provinceRepository;

    @Autowired
    private WardRepository wardRepository;

    @Override
    public ResponseBaseAbstract getAllProvinceCity() {
        List<ProvinceCity> provinceCities = this.provinceRepository.findAll();
        List<ProvinceCityResponse> data = provinceCities.stream().map(p -> new ProvinceCityResponse(p)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getDistrictByProvince(Integer id) {
        ProvinceCity p = this.provinceRepository.getProvinceById(id);
        if (p == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy province hoặc city có id = "+id);
        List<District> districts = this.districtRepository.getDistrictByProvinceId(id);
        List<DistrictResponse> data = districts.stream().map(d -> new DistrictResponse(d)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getWardByDistrict(Integer id) {
        District d = this.districtRepository.getDistrictById(id);
        if (d == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy district có id = "+id);

        List<CommuneWardTown> wardList = this.wardRepository.getCommuneWardTownByDistrictId(id);
        List<WardResponse> data = wardList.stream().map(w -> new WardResponse(w)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createDeliveryAddress(User user, CreateDeliveryAddressRequest request) {
        DeliveryAddress deliveryAddress = new DeliveryAddress();
        deliveryAddress.setId(UUID.randomUUID().toString());
        deliveryAddress.setReceiver(request.getReceiver());
        deliveryAddress.setPhone(request.getPhone());
        deliveryAddress.setDetailAddress(request.getDetailAddress());
        CommuneWardTown town = this.wardRepository.getTownById(request.getTownId());
        if (town == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy phường với id = "+request.getTownId());
        }
        deliveryAddress.setTown(town);
        deliveryAddress.setUser(user);
        this.deliveryAddressRepository.save(deliveryAddress);
        List<DeliveryAddress> addressList = this.deliveryAddressRepository.getAllDeliveryAddressByUserId(user.getId());
        if (addressList.size() == 1) {
            user.setDeliveryAddress(deliveryAddress);
            this.userRepository.save(user);
        }
        return SuccessResponse.builder()
                .addMessage("Tạo địa chỉ giao hàng thành công")
                .setData(new BuyerDeliveryAddressResponse(deliveryAddress))
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract updateDeliveryAddress(User user, UpdateDeliveryAddressRequest request) {
        DeliveryAddress deliveryAddress = this.deliveryAddressRepository.getDeliveryAddressById(request.getId());
        if (deliveryAddress == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại địa chỉ với id = " + request.getId());
        if (deliveryAddress.getUser().getId() != user.getId())
            throw ServiceExceptionFactory.forbidden().addMessage("Không có quyền thao tác");
        deliveryAddress.setDetailAddress(request.getDetailAddress());
        deliveryAddress.setReceiver(request.getReceiver());
        deliveryAddress.setPhone(request.getPhone());

        CommuneWardTown town = this.wardRepository.getTownById(request.getTownId());
        if (town == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Không tồn tại phường, thị trấn");
        deliveryAddress.setTown(town);
        this.deliveryAddressRepository.save(deliveryAddress);
        BuyerDeliveryAddressResponse data =
                new BuyerDeliveryAddressResponse(deliveryAddress);
        return SuccessResponse.builder().addMessage("Cập nhật địa chỉ giao hàng thành công")
                .setData(data)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteDeliveryAddress(User user, String deliveryAddressId) {
        DeliveryAddress deliveryAddress = this.deliveryAddressRepository.getDeliveryAddressById(deliveryAddressId);
        if (deliveryAddress == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại địa chỉ với id = " + deliveryAddressId);
        if (!deliveryAddress.getUser().getId().equals(user.getId()))
            throw ServiceExceptionFactory.forbidden().addMessage("Không có quyền thao tác");
        if (user.getDeliveryAddress().getId().equals(deliveryAddressId)) {
            user.setDeliveryAddress(null);
            this.userRepository.save(user);
        }
        this.deliveryAddressRepository.delete(deliveryAddress);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract getAllDeliveryAddressByUser(User user) {
        List<DeliveryAddress> deliveryAddresses = this.deliveryAddressRepository.getAllDeliveryAddressByUserId(user.getId());
        List<BuyerDeliveryAddressResponse> data = deliveryAddresses.stream().map(address -> new BuyerDeliveryAddressResponse(address)).toList();
        Map<String, Object> responseData = new HashMap<>();
        responseData.put("deliveryAddresses", data);
        responseData.put("defaultAddressId", user.getDeliveryAddress() != null ? user.getDeliveryAddress().getId() : null);

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responseData)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateDefaultAddress(User user, UpdateDefaultAddressRequest request) {
        if (request.getDefaultAddressId() == null) {
            throw ServiceExceptionFactory.badRequest().addMessage("Dữ liệu không hợp lệ");
        }

        DeliveryAddress address = this.deliveryAddressRepository.getDeliveryAddressById(request.getDefaultAddressId());
        if (address == null) {
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy địa chỉ có id = " + request.getDefaultAddressId());
        }

        user.setDeliveryAddress(address);
        this.userRepository.save(user);
        Map<String, Object> data = new HashMap<>();
        data.put("defaultAddressId",address.getId());
        return SuccessResponse.builder()
                .addMessage("Cập nhật địa chỉ mặc định thành công")
                .setData(data)
                .returnUpdated();
    }
}
