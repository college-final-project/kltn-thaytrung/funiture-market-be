package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class GetListReviewByReplyRequest {
    private boolean isReply;
    private Integer currentPage;
    private Integer pageSize;
    private String sort;
}
