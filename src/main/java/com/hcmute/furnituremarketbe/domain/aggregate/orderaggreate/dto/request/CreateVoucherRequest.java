package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.VoucherType;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class CreateVoucherRequest {
    private String name;
    private VoucherType type;
    private String code;
    private Date startDate;
    private Date endDate;
    private Double maxDiscount;
    private Double minValue;
    private Integer totalTimes;
    private Integer buyerLimit;
    private List<String> productIds = new ArrayList<>();
}
