package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums;

public enum OrderStatus {
    UNPAID,
    TO_SHIP,
    SHIPPING,
    COMPLETED,
    CANCELLED,
    FAILED_DELIVERY,
    REFUNDED
}
