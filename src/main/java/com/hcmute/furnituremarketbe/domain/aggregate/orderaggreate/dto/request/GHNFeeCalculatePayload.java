package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Setter;

@Data
public class GHNFeeCalculatePayload {
    @JsonProperty("from_district_id")
    private int fromDistrictId;

    @JsonProperty("service_type_id")
    private Integer serviceTypeId;

    @JsonProperty("to_district_id")
    private int toDistrictId;

    @JsonProperty("to_ward_code")
    private String toWardCode;

    @JsonProperty("height")
    private int height;

    @JsonProperty("length")
    private int length;

    @JsonProperty("weight")
    private int weight;

    @JsonProperty("width")
    private int width;

    @JsonProperty("insurance_value")
    private int insuranceValue;
}
