package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.VoucherType;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class UpdateVoucherRequest {
    @JsonIgnore
    private String id;
    private String name;
    private String code;
    private Date startDate;
    private Date endDate;
    private Double maxDiscount;
    private Double minValue;
    private Integer totalTimes;
    private Integer buyerLimit;
    private Boolean hidden;
    private List<String> productIds = new ArrayList<>();
}
