package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.validator.constraints.Length;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class Message {
    @Id
    private String id;

    @Column(name = "createdAt")
    private ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Column(name = "lastUpdatedAt")
    private ZonedDateTime lastUpdatedAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Column(name = "deletedAt")
    private Date deletedAt;

    @Column(name = "text_content", columnDefinition = "TEXT")
    private String content;

    @Column(name = "isRead")
    private Boolean isRead = false;

    @ManyToOne
    @JoinColumn(name = "sender_id")
    private User sender;

    @ManyToOne
    @JoinColumn(name = "receiver_id")
    private User receiver;
}
