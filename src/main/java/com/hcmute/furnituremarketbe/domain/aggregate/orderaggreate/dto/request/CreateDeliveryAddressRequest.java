package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

@Data
public class CreateDeliveryAddressRequest {
    private String receiver;
    private String phone;
    private String detailAddress;
    private Integer townId;
}
