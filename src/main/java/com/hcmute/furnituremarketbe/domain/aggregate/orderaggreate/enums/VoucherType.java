package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums;

public enum VoucherType {
    PRODUCT_VOUCHER,
    SHOP_VOUCHER
}
