package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherOrder;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface VoucherOrderRepository extends JpaRepository<VoucherOrder, String> {
    @Query("SELECT v FROM VoucherOrder v WHERE v.voucher.id=:voucherId AND v.order.id=:orderId")
    VoucherOrder getByVoucherIdAndOrderId(@Param("voucherId") String voucherId, @Param("orderId") String orderId);

    @Query("SELECT v FROM VoucherOrder vo INNER JOIN Voucher v ON vo.voucher.id = v.id WHERE vo.order.id=:orderId AND v.hidden=false")
    List<Voucher> getVoucherByOrder(@Param("orderId") String orderId);

    @Query("SELECT v FROM VoucherOrder v WHERE v.voucher.id=:id")
    List<VoucherOrder> getVoucherOrderByVoucherId(@Param("id") String id);

    @Query("SELECT o FROM VoucherOrder vo INNER JOIN Order o ON vo.order.id = o.id WHERE vo.voucher.id=:voucherId")
    List<Order> getOrderByVoucher(@Param("voucherId") String voucherId);
}
