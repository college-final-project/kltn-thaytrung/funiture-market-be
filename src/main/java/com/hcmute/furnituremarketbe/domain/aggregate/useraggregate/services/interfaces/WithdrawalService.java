package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdateWithdrawMoneyStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.WithdrawMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface WithdrawalService {
    ResponseBaseAbstract getAllWithdrawal();

    ResponseBaseAbstract createWithdrawMoneyRequest(User user, WithdrawMoneyRequest request);

    ResponseBaseAbstract updateWithdrawalStatus(UpdateWithdrawMoneyStatus request);
}
