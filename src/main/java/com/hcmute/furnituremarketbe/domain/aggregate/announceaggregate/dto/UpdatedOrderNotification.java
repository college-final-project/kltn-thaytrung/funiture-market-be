package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.OrderResponse;
import lombok.Data;

@Data
public class UpdatedOrderNotification {
    private OrderResponse orderResponse;
    private String message;

    public UpdatedOrderNotification(OrderResponse orderResponse, String message) {
        this.orderResponse = orderResponse;
        this.message = message;
    }
}
