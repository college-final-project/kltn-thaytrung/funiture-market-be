package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.MarketingPacket;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class MarketingProductResponse {
    private String id;
    private Date startDate;
    private Date endDate;
    private MarketingPacket packet;
    private List<String> keywords;
    private List<String> productIds;

    public MarketingProductResponse(MarketingProduct marketingProduct) {
        this.id = marketingProduct.getId();
        this.startDate = marketingProduct.getStartDate();
        this.endDate = marketingProduct.getEndDate();
        this.packet = marketingProduct.getPacket();
        this.keywords = marketingProduct.getKeywords();
    }
}
