package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums;

public enum TransactionType {
    CHARGE,
    WITHDRAW,
    PAY,
    ORDER_REFUND,
    ORDER_CANCEL,
    ORDER_INCOME
}
