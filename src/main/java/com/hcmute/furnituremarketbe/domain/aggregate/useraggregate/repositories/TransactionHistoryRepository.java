package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface TransactionHistoryRepository extends JpaRepository<TransactionHistory, String> {
    @Query("SELECT t FROM TransactionHistory t WHERE t.wallet.id=:id")
    List<TransactionHistory> getTransactionHistoryByWallet(@Param("id") String id);
}
