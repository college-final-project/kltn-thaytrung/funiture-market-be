package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReviewRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.GetListReviewByReplyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.ReplyReviewRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.RepliedValue;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

import java.util.Map;

public interface StoreReviewService {
    ResponseBaseAbstract createReview(User buyer, CreateReviewRequest request);

    ResponseBaseAbstract replyReview(ReplyReviewRequest request);

    ResponseBaseAbstract searchFilterStoreReview(Map<String, String> queries);

    ResponseBaseAbstract searchFilterProductReview(Map<String, String> queries);

    ResponseBaseAbstract getListReviewByReply(RepliedValue isReply, Integer currentPage, Integer pageSize, String sort);
}
