package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.District;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderItem;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.DistrictRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderItemRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.AddToStoreCategoryListOfProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ShortStoreResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreCategory;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.SystemCategory;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.*;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerViewHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.BuyerFavouriteProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.BuyerViewHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.BuyerViewHistoryService;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private StoreCategoryRepository storeCategoryRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private StoreCategoryRepository categoryRepository;

    @Autowired
    private SystemCategoryRepository systemCategoryRepository;

    @Autowired
    private OrderItemRepository orderItemRepository;

    @Autowired
    private BuyerFavouriteProductRepository buyerFavouriteProductRepository;

    @Autowired
    private MarketingProductRepository marketingProductRepository;

    @Autowired
    private BuyerViewHistoryService buyerViewHistoryService;

    @Autowired
    private BuyerViewHistoryRepository buyerViewHistoryRepository;

    @Autowired
    private RecommendationSystemService recommendationSystemService;

    @Override
    public ResponseBaseAbstract createProduct(CreateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException {
        Product product = new Product();
        product.setId(UUID.randomUUID().toString());
        product.setName(request.getName());
        product.setPrice(request.getPrice());
        if (request.getSalePrice() != null) {
            product.setSalePrice(request.getSalePrice());
            product.setOnSale(true);
        }
        else
            product.setOnSale(false);
        product.setOnDisplay(true);
        product.setUsed(request.isUsed());
        product.setDescription(request.getDescription());
        product.setWeight(request.getWeight());
        product.setHeight(request.getHeight());
        product.setLength(request.getLength());
        product.setWidth(request.getWidth());
        product.setMaterial(request.getMaterial());
        product.setSold(0);
        product.setInStock(request.getInStock());
        product.setFeatured(request.isFeatured());
        product.setTotalReviewPoint(0);
        product.setReviewAmount(0);
        product.setAvgPoint(0.0);
        product.setHidden(false);
        if (request.getSystemCategoryId() != null) {
            SystemCategory category = this.systemCategoryRepository.getCategoryById(request.getSystemCategoryId());
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + request.getSystemCategoryId());
            product.setCategory(category);
        }

        if (request.getStoreCategoryId() != null) {
            StoreCategory category = this.categoryRepository.getCategoryById(request.getStoreCategoryId());
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + request.getStoreCategoryId());
            List<String> storeCategories = new ArrayList<>();
            storeCategories.add(category.getName());
            product.setStoreCategories(storeCategories);
        }

        Store store = this.storeRepository.getStoreById(request.getStoreId());
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }

        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        product.setStore(store);

        String thumbnailName = this.fileStorageService.save(thumbnail);
        product.setThumbnail(thumbnailName);

        List<String> imageUrls = new ArrayList<>();
        List<String> imageNames = new ArrayList<>();
        for (MultipartFile image : images) {
            String imageName = this.fileStorageService.save(image);
            imageNames.add(imageName);
            imageUrls.add(this.fileStorageService.getImageUrl(imageName));
        }
        product.setImages(imageNames);
        this.productRepository.save(product);
        ProductResponse data = new ProductResponse(product);
        data.setImages(imageUrls);
        data.setThumbnail(fileStorageService.getImageUrl(data.getThumbnail()));
        return SuccessResponse.builder().addMessage("Tạo dữ liệu thành công").setData(data).returnCreated();
    }

    @Override
    public ResponseBaseAbstract getProductByStoreCategoryName(String categoryName) {
        Map<String, String> queries = new HashMap<>();
        queries.put("storeCategories.contains",categoryName);
        List<Product> productList = this.productRepository.searchProduct(queries).getContent();
        List<ProductResponse> data = productList.stream().map(
                ProductResponse::new
        ).toList();
        if (data.size() > 0)
            data = data.stream().filter(p -> !p.isHidden()).toList();
        generateImageUrls(data);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateProduct(UpdateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException {
        String id = request.getId();
        String storeId = request.getStoreId();
        String name = request.getName();
        Double price = request.getPrice();
        Double salePrice = request.getSalePrice();
        String description = request.getDescription();
        Integer weight = request.getWeight();
        Integer height = request.getHeight();
        Integer length = request.getLength();
        Integer width = request.getWidth();
        String material = request.getMaterial();
        Integer inStock = request.getInStock();
        Boolean onSale = request.getOnSale();
        Boolean onDisplay = request.getOnDisplay();
        Boolean used = request.getUsed();
        Boolean featured = request.getFeatured();
        String systemCategoryId = request.getSystemCategoryId();
        String storeCategoryId = request.getStoreCategoryId();

        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+id);
        }
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        if (name != null)
            product.setName(name);
        if (price != null)
            product.setPrice(price);
        if (salePrice != null)
            product.setSalePrice(salePrice);
        if (description != null)
            product.setDescription(description);
        if (weight != null)
            product.setWeight(weight);
        if (height != null)
            product.setHeight(height);
        if (length != null)
            product.setLength(length);
        if (width != null)
            product.setWidth(width);
        if (material != null)
            product.setMaterial(material);
        if (inStock != null)
            product.setInStock(inStock);
        if (onSale != null) {
            product.setOnSale(onSale);
            if (!onSale) {
                product.setSalePrice(null);
            }
        }
        if (onDisplay != null)
            product.setOnDisplay(onDisplay);
        if (used != null)
            product.setUsed(used);
        if (featured != null)
            product.setFeatured(featured);
        if (systemCategoryId != null) {
            SystemCategory category = this.systemCategoryRepository.getCategoryById(systemCategoryId);
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + systemCategoryId);
            product.setCategory(category);
        }
        if (storeCategoryId != null) {
            StoreCategory category = this.categoryRepository.getCategoryById(storeCategoryId);
            if (category == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy category với id = " + storeCategoryId);
            List<String> storeCategories = product.getStoreCategories();
            storeCategories.replaceAll(c -> c.equals(category.getName()) ? category.getName() : c);
            product.setStoreCategories(storeCategories);
        }
        else {
            product.setStoreCategories(null);
        }
        if (thumbnail != null)
            product.setThumbnail(this.fileStorageService.save(thumbnail));
        if (images != null) {
            List<String> imageList = new ArrayList<>();
            for (MultipartFile image : images)
                imageList.add(this.fileStorageService.save(image));
            product.setImages(imageList);
        }
        this.productRepository.save(product);
        ProductResponse data = new ProductResponse(product);
        List<String> imageUrls = product.getImages().stream().map(image -> this.fileStorageService.getImageUrl(image)).toList();
        data.setImages(imageUrls);
        data.setThumbnail(this.fileStorageService.getImageUrl(product.getThumbnail()));
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract deleteProduct(String toCheckStoreId, String id) throws IOException {
        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+id);
        }
        String storeId = product.getStore().getId();
        if (!toCheckStoreId.equals(storeId))
            throw ServiceExceptionFactory.badRequest().addMessage("Không thể xóa sản phẩm của cửa hàng khác");
        OrderItem orderItem = this.orderItemRepository.getOrderItemByProductId(id);
        if (orderItem == null) {
            this.fileStorageService.delete(product.getThumbnail());
            for (String image : product.getImages()) {
                this.fileStorageService.delete(image);
            }
        }
        else {
            orderItem.setProduct(null);
            this.orderItemRepository.save(orderItem);
        }

        this.productRepository.delete(product);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract getAllStoreProduct(String storeId) {
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        List<Product> productList = store.getProducts();
        List<ProductResponse> data = productList.stream().map(
                ProductResponse::new
        ).toList();
        generateImageUrls(data);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllProduct() {
        Pageable pageable = PageRequest.of(0, 12);
        Page<Product> productPage = this.productRepository.findAll(pageable);
        List<Product> productList = productPage.getContent();
        if (productList.size() > 0)
            productList = productList.stream().filter(p -> !p.isHidden()).toList();
        List<ProductResponse> responses = preProcess(null, productList);
        generateImageUrls(responses);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    private void generateImageUrls(List<ProductResponse> responses) {
        for (ProductResponse response : responses) {
            response.setThumbnail(this.fileStorageService.getImageUrl(response.getThumbnail()));
            List<String> images = response.getImages();
            images = images.stream().map(image -> this.fileStorageService.getImageUrl(image)).toList();
            response.setImages(images);
        }
    }

    @Override
    public ResponseBaseAbstract getProductById(User buyer, String id) {
        if (buyer != null)
            this.buyerViewHistoryService.updateBuyerViewHistory(buyer.getId(), id);
        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + id);
        }
        ShortStoreResponse storeInfo = getShortStoreResponse(product);
        ProductResponse data = new ProductResponse(product);
        List<String> images = data.getImages();

        for (int i = 0;i < images.size();i++) {
            images.set(i, this.fileStorageService.getImageUrl(images.get(i)));
        }
        data.setImages(images);
        data.setThumbnail(this.fileStorageService.getImageUrl(data.getThumbnail()));
        data.setStoreInfo(storeInfo);
        data.setFavourite(buyer != null && checkFavourite(data.getId(), buyer.getId()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getMostBuyingProducts(User buyer, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Product> productPage = this.productRepository.getMostBuyingProducts(pageable);
        int totalRecords = (int)productPage.getTotalElements();
        List<Product> products = productPage.getContent();
        List<ProductResponse> productResponses = preProcess(buyer, products);
        generateImageUrls(productResponses);
        PagingAndSortingResponse<ProductResponse> data = new PagingAndSortingResponse<>
                (productResponses, currentPage, pageSize, totalRecords,productPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getMostFavouriteProducts(User buyer, Integer currentPage, Integer pageSize) {
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Product> productPage = this.productRepository.getMostFavouriteProducts(pageable);
        List<Product> products = productPage.getContent();
        List<ProductResponse> productResponses = preProcess(buyer, products);
        generateImageUrls(productResponses);
        PagingAndSortingResponse<ProductResponse> data = new PagingAndSortingResponse<>
                (productResponses, currentPage, pageSize, (int)productPage.getTotalElements(),productPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllProductIsMarketing(User buyer, Integer currentPage, Integer pageSize) {
        Date givenDate = new Date();
        Pageable pageable = PageRequest.of(currentPage, pageSize);
        Page<Product> productPage = this.productRepository.findProductsByMarketing(givenDate, pageable);
        int totalRecords = (int)productPage.getTotalElements();
        List<Product> products = productPage.getContent();
        List<ProductResponse> marketingProductResponses = preProcess(buyer, products);
        if (marketingProductResponses.size() != 0)
            marketingProductResponses.sort(Comparator.comparingDouble(p -> p.getStoreInfo().getAvgReviewStar()));
        generateImageUrls(marketingProductResponses);
        PagingAndSortingResponse<ProductResponse> data = new PagingAndSortingResponse<>
                (marketingProductResponses, currentPage, pageSize, totalRecords,productPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchAndFilterProduct(User buyer, Map<String, String> queries) {
        queries.put("hidden.equals","false");
        PagingAndSortingResponse<Product> productPage = this.productRepository.searchProduct(queries);
        String keyword = "";
        int totalRecords = productPage.getTotalRecords();
        for (Map.Entry<String, String> entry : queries.entrySet()) {
            String key = entry.getKey();
            if (key.contains("name")) {
                keyword = entry.getValue();
                break;
            }
        }
        Map<String, String> marketingQueries = new HashMap<>();
        marketingQueries.put("keywords.contains",keyword);
        Date givenDate = new Date();
        List<MarketingProduct> mP = new ArrayList<>();
        if (!keyword.equals(""))
            mP = this.marketingProductRepository.searchMarketingProduct(marketingQueries).getContent();
        List<MarketingProduct> filterMP = mP.stream().filter(m -> m.getStartDate().before(givenDate) && m.getEndDate().after(givenDate)).toList();
        List<Product> marketingProducts = new ArrayList<>();
        for (MarketingProduct m : filterMP) {
            List<Product> mProduct = m.getProducts();
            marketingProducts.addAll(mProduct);
        }
        List<Product> productList = productPage.getContent();
        productList.removeAll(marketingProducts);
        List<ProductResponse> productResponses = preProcess(buyer, productList);
        if (productResponses.size() != 0)
        {
            productResponses.sort(Comparator.comparingInt(ProductResponse::getNumFav));
            Collections.reverse(productResponses);
        }

        List<ProductResponse> marketingProductResponses = preProcess(buyer, marketingProducts);
        if (marketingProductResponses.size() != 0)
            marketingProductResponses.sort(Comparator.comparingDouble(p -> p.getStoreInfo().getAvgReviewStar()));
        for (ProductResponse p : marketingProductResponses) {
            productResponses.add(0, p);
        }

        double pages = totalRecords / Double.valueOf(productPage.getPageSize());
        int totalPages = (int) pages;
        if (pages != (double) totalPages)
            totalPages += 1;
        generateImageUrls(productResponses);
        PagingAndSortingResponse<ProductResponse> data =
                new PagingAndSortingResponse<>(productResponses, productPage.getCurrentPage(), productPage.getPageSize(), totalRecords,totalPages);
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getRecommendationProducts(User buyer, Integer offset, Integer limit, boolean isExplicit) {
        List<Product> products = new ArrayList<>();
        if (buyer != null) {
            var ids = this.recommendationSystemService.getRecommendationProducts(
                    buyer.getId(),
                    offset,
                    limit,
                    isExplicit);
            if (ids.size() == 0) {
                Pageable pageable = PageRequest.of(offset, limit);
                Page<Product> productPage = this.productRepository.fetchRandomProduct(pageable);
                products = productPage.getContent();
            }
            else {
                Set<String> productIds = new HashSet<>(ids);
                for (String productId : productIds) {
                    Product product = this.productRepository.getProductById(productId);
                    products.add(product);
                }
            }
        }
        else {
            Pageable pageable = PageRequest.of(offset, limit);
            Page<Product> productPage = this.productRepository.fetchRandomProduct(pageable);
            products = productPage.getContent();
        }
        List<ProductResponse> data = preProcess(buyer, products);
        generateImageUrls(data);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract addToStoreCategoryListOfProduct(AddToStoreCategoryListOfProductRequest request) {
        String id = request.getProductId();
        Product product = this.productRepository.getProductById(id);
        if (product == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + id);
        }
        String categoryId = request.getStoreCategoryId();
        StoreCategory category = this.storeCategoryRepository.getCategoryById(categoryId);
        if (category == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy danh mục với id = " + categoryId);
        }
        List<String> storeCategories = product.getStoreCategories();
        List<String> toUpdateStoreCategories = new ArrayList<>();
        for (String cate : storeCategories) {
            if (!cate.equals(""))
                toUpdateStoreCategories.add(cate);
        }
        toUpdateStoreCategories.add(category.getName());
        product.setStoreCategories(toUpdateStoreCategories);
        this.productRepository.save(product);
        ProductResponse data = new ProductResponse(product);
        List<String> images = data.getImages();

        for (int i = 0;i < images.size();i++) {
            images.set(i, this.fileStorageService.getImageUrl(images.get(i)));
        }
        data.setImages(images);
        data.setThumbnail(this.fileStorageService.getImageUrl(data.getThumbnail()));
        return SuccessResponse.builder()
                .addMessage("Thêm sản phẩm vào danh mục thành công")
                .setData(data)
                .returnUpdated();
    }

    private ShortStoreResponse getShortStoreResponse(Product product) {
        Store store = product.getStore();
        List<Product> productList = store.getProducts();
        Double totalAvgStars = 0.0;
        Integer count = 0;
        for (Product p : productList) {
            if (p.getReviewAmount() != 0) {
                Double avgStar = p.getTotalReviewPoint().doubleValue() / p.getReviewAmount();
                totalAvgStars += avgStar;
                count += 1;
            }
        }
        Integer reviewAmount = productList.stream().mapToInt(Product::getReviewAmount).sum();
        Double avgReviewStar = totalAvgStars / count;
        ShortStoreResponse response = new ShortStoreResponse(store, avgReviewStar, reviewAmount, productList.size());
        response.setLogo(this.fileStorageService.getImageUrl(response.getLogo()));
        return response;
    }

    private boolean checkFavourite(String productId, String buyerId) {
        return this.buyerFavouriteProductRepository.existByBuyerIdAndProductId(productId, buyerId);
    }

    private List<ProductResponse> preProcess(User buyer, List<Product> products) {
        List<ProductResponse> productResponses = new ArrayList<>();
        for (Product product : products) {
            List<BuyerViewHistory> viewHistories = this.buyerViewHistoryRepository.getViewHistoryByProductId(product.getId());
            Integer views;
            if (viewHistories != null && viewHistories.size() != 0)
                views = viewHistories.stream().mapToInt(BuyerViewHistory::getCount).sum();
            else
                views = 0;
            ShortStoreResponse storeInfo = getShortStoreResponse(product);
            ProductResponse productResponse = new ProductResponse(product);
            productResponse.setStoreInfo(storeInfo);
            productResponse.setFavourite(buyer != null && checkFavourite(productResponse.getId(), buyer.getId()));
            productResponse.setViews(views);
            productResponses.add(productResponse);
        }
        return productResponses;
    }
}
