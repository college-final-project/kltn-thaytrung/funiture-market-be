package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import lombok.Data;

import java.util.Date;

@Data
public class NumOfOrderByDayResponse {
    private Date date;
    private Integer amount;
}
