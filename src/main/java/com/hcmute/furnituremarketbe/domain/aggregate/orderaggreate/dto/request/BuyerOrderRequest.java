package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.PaymentType;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class BuyerOrderRequest {
    private String deliveryAddressId;
    private PaymentType paymentType;
    private List<CreateOrderRequest> orderList = new ArrayList<>();
}
