package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateOrderRefundRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.OrderRefundResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.RefundComplaint;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderRefundRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.OrderRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.RefundComplaintRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderRefundService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Service
public class OrderRefundServiceImpl implements OrderRefundService {
    @Autowired
    private OrderRefundRepository orderRefundRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderService orderService;

    @Autowired
    private AnnounceService announceService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private RefundComplaintRepository refundComplaintRepository;

    @Override
    public ResponseBaseAbstract getOrderRefundByStore(String storeId) {
        List<OrderRefund> orderRefunds = this.orderRefundRepository.findByStoreId(storeId);
        List<OrderRefundResponse> responses = new ArrayList<>();
        for (OrderRefund orderRefund : orderRefunds) {
            OrderRefundResponse response = getOrderRefundResponse(orderRefund);
            responses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreOrderRefundByAccepted(String storeId, Boolean accepted) {
        List<OrderRefund> orderRefunds = this.orderRefundRepository.getStoreOrderRefundByAccepted(storeId, accepted);
        List<OrderRefundResponse> responses = new ArrayList<>();
        for (OrderRefund orderRefund : orderRefunds) {
            OrderRefundResponse response = getOrderRefundResponse(orderRefund);
            responses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract createOrderRefund(CreateOrderRefundRequest request, List<MultipartFile> images) {
        String reason = request.getReason();
        String orderId = request.getOrderId();
        OrderRefund orderRefund = new OrderRefund();
        orderRefund.setId(UUID.randomUUID().toString());
        orderRefund.setReason(reason);
        Order order = this.orderRepository.getOrderById(orderId);
        if (order == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy đơn hàng với id = " + orderId);
        if (order.getStatus() != OrderStatus.COMPLETED)
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Trạng thái đơn hàng không hợp lệ");
        orderRefund.setOrder(order);
        orderRefund.setAccepted(false);
        List<String> imageNames = images.stream().map(image -> {
            try {
                return this.fileStorageService.save(image);
            } catch (IOException e) {
                e.printStackTrace();
            }
            return " ";
        }).toList();
        orderRefund.setImages(imageNames);
        this.orderRefundRepository.save(orderRefund);
        OrderRefundResponse response = getOrderRefundResponse(orderRefund);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(response)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract doneOrderRefund(String orderRefundId, Boolean accepted) {
        OrderRefund orderRefund = this.orderRefundRepository.getOrderRefundById(orderRefundId);
        if (orderRefund == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy yêu cầu hoàn tiền với id = " + orderRefundId);
        orderRefund.setAccepted(accepted);
        this.orderRefundRepository.save(orderRefund);
        RefundComplaint refundComplaint = this.refundComplaintRepository.getRefundComplaintByOrderRefund(orderRefundId);
        refundComplaint.setDone(true);
        this.refundComplaintRepository.save(refundComplaint);
        if (accepted) {
            this.orderService.refundOrder(orderRefund.getOrder().getId());
            return SuccessResponse.builder()
                    .addMessage("Đồng ý hoàn đơn")
                    .setData(getOrderRefundResponse(orderRefund))
                    .returnUpdated();
        }
        this.announceService.onDeclineOrderRefund(orderRefund);
        return SuccessResponse.builder()
                .addMessage("Từ chối hoàn đơn")
                .setData(getOrderRefundResponse(orderRefund))
                .returnUpdated();
    }

    private OrderRefundResponse getOrderRefundResponse(OrderRefund orderRefund) {
        OrderRefundResponse response = new OrderRefundResponse(orderRefund);
        List<String> imageUrls = response.getImages().stream().map(i -> this.fileStorageService.getImageUrl(i)).toList();
        response.setImages(imageUrls);
        return response;
    }
}
