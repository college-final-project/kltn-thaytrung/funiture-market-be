package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class UpdateStoreInfoRequest {
    private String shopName;
    private String ownerName;
    private String address;
    private MultipartFile logo;
    private MultipartFile topBanner;
    private MultipartFile infoBanner;
    private String description;
}
