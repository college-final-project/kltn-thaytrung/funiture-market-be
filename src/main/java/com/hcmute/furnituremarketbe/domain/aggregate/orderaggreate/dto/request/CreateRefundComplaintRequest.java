package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import lombok.Data;

@Data
public class CreateRefundComplaintRequest {
    private String reason;
    private String orderRefundId;
}
