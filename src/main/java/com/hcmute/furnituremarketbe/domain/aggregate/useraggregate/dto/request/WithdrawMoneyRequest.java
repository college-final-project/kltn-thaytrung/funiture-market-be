package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import lombok.Data;

@Data
public class WithdrawMoneyRequest {
    private Double value;
    private String accountNumber;
    private String bankName;
    private String ownerName;
}
