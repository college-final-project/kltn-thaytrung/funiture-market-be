package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import lombok.Data;

import java.util.List;

@Data
public class BuyerStoreResponse {
    private String id;
    private String shopName;
    private String address;
    private String logo;
    private String description;
    private boolean breakStatus;
    private Integer numFollower;
    private Integer numFollowing;
    private String topBanner;
    private String infoBanner;
    private Double avgReviewStar;
    private Integer numReview;
    private Integer productAmount;
    private boolean followed;

    public BuyerStoreResponse(Store store, Integer numFollower, Integer numFollowing, Double avgReviewStar, Integer numReview, Integer productAmount, boolean followed) {
        this.id = store.getId();
        this.shopName = store.getShopName();
        this.address = store.getAddress();
        this.logo = store.getLogo();
        this.description = store.getDescription();
        this.breakStatus = store.isBreakStatus();
        this.numFollower = numFollower;
        this.numFollowing = numFollowing;
        this.topBanner = store.getTopBanner();
        this.infoBanner = store.getInfoBanner();
        this.avgReviewStar = avgReviewStar;
        this.numReview = numReview;
        this.productAmount = productAmount;
        this.followed = followed;
    }
}
