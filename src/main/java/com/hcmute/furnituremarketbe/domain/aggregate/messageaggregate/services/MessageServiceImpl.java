package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.entity.Message;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.enums.ChatMessageOneToOneType;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageOneToOne;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.repositories.MessageRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.services.interfaces.MessageService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private UserRepository userRepository;

    @Override
    public ResponseBaseAbstract getListMessageWithOnePerson(User user, String opponentId) {
        User receiver = this.userRepository.getUserById(opponentId);
        if (receiver == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy người dùng với id = "+opponentId);
        }
        List<Message> rawMessages = this.messageRepository.getAllMessagesBetweenTwoPeople(user.getId(), opponentId);
        rawMessages.sort(Comparator.comparing(Message::getCreatedAt));
        List<ChatMessageResponse> messages = rawMessages.stream().map(m -> new ChatMessageResponse(
                m.getSender().getId(),
                m.getReceiver().getId(),
                m
        )).toList();

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(messages)
                .returnGetOK();
    }

    @Override
    public ChatMessageResponse storeMessage(ChatMessageOneToOne message) {
        Message msg = new Message();
        msg.setId(UUID.randomUUID().toString());
        msg.setSender(this.entityManager.getReference(User.class, message.getSenderId()));
        msg.setReceiver(this.entityManager.getReference(User.class, message.getReceiverId()));
        msg.setCreatedAt(message.getCreatedAt());
        msg.setIsRead(false);

        if (message.getType() == ChatMessageOneToOneType.MESSAGE) {
            msg.setContent(message.getMessage());
            msg.setLastUpdatedAt(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
        }
        else if (message.getType() == ChatMessageOneToOneType.IMAGE) {
            msg.setContent("img:"+message.getImage());
        }
        this.messageRepository.save(msg);
        if (message.getType() == ChatMessageOneToOneType.SEEN) {
            String messageId = message.getMessageId();
            Message seenMessage = this.messageRepository.getMessageById(messageId);
            if (seenMessage != null) {
                seenMessage.setIsRead(true);
                seenMessage.setLastUpdatedAt(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
                this.messageRepository.save(seenMessage);
                return new ChatMessageResponse(message.getSenderId(), message.getReceiverId(), seenMessage);
            }
        }
        return new ChatMessageResponse(message.getSenderId(), message.getReceiverId(), msg);
    }

    @Override
    public ResponseBaseAbstract deleteMessage(String messageId) {
        Message message = this.messageRepository.getMessageById(messageId);
        if (message == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy tin nhắn có id = "+messageId);
        }
        message.setDeletedAt(new Date());
        return SuccessResponse.builder()
                .addMessage("Xóa Tin nhắn thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract getAllOpponents(String userId) {
        List<User> userList = this.messageRepository.getAllOpponents(userId);
        List<UserResponse> responses = userList.stream().map(UserResponse::new).toList();
        for (UserResponse response : responses) {
            response.setAvatar(this.generateAvatarUrl(response.getAvatar()));
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(responses)
                .returnGetOK();
    }

    private String generateAvatarUrl(String avatar) {
        if (avatar.startsWith("https")) {
            return avatar;
        }
        else
            return this.fileStorageService.getImageUrl(avatar);
    }

}
