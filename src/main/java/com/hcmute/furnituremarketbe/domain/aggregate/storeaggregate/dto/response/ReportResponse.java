package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import lombok.Data;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class ReportResponse {
    private String id;
    private String reason;
    private String description;
    private ReportStatus status;
    private ReportType type;
    private String reporterName;
    private Date createdAt;
    private Date updatedAt;
    private Object objectInfo;
    private List<ReportExplanationResponse> explanations = new ArrayList<>();

    public ReportResponse(Report report) {
        this.id = report.getId();
        this.reason = report.getReason();
        this.description = report.getDescription();
        this.status = report.getStatus();
        this.type = report.getType();
        this.reporterName = report.getReporterName();
        this.createdAt = report.getCreatedAt();
        this.updatedAt = report.getUpdatedAt();
    }
}
