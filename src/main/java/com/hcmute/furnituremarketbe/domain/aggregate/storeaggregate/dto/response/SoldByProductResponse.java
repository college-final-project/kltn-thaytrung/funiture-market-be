package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import lombok.Data;

@Data
public class SoldByProductResponse {
    private String productId;
    private String productName;
    private Integer soldCount;

    public SoldByProductResponse(String productId, String productName, Integer soldCount) {
        this.productId = productId;
        this.productName = productName;
        this.soldCount = soldCount;
    }
}
