package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.marketing.MarketingProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend.MarketingProductExtendRepository;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface MarketingProductRepository extends JpaRepository<MarketingProduct, String>, MarketingProductExtendRepository {
    @Query("SELECT m FROM MarketingProduct m WHERE m.store.id=:id")
    List<MarketingProduct> getMarketingProductByStore(@Param("id") String id);

    @Query("SELECT m FROM MarketingProduct m WHERE FUNCTION('DATE', m.startDate) <= FUNCTION('DATE', :endDate) AND FUNCTION('DATE', m.startDate) >= FUNCTION('DATE', :startDate)")
    List<MarketingProduct> getMarketingProductOfMonth(@Param("endDate")Date endDate, @Param("startDate") Date startDate);

    @Query("SELECT m FROM MarketingProduct m WHERE FUNCTION('DATE', m.startDate) = FUNCTION('DATE', :givenDate)")
    List<MarketingProduct> getMarketingProductByDay(@Param("givenDate") Date givenDate);
}
