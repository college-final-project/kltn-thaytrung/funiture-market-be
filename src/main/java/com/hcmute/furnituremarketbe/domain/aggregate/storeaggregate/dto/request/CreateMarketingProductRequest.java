package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.MarketingPacket;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CreateMarketingProductRequest {
    private MarketingPacket packet;
    private List<String> keywords = new ArrayList<>();
    private List<String> productIds = new ArrayList<>();
}
