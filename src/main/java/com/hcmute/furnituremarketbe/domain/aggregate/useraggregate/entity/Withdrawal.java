package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.WithdrawStatus;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Data
public class Withdrawal {
    @Id
    private String id;

    @Column(name = "value")
    private Double value;

    @Enumerated(EnumType.STRING)
    private WithdrawStatus status;

    @Column(name = "account_number")
    private String accountNumber;

    @Column(name = "bank_name")
    private String bankName;

    @Column(name = "owner_name")
    private String ownerName;

    @Column(name = "created_at")
    private ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Column(name = "updated_at")
    private ZonedDateTime updatedAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Wallet wallet;
}
