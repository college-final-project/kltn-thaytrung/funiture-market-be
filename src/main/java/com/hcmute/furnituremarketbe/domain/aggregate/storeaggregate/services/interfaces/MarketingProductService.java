package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateMarketingProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.MarketingStatisticResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface MarketingProductService {
    ResponseBaseAbstract createMarketingProduct(User seller, CreateMarketingProductRequest request);

    ResponseBaseAbstract getAllMarketingProduct(Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getStoreMarketingProducts(User seller);

    MarketingStatisticResponse getMarketingProductStatistic(Integer month, Integer year);
}
