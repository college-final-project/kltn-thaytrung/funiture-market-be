package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.OrderStoreResponse;
import lombok.Data;

import java.util.*;

@Data
public class OrderDetailResponse {
    private String id;
    private Date createdAt;
    private OrderStatus currentStatus;
    private BuyerDeliveryAddressResponse deliveryAddress;
    private Double shippingFee;
    private Double voucherDiscount;
    private Double total;
    private Boolean paid;
    private List<StatusWithDate> statusWithDates = new ArrayList<>();
    List<OrderItemResponse> orderItemList = new ArrayList<>();
    private OrderStoreResponse storeInfo;

    public OrderDetailResponse(Order order) {
        this.id = order.getId();
        this.createdAt = order.getCreatedAt();
        this.currentStatus = order.getStatus();
        this.total = order.getTotal();
        this.shippingFee = order.getShippingFee();
        this.voucherDiscount = order.getVoucherDiscount();
        this.paid = order.isPaid();
    }
}
