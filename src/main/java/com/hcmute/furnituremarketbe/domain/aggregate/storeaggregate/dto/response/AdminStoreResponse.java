package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import lombok.Data;

import java.util.List;

@Data
public class AdminStoreResponse {
    private String id;
    private String shopName;
    private String ownerName;
    private String address;
    private String logo;
    private String description;
    private List<String> tax;
    private List<String> identifier;
    private boolean breakStatus;
    private Integer numFollower;
    private Integer numFollowing;
    private String topBanner;
    private String infoBanner;
    private Double avgReviewStar;
    private Integer numReview;

    public AdminStoreResponse(Store store, String ownerName, Integer numFollower, Integer numFollowing, Double avgReviewStar, Integer numReview) {
        this.id = store.getId();
        this.shopName = store.getShopName();
        this.ownerName = ownerName;
        this.address = store.getAddress();
        this.logo = store.getLogo();
        this.description = store.getDescription();
        this.tax = store.getTax();
        this.identifier = store.getIdentifier();
        this.breakStatus = store.isBreakStatus();
        this.numFollower = numFollower;
        this.numFollowing = numFollowing;
        this.topBanner = store.getTopBanner();
        this.infoBanner = store.getInfoBanner();
        this.avgReviewStar = avgReviewStar;
        this.numReview = numReview;
    }
}
