package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface DeliveryAddressService {
    ResponseBaseAbstract getAllProvinceCity();

    ResponseBaseAbstract getDistrictByProvince(Integer id);

    ResponseBaseAbstract getWardByDistrict(Integer id);

    ResponseBaseAbstract createDeliveryAddress(User user, CreateDeliveryAddressRequest request);

    ResponseBaseAbstract updateDeliveryAddress(User user, UpdateDeliveryAddressRequest request);

    ResponseBaseAbstract deleteDeliveryAddress(User user, String deliveryAddressId);

    ResponseBaseAbstract getAllDeliveryAddressByUser(User user);

    ResponseBaseAbstract updateDefaultAddress(User user, UpdateDefaultAddressRequest request);
}
