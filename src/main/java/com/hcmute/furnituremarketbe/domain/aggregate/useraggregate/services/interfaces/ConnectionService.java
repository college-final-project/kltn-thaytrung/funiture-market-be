package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface ConnectionService {
    ResponseBaseAbstract getFollowers(String userId);

    ResponseBaseAbstract getFollowingPeople(String userId);

    ResponseBaseAbstract toggle(User user, String id);
}
