package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.BuyerFavouriteProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerFavouriteProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.BuyerFavouriteProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.BuyerFavouriteProductService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
public class BuyerFavouriteProductServiceImpl implements BuyerFavouriteProductService {
    @Autowired
    private BuyerFavouriteProductRepository buyerFavouriteProductRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseBaseAbstract toggleFavouriteProduct(String buyerId, String productId) {
        User buyer = this.userRepository.getUserById(buyerId);
        if (buyer == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy tài khoản với id = " + buyerId);
        Product product = this.productRepository.getProductById(productId);
        if (product == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = " + productId);
        BuyerFavouriteProduct b = this.buyerFavouriteProductRepository.getByBuyerIdAndProductId(productId,buyerId);
        if (b == null) {
            BuyerFavouriteProduct bfp = new BuyerFavouriteProduct();
            bfp.setId(UUID.randomUUID().toString());
            bfp.setProduct(product);
            bfp.setBuyer(buyer);
            this.buyerFavouriteProductRepository.save(bfp);
            return SuccessResponse.builder()
                    .addMessage("Đã yêu thích sản phẩm")
                    .returnGetOK();
        }
        else {
            this.buyerFavouriteProductRepository.delete(b);
            return SuccessResponse.builder()
                    .addMessage("Đã bỏ thích sản phẩm")
                    .returnGetOK();
        }
    }

    @Override
    public ResponseBaseAbstract getWishListByBuyer(String buyerId) {
        List<Product> wishList = this.buyerFavouriteProductRepository.getFavouriteProductByBuyerId(buyerId);
        List<BuyerFavouriteProductResponse> data = wishList.stream().map(product -> new BuyerFavouriteProductResponse(product)).toList();
        for (BuyerFavouriteProductResponse response : data) {
            response.setThumbnail(this.fileStorageService.getImageUrl(response.getThumbnail()));
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }
}
