package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import lombok.Data;

@Data
public class UpdateOrderStatusRequest {
    private String orderId;
    private OrderStatus status;
}
