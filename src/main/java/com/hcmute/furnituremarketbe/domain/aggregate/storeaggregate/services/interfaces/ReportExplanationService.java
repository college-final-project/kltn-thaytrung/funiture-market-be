package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportExplanationRequest;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface ReportExplanationService {
    ResponseBaseAbstract createReportExplanation(CreateReportExplanationRequest request, List<MultipartFile> images);
}
