package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface UserRepository extends JpaRepository<User, String> {
    @Query("SELECT u FROM User u WHERE u.email=:email")
    User getUserByEmail(@Param("email") String email);

    @Query("SELECT u FROM User u WHERE u.id=:id")
    User getUserById(@Param("id") String id);

    @Query("SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END FROM User u WHERE u.email=:email")
    boolean existsByEmail(@Param("email") String email);

    @Query("SELECT u FROM User u WHERE u.email=:email AND u.emailConfirmed = true ")
    User getUserByEmailAndEmailConfirmation(@Param("email") String email);

    @Query("SELECT u FROM User u WHERE u.role=:role")
    List<User> getUserByRole(@Param("role") UserRole role);
}
