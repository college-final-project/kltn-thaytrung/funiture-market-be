package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import lombok.Data;

@Data
public class UseVoucherResponse {
    private Double discount;
    private Double total;

    public UseVoucherResponse(Double discount, Double total) {
        this.discount = discount;
        this.total = total;
    }
}
