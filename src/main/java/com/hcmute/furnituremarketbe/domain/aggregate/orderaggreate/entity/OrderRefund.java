package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import jakarta.persistence.*;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class OrderRefund {
    @Id
    private String id;

    @Column(name = "reason")
    private String reason;

    @Column(name = "created_at")
    private ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Convert(converter = ListStringConverter.class)
    @Column(name = "images", columnDefinition = "TEXT")
    private List<String> images = new ArrayList<>();

    @Column(name = "accepted")
    private boolean accepted;

    @OneToOne
    @JoinColumn(name = "order_id", referencedColumnName = "id")
    private Order order;
}
