package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerViewHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.BuyerViewHistoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.BuyerViewHistoryService;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.UUID;

@Service
public class BuyerViewHistoryServiceImpl implements BuyerViewHistoryService {
    @Autowired
    private BuyerViewHistoryRepository buyerViewHistoryRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    @Transactional
    public void updateBuyerViewHistory(String buyerId, String productId) {
        User buyer = this.userRepository.getUserById(buyerId);
        if (buyer == null)
            throw ServiceExceptionFactory.notFound()
                .addMessage("Không tìm thấy người dùng với id = "+buyerId);
        Product product = this.productRepository.getProductById(productId);
        if (product == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+productId);
        BuyerViewHistory buyerViewHistory = this.buyerViewHistoryRepository.getBuyerViewHistoryByBuyerIdAndProductId(buyerId, productId);
        if (buyerViewHistory == null) {
            buyerViewHistory = new BuyerViewHistory();
            buyerViewHistory.setId(UUID.randomUUID().toString());
            buyerViewHistory.setBuyer(buyer);
            buyerViewHistory.setProduct(product);
            buyerViewHistory.setCount(1);
        }
        else {
            buyerViewHistory.setCount(buyerViewHistory.getCount() + 1);
        }
        this.buyerViewHistoryRepository.save(buyerViewHistory);
    }
}
