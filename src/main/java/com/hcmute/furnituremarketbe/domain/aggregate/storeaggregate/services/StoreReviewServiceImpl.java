package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReviewRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.GetListReviewByReplyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.ReplyReviewRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ProductReviewResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ShortProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.StoreReviewResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.RepliedValue;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreReviewRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreReviewService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

@Service
public class StoreReviewServiceImpl implements StoreReviewService {
    @Autowired
    private StoreReviewRepository storeReviewRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseBaseAbstract createReview(User buyer, CreateReviewRequest request) {
        String productId = request.getProductId();
        Product product = this.productRepository.getProductById(productId);
        if (product == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy sản phẩm với id = "+productId);
        product.setReviewAmount(product.getReviewAmount()+1);
        product.setTotalReviewPoint(product.getTotalReviewPoint() + request.getStar());
        product.setAvgPoint((double) (product.getTotalReviewPoint() / product.getReviewAmount()));
        this.productRepository.save(product);
        StoreReview storeReview = new StoreReview();
        storeReview.setId(UUID.randomUUID().toString());
        storeReview.setBuyer(buyer);
        storeReview.setContent(request.getContent());
        storeReview.setStar(request.getStar());
        storeReview.setStore(product.getStore());
        storeReview.setProduct(product);
        this.storeReviewRepository.save(storeReview);
        ProductReviewResponse response = new ProductReviewResponse(storeReview);
        UserResponse reviewer = new UserResponse(storeReview.getBuyer());
        reviewer.setAvatar(generateAvatarUrl(reviewer.getAvatar()));
        response.setReviewer(reviewer);
        return SuccessResponse.builder()
                .addMessage("Tạo đánh giá thành công")
                .setData(response)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract replyReview(ReplyReviewRequest request) {
        String reviewId = request.getReviewId();
        StoreReview review = this.storeReviewRepository.getStoreReviewById(reviewId);
        if (review == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy đánh giá với id = "+reviewId);
        review.setReply(request.getReplyContent());
        this.storeReviewRepository.save(review);
        StoreReviewResponse response = new StoreReviewResponse(review);
        UserResponse reviewer = new UserResponse(review.getBuyer());
        reviewer.setAvatar(generateAvatarUrl(reviewer.getAvatar()));
        response.setReviewer(reviewer);
        return SuccessResponse.builder()
                .addMessage("Trả lời đánh giá thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract searchFilterStoreReview(Map<String, String> queries) {
        PagingAndSortingResponse<StoreReview> storeReviewPage = this.storeReviewRepository.searchStoreReview(queries);
        List<StoreReview> reviews = storeReviewPage.getContent();
        List<StoreReviewResponse> reviewResponses = getStoreReviewResponses(reviews);
        PagingAndSortingResponse<StoreReviewResponse> data =
                new PagingAndSortingResponse<>(reviewResponses,storeReviewPage.getCurrentPage(),storeReviewPage.getPageSize(),storeReviewPage.getTotalRecords(),storeReviewPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchFilterProductReview(Map<String, String> queries) {
        PagingAndSortingResponse<StoreReview> storeReviewPage = this.storeReviewRepository.searchStoreReview(queries);
        List<StoreReview> reviews = storeReviewPage.getContent();
        List<ProductReviewResponse> reviewResponses = new ArrayList<>();
        for (int i = 0;i < reviews.size();i++) {
            ProductReviewResponse response = new ProductReviewResponse(reviews.get(i));
            UserResponse reviewer = new UserResponse(reviews.get(i).getBuyer());
            reviewer.setAvatar(generateAvatarUrl(reviewer.getAvatar()));
            response.setReviewer(reviewer);
            reviewResponses.add(response);
        }
        PagingAndSortingResponse<ProductReviewResponse> data =
                new PagingAndSortingResponse<>(reviewResponses,storeReviewPage.getCurrentPage(),storeReviewPage.getPageSize(),storeReviewPage.getTotalRecords(),storeReviewPage.getTotalPages());
        return SuccessResponse.builder().addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getListReviewByReply(RepliedValue isReply, Integer currentPage, Integer pageSize, String sort) {
        Pageable pageable;
        if (sort == null)
        pageable = PageRequest.of(currentPage, pageSize, Sort.by("id").ascending());
        else
        {
            String[] tokens = sort.split("\\.");
            String sortColumn = tokens[0];
            String sortType = tokens[1];
            if (sortType.equalsIgnoreCase("DESC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).descending());
            else if (sortType.equalsIgnoreCase("ASC"))
                pageable = PageRequest.of(currentPage, pageSize, Sort.by(sortColumn).ascending());
            else
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Sort type không hợp lệ");
        }
        Page<StoreReview> reviewPage;
        if (isReply == RepliedValue.TRUE)
            reviewPage = this.storeReviewRepository.getReplyReviews(pageable);
        else
            reviewPage = this.storeReviewRepository.getNotReplyReviews(pageable);
        List<StoreReview> reviews = reviewPage.getContent();
        List<StoreReviewResponse> reviewResponses = getStoreReviewResponses(reviews);
        Integer totalPages = reviewPage.getTotalPages();
        PagingAndSortingResponse<StoreReviewResponse> data =
                new PagingAndSortingResponse<>(reviewResponses,currentPage,pageSize,reviews.size(),totalPages);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data).returnGetOK();
    }

    private List<StoreReviewResponse> getStoreReviewResponses(List<StoreReview> reviews) {
        List<StoreReviewResponse> reviewResponses = new ArrayList<>();
        for (int i = 0;i < reviews.size();i++) {
            StoreReviewResponse response = new StoreReviewResponse(reviews.get(i));
            UserResponse reviewer = new UserResponse(reviews.get(i).getBuyer());
            reviewer.setAvatar(generateAvatarUrl(reviewer.getAvatar()));
            response.setReviewer(reviewer);

            ShortProductResponse productResponse = new ShortProductResponse(reviews.get(i).getProduct());
            productResponse.setThumbnail(this.fileStorageService.getImageUrl(productResponse.getThumbnail()));
            response.setProductInfo(productResponse);

            reviewResponses.add(response);
        }
        return reviewResponses;
    }

    private String generateAvatarUrl(String avatar) {
        if (avatar.startsWith("https")) {
            return avatar;
        }
        else
            return this.fileStorageService.getImageUrl(avatar);
    }
}
