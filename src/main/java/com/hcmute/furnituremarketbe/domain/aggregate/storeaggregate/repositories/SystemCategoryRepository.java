package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreCategory;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.SystemCategory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface SystemCategoryRepository extends JpaRepository<SystemCategory, String> {
    @Query("SELECT c FROM SystemCategory c WHERE c.id=:id")
    SystemCategory getCategoryById(@Param("id") String id);
}
