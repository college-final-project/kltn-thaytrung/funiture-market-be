package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.DeliveryAddress;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import jakarta.persistence.*;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

@Entity
@Data
public class User implements UserDetails {
    @Id
    private String id;

    @Column(name = "full_name")
    private String fullName;

    @Column(name = "email")
    private String email;

    @Column(name = "phone")
    private String phone;

    @Column(name = "password")
    private String password;

    @Column(name = "avatar")
    private String avatar;

    @Column(name = "birthday")
    private Date birthday;

    @Column(name = "active")
    private Boolean active = true;

    @Enumerated(EnumType.STRING)
    private UserGender gender;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    @Column(name = "email_confirmed")
    private Boolean emailConfirmed;

    @OneToOne
    @JoinColumn(name = "default_address_id")
    private DeliveryAddress deliveryAddress;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        List<GrantedAuthority> authorities = new ArrayList<>();
        String roleName = role.name();
        if (roleName.equals(UserRole.ADMIN))
            authorities.add(new SimpleGrantedAuthority(roleName));
        else{
            if (!active)
                authorities.add(new SimpleGrantedAuthority("LOCKED_ACCOUNT"));
            else
                authorities.add(new SimpleGrantedAuthority(roleName));
        }
        return authorities;
    }

    @Override
    public String getUsername() {
        return this.getEmail();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return active;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return active;
    }
}
