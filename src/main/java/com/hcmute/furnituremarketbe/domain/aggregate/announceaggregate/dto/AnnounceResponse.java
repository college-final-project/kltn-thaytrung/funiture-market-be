package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.entity.Announce;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
public class AnnounceResponse {
    private String id;
    private String createdAt;
    private AnnounceType type;
    private Boolean seen;
    private List<String> content = new ArrayList<>();

    public AnnounceResponse(Announce announce) {
        this.id = announce.getId();
        this.createdAt = zonedDateTimeToString(announce.getCreatedAt());
        this.type = announce.getType();
        this.seen = announce.getSeen();
        this.content = announce.getContent();
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
