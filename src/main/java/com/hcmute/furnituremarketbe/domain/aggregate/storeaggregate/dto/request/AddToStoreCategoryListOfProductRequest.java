package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class AddToStoreCategoryListOfProductRequest {
    private String productId;
    private String storeCategoryId;
}
