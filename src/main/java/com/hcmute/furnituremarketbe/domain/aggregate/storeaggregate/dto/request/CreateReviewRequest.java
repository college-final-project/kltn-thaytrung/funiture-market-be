package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class CreateReviewRequest {
    private String productId;
    private String content;
    private Integer star;
}
