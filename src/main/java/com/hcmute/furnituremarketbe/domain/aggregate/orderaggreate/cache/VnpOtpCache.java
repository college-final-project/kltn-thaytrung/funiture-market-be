package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.hcmute.furnituremarketbe.configuration.VNPayConfiguration;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionBase;
import org.springframework.http.HttpStatus;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class VnpOtpCache {
    private int vnpOtpLength;

    LoadingCache<String, String> loadingCache;

    public VnpOtpCache(long vnpExpiredAfterMins, int vnpOtpLength) {
        loadingCache = CacheBuilder
                .newBuilder()
                .expireAfterWrite(vnpExpiredAfterMins, TimeUnit.MINUTES)
                .build(new CacheLoader<String, String>() {
                    @Override
                    public String load(String key) throws Exception {
                        return null;
                    }
                });
        this.vnpOtpLength = vnpOtpLength;
    }

    public void createVnpOtp(String email, String otp) {
        loadingCache.put(email, otp);
    }

    public String getOtpCode(String email) {
        try {
            var otp = loadingCache.get(email);
            if (otp != null)
                return otp;
            else
                return "";
        }catch (ExecutionException e) {
            throw new ServiceExceptionBase(HttpStatus.INTERNAL_SERVER_ERROR).addMessage(e.getMessage());
        }
    }

    public void delete(String email) {
        loadingCache.invalidate(email);
    }
}
