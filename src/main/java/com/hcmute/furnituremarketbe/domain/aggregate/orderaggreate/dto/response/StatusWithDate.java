package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import lombok.Data;

@Data
public class StatusWithDate {
    private OrderStatus status;
    private String date;

    public StatusWithDate(OrderStatus status, String date) {
        this.status = status;
        this.date = date;
    }
}
