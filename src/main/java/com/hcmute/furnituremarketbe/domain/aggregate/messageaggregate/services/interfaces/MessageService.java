package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageOneToOne;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;

public interface MessageService {
    ResponseBaseAbstract getListMessageWithOnePerson(User user, String opponentId);

    ChatMessageResponse storeMessage(ChatMessageOneToOne message);

    ResponseBaseAbstract deleteMessage(String messageId);

    ResponseBaseAbstract getAllOpponents(String userId);
}
