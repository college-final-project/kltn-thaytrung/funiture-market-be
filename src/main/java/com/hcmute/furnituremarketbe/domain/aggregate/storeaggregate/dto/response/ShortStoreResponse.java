package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import lombok.Data;

@Data
public class ShortStoreResponse {
    private String id;
    private String shopName;
    private String address;
    private String logo;
    private Double avgReviewStar;
    private Integer numReview;
    private Integer productAmount;

    public ShortStoreResponse(Store store, Double avgReviewStar, Integer numReview, Integer productAmount) {
        this.id = store.getId();
        this.shopName = store.getShopName();
        this.address = store.getAddress();
        this.logo = store.getLogo();
        this.avgReviewStar = avgReviewStar;
        this.numReview = numReview;
        this.productAmount = productAmount;
    }
}
