package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderItem;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import lombok.Data;

import java.util.List;

@Data
public class StoreOrderIncomeResponse {
    private String orderId;
    private Double tax;
    private Double totalPrice;
    private Double amount;

    public StoreOrderIncomeResponse(Order order) {
        this.orderId = order.getId();
        this.tax = (order.getTotal() - order.getVoucherDiscount()) * 0.03;
        this.totalPrice = getTotalPriceOfOrder(order);
        this.amount = getProfitOfOrder(order);
    }

    private Double getTotalPriceOfOrder(Order order) {
        List<OrderItem> orderItems = order.getOrderItems();
        Double total = 0.0;
        for (OrderItem item : orderItems) {
            Product product = item.getProduct();
            total += product.getPrice() * item.getQuantity();
        }
        return total;
    }

    private Double getProfitOfOrder(Order order) {
        List<OrderItem> orderItems = order.getOrderItems();
        Double total = 0.0;
        for (OrderItem item : orderItems) {
            Product product = item.getProduct();
            total += product.getPrice() * item.getQuantity();
        }
        return (order.getTotal() - order.getVoucherDiscount()) * 0.97 - total;
    }
}
