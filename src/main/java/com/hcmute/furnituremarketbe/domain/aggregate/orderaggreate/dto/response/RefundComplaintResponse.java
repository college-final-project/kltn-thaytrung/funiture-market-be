package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.RefundComplaint;
import lombok.Data;

import java.util.List;

@Data
public class RefundComplaintResponse {
    private String id;
    private String reason;
    private boolean done;
    private List<String> images;
    private OrderRefundResponse orderRefundResponse;

    public RefundComplaintResponse(RefundComplaint refundComplaint, OrderRefundResponse response) {
        this.id = refundComplaint.getId();
        this.reason = refundComplaint.getReason();
        this.done = refundComplaint.isDone();
        this.images = refundComplaint.getImages();
        assert refundComplaint.getOrderRefund() != null;
        this.orderRefundResponse = response;
    }
}
