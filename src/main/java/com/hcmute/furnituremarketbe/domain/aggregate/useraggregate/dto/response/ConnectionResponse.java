package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Connection;
import lombok.Data;

import java.util.Date;

/**
 * Data transfer object (DTO) for Follower
 */
@Data
public class ConnectionResponse {

    private String id;

    private String userId;

    private String followerId;


    public ConnectionResponse(Connection connection) {

        this.id = connection.getId();
        if (connection.getUser() != null) {
            this.userId = connection.getUser().getId();
        }

        if (connection.getFollower() != null) {
            this.followerId = connection.getFollower().getId();
        }

    }
}