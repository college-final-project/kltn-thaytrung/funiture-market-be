package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderItem;
import lombok.Data;

@Data
public class OrderItemResponse {
    private String id;
    private String productName;
    private String productMaterial;
    private String productThumbnail;
    private Double productPrice;
    private Integer quantity;
    private Double total;
    private String productId;

    public OrderItemResponse(OrderItem item) {
        this.id = item.getId();
        this.productName = item.getProductName();
        this.productMaterial = item.getProductMaterial();
        this.productThumbnail = item.getProductThumbnail();
        this.productPrice = item.getProductPrice();
        this.quantity = item.getQuantity();
        this.total = item.getTotal();
        this.productId = item.getProduct().getId();
    }
}
