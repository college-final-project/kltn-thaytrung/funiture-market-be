package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import lombok.Data;

import java.util.Date;

@Data
public class UpdateProfileRequest {
    private String fullName;
    private UserGender gender;
    private Date birthday;
}
