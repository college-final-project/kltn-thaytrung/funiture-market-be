package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ProductExtendRepositoryImpl extends ExtendEntityRepositoryBase<Product> implements ProductExtendRepository{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<Product> searchProduct(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<Product> criteriaQuery = criteriaBuilder.createQuery(Product.class);
        Root<Product> productRoot = criteriaQuery.from(Product.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                productRoot,
                Product.class.getDeclaredFields()
        );
    }
}
