package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.cache.OtpCache;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.TokenResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.UserResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.RefreshToken;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.RefreshTokenRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.RefreshTokenService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.UserService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.*;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private WalletRepository walletRepository;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private RefreshTokenService refreshTokenService;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AnnounceService announceService;

    @Autowired
    private MailService mailService;

    @Autowired
    private OtpCache otpCache;

    @Value("${com.hcmute.furnituremarketbe.default-avatar}")
    private String defaultAvatar;


    private final List<UserRole> userRoles = Arrays.stream(UserRole.values()).filter(u -> u.name().contains("ADMIN_")).toList();

    @Override
    public ResponseBaseAbstract login(LoginRequest request, HttpServletResponse response) {
        User user = this.userRepository.getUserByEmail(request.getEmail());

        if (user == null) {
            throw ServiceExceptionFactory.unauthorized()
                    .addMessage("Email không tồn tại");
        }

        if (!this.passwordEncoder.matches(request.getPassword(), user.getPassword())) {
            throw ServiceExceptionFactory.unauthorized()
                    .addMessage("Mật khẩu không đúng");
        }
        TokenResponse tokenResponse = this.refreshTokenService.generateLoginTokens(user);
        Map<String, Object> data = new HashMap<>();
        UserResponse userResponse = new UserResponse(user);
        userResponse.setAvatar(generateAvatarUrl(userResponse.getAvatar()));
        data.put("user", userResponse);
        data.put("accessToken", tokenResponse.getAccessToken());
        data.put("refreshToken", tokenResponse.getRefreshToken());

        Cookie cookie = new Cookie("X-Refresh-Token", tokenResponse.getRefreshToken());
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        response.addCookie(cookie);

        return SuccessResponse.builder()
                .addMessage("Đăng nhập thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getListRole() {
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(userRoles)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract logout(String refreshTokenId, HttpServletRequest request) {
        String xRefreshToken = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("X-Refresh-Token".equals(cookie.getName())) {
                    xRefreshToken = cookie.getValue();
                    break;
                }
            }
        }
        if (xRefreshToken != null && refreshTokenId == null) {
            refreshTokenId = xRefreshToken;
        }
        this.refreshTokenService.deleteTokenWhenLogout(refreshTokenId);
        return SuccessResponse.builder()
                .addMessage("Đăng xuất thành công")
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract toggleUserStatus(String userId) {
        User user = this.userRepository.getUserById(userId);
        if (user == null) {
            throw ServiceExceptionFactory.unauthorized()
                    .addMessage("Id không tồn tại");
        }
        user.setActive(!user.getActive());
        this.userRepository.save(user);
        UserResponse userResponse = new UserResponse(user);
        userResponse.setAvatar(generateAvatarUrl(userResponse.getAvatar()));
        return SuccessResponse.builder()
                .addMessage("Chuyển trạng thái người dùng thành công")
                .setData(userResponse)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract createAdminAccount(CreateAdminAccountRequest request) {
        User admin = new User();
        admin.setId(UUID.randomUUID().toString());
        admin.setFullName(request.getFullName());
        admin.setEmail(request.getEmail());
        admin.setPhone(request.getPhone());
        admin.setPassword(this.passwordEncoder.encode(request.getPassword()));
        admin.setAvatar(this.defaultAvatar);
        admin.setBirthday(request.getBirthday());
        admin.setActive(true);
        admin.setGender(UserGender.MALE);
        admin.setRole(request.getRole());
        admin.setEmailConfirmed(true);
        this.userRepository.save(admin);

        return SuccessResponse.builder()
                .addMessage("Tạo tài khoản admin thành công")
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract registerBuyerAccount(RegisterBuyerRequest request) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setFullName(request.getFullName());
        user.setEmail(request.getEmail());
        user.setPhone(request.getPhone());
        user.setBirthday(request.getBirthday());
        user.setPassword(this.passwordEncoder.encode(request.getPassword()));
        user.setActive(true);
        user.setAvatar(this.defaultAvatar);
        user.setEmailConfirmed(false);
        user.setRole(UserRole.BUYER);
        user.setGender(request.getGender());
        this.userRepository.save(user);

        Wallet wallet = new Wallet();
        wallet.setId(UUID.randomUUID().toString());
        wallet.setValue(0.0);
        wallet.setUser(user);
        this.walletRepository.save(wallet);

        return SuccessResponse.builder()
                .addMessage("Vui lòng xác minh email để hoàn tất việc đăng ký!")
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract registerSellerAccount(RegisterSellerRequest request) {
        User seller = new User();
        seller.setId(UUID.randomUUID().toString());
        seller.setFullName(request.getOwnerName());
        seller.setEmail(request.getEmail());
        seller.setPhone(request.getPhone());
        seller.setPassword(this.passwordEncoder.encode(request.getPassword()));
        seller.setAvatar(this.defaultAvatar);
        seller.setBirthday(request.getBirthday());
        seller.setActive(true);
        seller.setGender(request.getGender());
        seller.setRole(UserRole.SELLER);
        seller.setEmailConfirmed(false);
        this.userRepository.save(seller);

        Wallet wallet = new Wallet();
        wallet.setId(UUID.randomUUID().toString());
        wallet.setValue(0.0);
        wallet.setUser(seller);
        this.walletRepository.save(wallet);

        Store store = new Store();
        store.setId(seller.getId());
        store.setAddress(request.getAddress());
        store.setDeliveryMethod(request.getDeliveryMethod());
        store.setQualified(false);
        store.setUser(seller);
        this.storeRepository.save(store);
        return SuccessResponse.builder()
                .addMessage("Vui lòng xác minh email để hoàn tất việc đăng ký!")
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract refresh(String refreshTokenId,
                                        HttpServletRequest request, HttpServletResponse response) {
        String xRefreshToken = null;
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if ("X-Refresh-Token".equals(cookie.getName())) {
                    xRefreshToken = cookie.getValue();
                    break;
                }
            }
        }
        if (xRefreshToken != null && refreshTokenId == null) {
            refreshTokenId = xRefreshToken;
        }
        TokenResponse tokenResponse = this.refreshTokenService.renewToken(refreshTokenId);
        RefreshToken refreshToken = this.refreshTokenRepository.getRefreshTokenById(refreshTokenId);
        User user = refreshToken.getUser();
        Map<String, Object> data = new HashMap<>();
        UserResponse userResponse = new UserResponse(user);
        userResponse.setAvatar(generateAvatarUrl(userResponse.getAvatar()));
        data.put("user", userResponse);
        data.put("accessToken", tokenResponse.getAccessToken());
        data.put("refreshToken", tokenResponse.getRefreshToken());

        Cookie cookie = new Cookie("X-Refresh-Token", tokenResponse.getRefreshToken());
        cookie.setHttpOnly(true);
        cookie.setSecure(true);
        response.addCookie(cookie);
        return SuccessResponse.builder()
                .addMessage("Làm mới token thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getUserProfile(User user) {
        UserResponse userResponse = new UserResponse(user);
        userResponse.setAvatar(generateAvatarUrl(userResponse.getAvatar()));
        return SuccessResponse.builder().addMessage("Lấy thông tin cá nhân thành công")
                .setData(userResponse).returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updateProfile(User user, UpdateProfileRequest request) {
        user.setFullName(request.getFullName());
        user.setGender(request.getGender());
        this.userRepository.save(user);
        this.announceService.onUpdatedUserAccount(user, "Cập nhật thông tin cá nhân thành công");
        return SuccessResponse.builder().addMessage("Cập nhật thông tin cá nhân thành công")
                .setData(new UserResponse(user)).returnUpdated();
    }

    @Override
    public ResponseBaseAbstract changePassword(User user, UpdatePasswordRequest request) {
        if (StringUtils.isEmpty(request.getOldPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập mật khẩu cũ");
        if (StringUtils.isEmpty(request.getNewPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập mật khẩu mới");

        String oldPassword = request.getOldPassword();
        String newPassword = request.getNewPassword();

        if (!this.passwordEncoder.matches(oldPassword, user.getPassword()))
            throw ServiceExceptionFactory.badRequest().addMessage("Mật khẩu cũ không chính xác");

        user.setPassword(this.passwordEncoder.encode(newPassword));
        this.userRepository.save(user);
        this.announceService.onUpdatedUserAccount(user, "Cập nhật mật khẩu thành công");

        return SuccessResponse.builder().addMessage("Cập nhật mật khẩu thành công")
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updateAvatar(User user, MultipartFile newAvatar) throws IOException {
        String newAvatarName = this.fileStorageService.save(newAvatar);
        deleteOldAvatar(user.getAvatar());
        user.setAvatar(newAvatarName);
        this.userRepository.save(user);
        UserResponse userResponse = new UserResponse(user);
        userResponse.setAvatar(generateAvatarUrl(userResponse.getAvatar()));
        this.announceService.onUpdatedUserAccount(user, "Cập nhật ảnh đại diện thành công");
        return SuccessResponse.builder().addMessage("Cập nhật ảnh đại diện thành công")
                .setData(userResponse).returnUpdated();
    }

    @Override
    public ResponseBaseAbstract confirmUserEmail(EmailConfirmationRequest request) {
        String email = request.getEmail();
        User user = this.userRepository.getUserByEmail(email);
        if (user == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tim thấy người dùng với email = "+email);
        }
        String otpCode = request.getOtpCode();
        if (!otpCode.equals(this.otpCache.getOtpCode(email))) {
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");
        }
        user.setEmailConfirmed(true);
        this.userRepository.save(user);
        return SuccessResponse.builder().addMessage("Xác thực email thành công")
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateEmail(User user, UpdateEmailRequest request) {
        String oldEmail = user.getEmail();
        String otpCode = request.getOtpCode();
        String newEmail = request.getNewEmail();
        if (otpCode == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Thiếu trường otpCode");
        if (newEmail == null)
            throw ServiceExceptionFactory.badRequest().addMessage("Thiếu trường email");
        if (!otpCode.equals(this.otpCache.getOtpCode(oldEmail))) {
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");
        }
        user.setEmail(newEmail);
        user.setEmailConfirmed(false);
        this.userRepository.save(user);
        this.announceService.onUpdatedUserAccount(user, "Cập nhật email thành công");
        return SuccessResponse.builder().addMessage("Cập nhật email thành công")
                .returnGetOK();
    }

    public ResponseBaseAbstract updatePhone() {throw new RuntimeException();}

    private String generateAvatarUrl(String avatar) {
        if (avatar.startsWith("https")) {
            return avatar;
        }
        else
            return this.fileStorageService.getImageUrl(avatar);
    }

    private void deleteOldAvatar(String avatar) throws IOException {
        if (!avatar.startsWith("https")) {
            this.fileStorageService.delete(avatar);
        }
    }

    @Override
    public ResponseBaseAbstract restorePassword(EmailConfirmationRequest request) {
        if (StringUtils.isEmpty(request.getEmail()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập email");
        if (StringUtils.isEmpty(request.getOtpCode()))
            throw ServiceExceptionFactory.badRequest().addMessage("Chưa nhập otp");

        String email = request.getEmail();
        String otpCode = request.getOtpCode();
        User user = this.userRepository.getUserByEmail(email);

        if (user == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại user với email = " + email);
        if (!user.getEmailConfirmed())
            throw ServiceExceptionFactory.badRequest().addMessage("Tài khoản của bạn chưa xác thực email");

        if (!otpCode.equals(this.otpCache.getOtpCode(email)))
            throw ServiceExceptionFactory.badRequest().addMessage("Otp không hợp lệ");

        String newPassword = generateRandomPassword();
        user.setPassword(this.passwordEncoder.encode(newPassword));
        this.userRepository.save(user);
        this.mailService.sendNewPassword(user, newPassword);

        return SuccessResponse.builder().addMessage("Mật khẩu mới đã được gửi qua mail")
                .returnUpdated();
    }

    private String generateRandomPassword() {
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        String pwd = RandomStringUtils.random( 15, characters );
        return pwd;
    }

    @Override
    public ResponseBaseAbstract getAllUsers() {
        List<User> users = this.userRepository.findAll();
        List<UserResponse> data = new ArrayList<>();
        for (User user : users) {
            UserResponse response = new UserResponse(user);
            response.setAvatar(generateAvatarUrl(response.getAvatar()));
            data.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }
}
