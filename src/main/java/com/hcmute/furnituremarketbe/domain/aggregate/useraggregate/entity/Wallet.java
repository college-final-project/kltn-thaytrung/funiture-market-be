package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Order;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.List;

@Entity
@Data
public class Wallet {
    @Id
    private String id;

    @Column(name = "value")
    private Double value;

    @ManyToOne
    @JoinColumn(name = "user")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User user;

    @OneToMany(mappedBy = "wallet", cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private List<TransactionHistory> transactionHistories;
}
