package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserGender;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import lombok.Data;

import java.util.Date;

@Data
public class CreateAdminAccountRequest {
    private String email;
    private String fullName;
    private String phone;
    private String password;
    private Date birthday;
    private UserRole role;
    private UserGender gender;
}
