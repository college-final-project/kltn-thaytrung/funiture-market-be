package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.entity.Announce;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface AnnounceRepository extends JpaRepository<Announce, String> {
    @Query("SELECT a FROM Announce a WHERE a.type=:type AND a.user.id=:id ORDER BY a.createdAt DESC")
    Page<Announce> getUserAnnounceByType(@Param("type") AnnounceType type, @Param("id") String id, Pageable pageable);

    @Query("SELECT a FROM Announce a WHERE a.id=:id")
    Announce getAnnounceById(@Param("id") String id);

    @Query("SELECT a FROM Announce a WHERE a.seen=true AND FUNCTION('DATE', a.createdAt) <= FUNCTION('DATE', :givenDate)")
    List<Announce> getReadAnnounce(@Param("givenDate")Date givenDate);

    @Query("SELECT a FROM Announce a WHERE a.user.id=:id")
    Page<Announce> getAnnounceByUser(@Param("id") String id, Pageable pageable);
}
