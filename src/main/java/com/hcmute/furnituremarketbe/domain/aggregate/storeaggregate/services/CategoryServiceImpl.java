package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.CategoryResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.SystemCategoryResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreCategory;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.SystemCategory;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreCategoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.SystemCategoryRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.CategoryService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private SystemCategoryRepository systemCategoryRepository;

    @Autowired
    private StoreCategoryRepository storeCategoryRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Override
    public ResponseBaseAbstract createStoreCategory(String storeId, CreateCategoryRequest request) {
        StoreCategory category = new StoreCategory();
        category.setId(UUID.randomUUID().toString());
        category.setName(request.getName());
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        category.setStore(store);
        this.storeCategoryRepository.save(category);
        CategoryResponse data = new CategoryResponse(category);
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllStoreCategory(String storeId) {
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        List<StoreCategory> categoryList = store.getCategories();
        List<CategoryResponse> data = categoryList.stream().map(category -> new CategoryResponse(category)).toList();
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreCategoryById(String storeId, String id){
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        StoreCategory category = this.storeCategoryRepository.getCategoryById(id);
        if (category == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy danh mục cửa hàng có id ="+id);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(new CategoryResponse(category))
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateStoreCategory(UpdateStoreCategoryRequest request) {
        String id = request.getId();
        StoreCategory category = this.storeCategoryRepository.getCategoryById(id);
        if (category == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy danh mục cửa hàng có id ="+id);
        }
        Store store = category.getStore();
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        category.setName(request.getName());
        this.storeCategoryRepository.save(category);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(new CategoryResponse(category))
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteStoreCategory(String id) {
        StoreCategory category = this.storeCategoryRepository.getCategoryById(id);
        if (category == null) {
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy danh mục cửa hàng có id ="+id);
        }
        Store store = category.getStore();
        if (store == null)
            throw ServiceExceptionFactory.badRequest()
                .addMessage("Bạn chưa có cửa hàng");
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");
        String categoryName = category.getName();
        List<Product> productList = this.productRepository.findAll();
        List<Product> filterList = productList.stream()
                .filter(product -> product.getStoreCategories().contains(categoryName))
                .collect(Collectors.toList());

        if (filterList.size() != 0) {
            for (Product product : filterList) {
                List<String> storeCategories = product.getStoreCategories();
                storeCategories.remove(categoryName);
                product.setStoreCategories(storeCategories);
                this.productRepository.save(product);
            }
        }

        this.storeCategoryRepository.delete(category);
        return SuccessResponse.builder().addMessage("Xóa dữ liệu thành công").returnDeleted();
    }

    @Override
    public ResponseBaseAbstract createSystemCategory(CreateSystemCategoryRequest request) throws IOException {
        SystemCategory category = new SystemCategory();
        category.setId(UUID.randomUUID().toString());
        category.setName(request.getName());
        String image = this.fileStorageService.save(request.getImage());
        category.setImage(image);
        this.systemCategoryRepository.save(category);
        SystemCategoryResponse categoryResponse = new SystemCategoryResponse(category);
        categoryResponse.setImage(this.fileStorageService.getImageUrl(categoryResponse.getImage()));
        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(categoryResponse)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract getAllSystemCategory() {
        List<SystemCategory> categoryList = this.systemCategoryRepository.findAll();
        List<SystemCategoryResponse> data = categoryList.stream().map(category -> new SystemCategoryResponse(category)).toList();
        if (data.size() != 0) {
            for (SystemCategoryResponse categoryResponse : data) {
                categoryResponse.setImage(this.fileStorageService.getImageUrl(categoryResponse.getImage()));
            }
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getSystemCategoryById(String id) {
        SystemCategory category = this.systemCategoryRepository.getCategoryById(id);
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy system category với id = "+ id);
        SystemCategoryResponse data = new SystemCategoryResponse(category);
        data.setImage(this.fileStorageService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateSystemCategory(UpdateSystemCategoryRequest request) throws IOException {
        SystemCategory category = this.systemCategoryRepository.getCategoryById(request.getId());
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy system category với id = "+ request.getId());
        category.setName(request.getName());
        if (request.getImage() != null) {
            this.fileStorageService.delete(category.getImage());
            category.setImage(this.fileStorageService.save(request.getImage()));
        }
        this.systemCategoryRepository.save(category);

        SystemCategoryResponse data = new SystemCategoryResponse(category);
        data.setImage(this.fileStorageService.getImageUrl(data.getImage()));
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteSystemCategory(String id) throws IOException {
        SystemCategory category = this.systemCategoryRepository.getCategoryById(id);
        if (category == null)
            throw ServiceExceptionFactory
                    .notFound()
                    .addMessage("Không tìm thấy category với id = "+ id);
        if (category.getImage() != null) {
            this.fileStorageService.delete(category.getImage());
        }
        this.systemCategoryRepository.delete(category);

        return SuccessResponse.builder().addMessage("Xóa dữ liệu thành công").returnDeleted();
    }
}
