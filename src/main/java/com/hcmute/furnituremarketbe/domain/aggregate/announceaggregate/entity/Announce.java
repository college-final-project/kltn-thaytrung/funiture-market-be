package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@Data
@Table(name = "tbl_announce")
public class Announce {
    @Id
    private String id;

    @Column(name = "created_at")
    private ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Enumerated(EnumType.STRING)
    private AnnounceType type;

    @Column(name = "seen")
    private Boolean seen;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "content", columnDefinition = "TEXT")
    private List<String> content = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;
}
