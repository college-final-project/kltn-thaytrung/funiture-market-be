package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface TransactionHistoryService {
    ResponseBaseAbstract getAllTransactionHistory();

    ResponseBaseAbstract getAllTransactionHistoryByUser(User user);
}
