package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.SendViaEmailType;
import lombok.Data;

@Data
public class SendOtpViaEmailRequest {
    private String email;
    private SendViaEmailType type;
}
