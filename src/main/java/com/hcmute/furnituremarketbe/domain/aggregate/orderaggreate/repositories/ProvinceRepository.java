package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.ProvinceCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface ProvinceRepository extends JpaRepository<ProvinceCity, Integer> {
    @Query("SELECT p FROM ProvinceCity p WHERE p.id=:id")
    ProvinceCity getProvinceById(@Param("id") Integer id);
}
