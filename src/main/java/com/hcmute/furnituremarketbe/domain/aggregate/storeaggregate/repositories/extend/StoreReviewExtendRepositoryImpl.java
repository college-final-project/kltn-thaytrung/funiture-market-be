package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.base.ExtendEntityRepositoryBase;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import jakarta.persistence.criteria.CriteriaBuilder;
import jakarta.persistence.criteria.CriteriaQuery;
import jakarta.persistence.criteria.Root;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class StoreReviewExtendRepositoryImpl extends ExtendEntityRepositoryBase<StoreReview> implements StoreReviewExtendRepository{
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public PagingAndSortingResponse<StoreReview> searchStoreReview(Map<String, String> queries) {
        CriteriaBuilder criteriaBuilder = this.entityManager.getCriteriaBuilder();
        CriteriaQuery<StoreReview> criteriaQuery = criteriaBuilder.createQuery(StoreReview.class);
        Root<StoreReview> reviewRoot = criteriaQuery.from(StoreReview.class);


        return this.dynamicSearchEntity(
                this.entityManager,
                queries,
                criteriaBuilder,
                criteriaQuery,
                reviewRoot,
                StoreReview.class.getDeclaredFields()
        );
    }
}
