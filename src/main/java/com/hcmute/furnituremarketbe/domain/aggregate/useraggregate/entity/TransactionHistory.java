package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import jakarta.persistence.*;
import lombok.Data;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Entity
@Data
public class TransactionHistory {
    @Id
    private String id;

    @Column(name = "created_at")
    private ZonedDateTime createdAt = ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh"));

    @Enumerated(EnumType.STRING)
    private TransactionType type;

    @Column(name = "value")
    private Double value;

    @ManyToOne
    @JoinColumn(name = "wallet_id")
    private Wallet wallet;
}
