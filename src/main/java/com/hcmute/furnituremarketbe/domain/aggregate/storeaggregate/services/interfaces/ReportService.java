package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateReportStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface ReportService {
    ResponseBaseAbstract createReport(CreateReportRequest request);

    ResponseBaseAbstract getAllReports();

    ResponseBaseAbstract getReportById(String id);

    ResponseBaseAbstract getAllStoreProductReport(User seller);

    ResponseBaseAbstract updateReportStatus(UpdateReportStatusRequest request);
}
