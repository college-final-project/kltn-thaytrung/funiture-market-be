package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.ReportType;
import lombok.Data;

@Data
public class CreateReportRequest {
    private String reason;
    private String description;
    private ReportType type;
    private String reporterName;
    private String objectId;
}
