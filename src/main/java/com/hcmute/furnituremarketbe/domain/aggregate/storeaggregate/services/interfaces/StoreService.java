package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateStoreRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreIdentifierRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreInfoRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreTaxRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

import java.io.IOException;
import java.util.Map;

public interface StoreService {
    ResponseBaseAbstract getSellerStoreInfo(User seller);

    ResponseBaseAbstract viewStore(User user, String storeId);

    ResponseBaseAbstract updateStoreInfo(User seller, UpdateStoreInfoRequest request) throws IOException;

    ResponseBaseAbstract updateStoreTax(User seller, UpdateStoreTaxRequest request);

    ResponseBaseAbstract updateStoreIdentifier(User seller, UpdateStoreIdentifierRequest request) throws IOException;

    ResponseBaseAbstract marketingStore();

    ResponseBaseAbstract getAllStore();

    ResponseBaseAbstract getStoreById(String id);

    ResponseBaseAbstract searchFilterStore(Map<String, String> queries);
}
