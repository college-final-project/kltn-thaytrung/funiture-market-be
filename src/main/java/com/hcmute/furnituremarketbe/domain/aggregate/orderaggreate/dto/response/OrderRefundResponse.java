package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Data
public class OrderRefundResponse {
    private String id;
    private String reason;
    private String createdAt;
    private List<String> images;
    private boolean accepted;
    private String orderId;

    public OrderRefundResponse(OrderRefund orderRefund) {
        this.id = orderRefund.getId();
        this.reason = orderRefund.getReason();
        this.createdAt = zonedDateTimeToString(orderRefund.getCreatedAt());
        this.images = orderRefund.getImages();
        this.accepted = orderRefund.isAccepted();
        this.orderId = orderRefund.getOrder() != null ? orderRefund.getOrder().getId() : null;
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
