package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.RefundComplaint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RefundComplaintRepository extends JpaRepository<RefundComplaint, String> {
    @Query("SELECT r FROM RefundComplaint r WHERE r.id=:id")
    RefundComplaint getRefundComplaintById(@Param("id") String id);

    @Query("SELECT r FROM RefundComplaint r WHERE r.done=:done")
    List<RefundComplaint> getRefundByDone(@Param("done") boolean done);

    @Query("SELECT r FROM RefundComplaint r WHERE r.orderRefund.id=:id")
    RefundComplaint getRefundComplaintByOrderRefund(@Param("id") String id);
}
