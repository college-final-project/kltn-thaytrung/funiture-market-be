package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.entity.Announce;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.repositories.AnnounceRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.RefreshToken;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.RefreshTokenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class AnnounceCleanupTask {
    @Autowired
    private AnnounceRepository announceRepository;

    @Scheduled(cron = "0 0 0 * * *")
    public void deleteExpiredTokens() {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_MONTH, -3);
        Date givenDate = calendar.getTime();
        List<Announce> toDeleteAnnounces = this.announceRepository.getReadAnnounce(givenDate);
        this.announceRepository.deleteAll(toDeleteAnnounces);
    }
}
