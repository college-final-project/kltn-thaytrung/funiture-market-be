package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.DeliveryAddress;
import lombok.Data;

@Data
public class BuyerDeliveryAddressResponse {
    private String id;
    private String deliveryAddress;
    private String receiverName;
    private String receiverPhone;

    public BuyerDeliveryAddressResponse(DeliveryAddress deliveryAddress) {
        this.id = deliveryAddress.getId();
        this.deliveryAddress = deliveryAddress.getDetailAddress();
        this.receiverName = deliveryAddress.getReceiver();
        this.receiverPhone = deliveryAddress.getPhone();
    }
}
