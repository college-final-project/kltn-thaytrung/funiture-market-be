package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request;

import lombok.Data;

@Data
public class UpdateStoreTaxRequest {
    private String taxType;
    private String taxCode;
}
