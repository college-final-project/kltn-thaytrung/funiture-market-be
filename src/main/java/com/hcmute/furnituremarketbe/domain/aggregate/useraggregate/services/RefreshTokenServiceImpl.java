package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.TokenResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.RefreshToken;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.RefreshTokenRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.RefreshTokenService;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.jwt.JwtTokenProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {
    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private JwtTokenProvider jwtTokenProvider;

    @Override
    public TokenResponse generateLoginTokens(User user) {
        String accessToken = this.jwtTokenProvider.generateToken(user);
        RefreshToken refreshToken = generateRefreshToken(user);
        this.refreshTokenRepository.save(refreshToken);
        return new TokenResponse(refreshToken.getId(), accessToken);
    }

    @Override
    public TokenResponse renewToken(String refreshTokenId) {
        RefreshToken storedToken = this.refreshTokenRepository.getRefreshTokenById(refreshTokenId);
        User user = storedToken.getUser();
        if (storedToken == null)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Refresh token không tồn tại");
        if (!storedToken.isActivated()) {
            deleteDeactivatedToken(storedToken.getFamilyId());
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Refresh token không có hiệu lực");
        }
        if (storedToken.getExpirationDate().before(new Date())) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Refresh token đã hết hạn");
        }

        storedToken.setActivated(false);
        this.refreshTokenRepository.save(storedToken);

        String accessToken = this.jwtTokenProvider.generateToken(user);
        RefreshToken refreshToken = generateRefreshToken(user);
        refreshToken.setFamilyId(storedToken.getFamilyId());
        this.refreshTokenRepository.save(refreshToken);
        return new TokenResponse(refreshToken.getId(), accessToken);
    }

    private RefreshToken generateRefreshToken(User user) {
        RefreshToken refreshToken = new RefreshToken();
        refreshToken.setId(UUID.randomUUID().toString());
        LocalDateTime expirationDateTime = LocalDateTime.now().plusDays(7);
        Date expirationDate = Date.from(expirationDateTime.atZone(ZoneOffset.systemDefault()).toInstant());
        refreshToken.setExpirationDate(expirationDate);
        refreshToken.setActivated(true);
        refreshToken.setUser(user);
        refreshToken.setFamilyId(refreshToken.getId());
        return refreshToken;
    }

    private void deleteDeactivatedToken(String id) {
        List<RefreshToken> toDeleteTokens = this.refreshTokenRepository.getAllTokenByFamilyId(id);
        this.refreshTokenRepository.deleteAll(toDeleteTokens);
    }

    @Override
    public void deleteTokenWhenLogout(String refreshToken) {
        RefreshToken storedToken = this.refreshTokenRepository.getRefreshTokenById(refreshToken);
        if (storedToken == null)
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Refresh token không tồn tại");
        deleteDeactivatedToken(storedToken.getFamilyId());
    }
}
