package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.ReportExplanation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ReportExplanationRepository extends JpaRepository<ReportExplanation, String> {
    @Query("SELECT r FROM ReportExplanation r WHERE r.id=:id")
    ReportExplanation getReportExplanationById(@Param("id") String id);

    @Query("SELECT r FROM ReportExplanation r WHERE r.report.id=:id")
    List<ReportExplanation> getReportExplanationByReportId(@Param("id") String id);
}
