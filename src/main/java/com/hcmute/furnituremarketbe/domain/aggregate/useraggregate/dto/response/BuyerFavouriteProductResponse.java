package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import lombok.Data;

@Data
public class BuyerFavouriteProductResponse {
    private String id;
    private String name;
    private String thumbnail;
    private String material;

    public BuyerFavouriteProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.thumbnail = product.getThumbnail();
        this.material = product.getMaterial();
    }
}
