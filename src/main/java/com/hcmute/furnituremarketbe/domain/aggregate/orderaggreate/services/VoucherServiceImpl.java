package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UseVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.UseVoucherResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.VoucherResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherUsage;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.VoucherType;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.VoucherProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.VoucherRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.VoucherUsageRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.VoucherService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.ShortProductResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Service
public class VoucherServiceImpl implements VoucherService {
    @Autowired
    private VoucherRepository voucherRepository;

    @Autowired
    private VoucherUsageRepository voucherUsageRepository;

    @Autowired
    private VoucherProductRepository voucherProductRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private FileStorageService fileStorageService;

    @Autowired
    private AnnounceService announceService;

    @Override
    public ResponseBaseAbstract createVoucher(User seller, CreateVoucherRequest request) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        Date startDate = request.getStartDate();
        Date endDate = request.getEndDate();
        LocalDate localStartDate = startDate.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
        LocalDate localEndDate = endDate.toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
        LocalDate localCurrentDate = new Date().toInstant().atZone(java.time.ZoneId.systemDefault()).toLocalDate();
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        if (localStartDate.isAfter(localEndDate))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày bắt đầu sau ngày kết thúc");

        if (localStartDate.isBefore(localCurrentDate))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày bắt đầu trước ngày hiện tại");

        if (localEndDate.isBefore(localCurrentDate))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày kết thúc trước ngày hiện tại");

        Voucher voucher = new Voucher();
        voucher.setId(UUID.randomUUID().toString());
        voucher.setName(request.getName());
        voucher.setCode(request.getCode());
        voucher.setType(request.getType());
        voucher.setStartDate(startDate);
        voucher.setEndDate(endDate);
        voucher.setMaxDiscount(request.getMaxDiscount());
        voucher.setMinValue(request.getMinValue());
        voucher.setTotalTimes(request.getTotalTimes());
        voucher.setBuyerLimit(request.getBuyerLimit());
        voucher.setHidden(false);
        voucher.setStore(store);
        this.voucherRepository.save(voucher);

        List<Product> products = new ArrayList<>();
        if (request.getType() == VoucherType.SHOP_VOUCHER) {
            products = store.getProducts();
        }
        else {
            for (String productId : request.getProductIds()) {
                Product product = this.productRepository.getProductById(productId);
                if (product == null)
                    throw ServiceExceptionFactory.notFound()
                            .addMessage("Không tìm thấy sản phẩm với id = "+productId);
                products.add(product);
            }
        }

        List<ShortProductResponse> productList = new ArrayList<>();

        for (Product product : products) {
            VoucherProduct voucherProduct = new VoucherProduct();
            voucherProduct.setId(UUID.randomUUID().toString());
            voucherProduct.setVoucher(voucher);
            voucherProduct.setProduct(product);
            this.voucherProductRepository.save(voucherProduct);
            ShortProductResponse productResponse = new ShortProductResponse(product);
            productResponse.setThumbnail(this.fileStorageService.getImageUrl(productResponse.getThumbnail()));
            productList.add(productResponse);
        }

        VoucherResponse data = new VoucherResponse(voucher);
        data.setProductList(productList);
        this.announceService.onCreatedVoucher(data, store);

        return SuccessResponse.builder()
                .addMessage("Tạo dữ liệu thành công")
                .setData(data)
                .returnCreated();
    }

    @Override
    public ResponseBaseAbstract updateVoucher(User seller, UpdateVoucherRequest request) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        Voucher voucher = this.voucherRepository.getVoucherById(request.getId());
        if (voucher == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy voucher với id = "+request.getId());


        String name = request.getName();
        String code = request.getCode();
        Date startDate = request.getStartDate();
        Date endDate = request.getEndDate();

        if (startDate != null && endDate != null && (startDate.after(endDate)))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày bắt đầu sau ngày kết thúc");

        if (startDate != null && startDate.before(new Date()))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày bắt đầu trước ngày hiện tại");

        if (endDate != null && endDate.before(new Date()))
            throw ServiceExceptionFactory.badRequest().addMessage("Lỗi ngày kết thúc trước ngày hiện tại");

        Double maxDiscount = request.getMaxDiscount();
        Double minValue = request.getMinValue();
        Integer totalTimes = request.getTotalTimes();
        Integer buyerLimit = request.getBuyerLimit();
        Boolean hidden = request.getHidden();
        List<String> productIds = request.getProductIds();

        if (name != null)
            voucher.setName(name);
        if (code != null)
            voucher.setCode(code);
        if (startDate != null)
            voucher.setStartDate(startDate);
        if (endDate != null)
            voucher.setEndDate(endDate);
        if (maxDiscount != null)
            voucher.setMaxDiscount(maxDiscount);
        if (minValue != null)
            voucher.setMinValue(minValue);
        if (totalTimes != null)
            voucher.setTotalTimes(totalTimes);
        if (buyerLimit != null)
            voucher.setBuyerLimit(buyerLimit);
        if (hidden != null)
            voucher.setHidden(hidden);
        this.voucherRepository.save(voucher);
        List<Product> products = new ArrayList<>();

        if (productIds != null && productIds.size() > 0) {
            clearApplyProducts(voucher.getId());
            for (String productId : productIds) {
                Product product = this.productRepository.getProductById(productId);
                if (product == null)
                    throw ServiceExceptionFactory.notFound()
                            .addMessage("Không tìm thấy sản phẩm với id = "+productId);
                products.add(product);
            }
            for (Product product : products) {
                VoucherProduct voucherProduct = new VoucherProduct();
                voucherProduct.setId(UUID.randomUUID().toString());
                voucherProduct.setVoucher(voucher);
                voucherProduct.setProduct(product);
                this.voucherProductRepository.save(voucherProduct);
            }
        }

        List<Product> productList = this.voucherProductRepository.getProductByVoucher(voucher.getId());
        List<ShortProductResponse> productResponses = new ArrayList<>();
        for (Product product : productList) {
            ShortProductResponse productResponse = new ShortProductResponse(product);
            productResponse.setThumbnail(this.fileStorageService.getImageUrl(productResponse.getThumbnail()));
            productResponses.add(productResponse);
        }
        VoucherResponse data = new VoucherResponse(voucher);
        data.setProductList(productResponses);

        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(data)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract deleteVoucher(User seller, String voucherId) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn chưa có cửa hàng");
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        Voucher voucher = this.voucherRepository.getVoucherById(voucherId);
        if (voucher == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy voucher với id = "+voucherId);

        this.voucherRepository.delete(voucher);
        return SuccessResponse.builder()
                .addMessage("Xóa dữ liệu thành công")
                .returnDeleted();
    }

    @Override
    public ResponseBaseAbstract useVoucher(User buyer, UseVoucherRequest request) {
        List<String> voucherIds = request.getVoucherIds();
        Double finalDiscount = 0.0;
        for (String voucherId : voucherIds) {
            Voucher voucher = this.voucherRepository.getVoucherById(voucherId);
            if (voucher == null)
                throw ServiceExceptionFactory.notFound()
                        .addMessage("Không tìm thấy voucher với id = "+voucherId);
            VoucherUsage voucherUsage = this.voucherUsageRepository.getByVoucherIdAndBuyerId(voucherId, buyer.getId());
            if (voucherUsage != null && voucherUsage.getRemainingTimes() == 0)
                throw ServiceExceptionFactory.badRequest()
                    .addMessage("Bạn đã hết hạn sử dụng voucher này id = "+voucherId);
            VoucherProduct voucherProduct = this.voucherProductRepository.getByVoucherIdAndProductId(voucherId, request.getProductId());
            if (voucherProduct == null)
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Voucher có id = " + voucherId + " không thể sử dụng trên sản phẩm có id = "+request.getProductId());
            if (!isValidVoucher(voucher))
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Voucher với id = "+voucherId + " không hợp lệ để sử dụng");
            if (request.getTotal() < voucher.getMinValue())
                throw ServiceExceptionFactory.badRequest()
                        .addMessage("Voucher với id = "+voucherId + " không đủ điều kiện để sử dụng");
            finalDiscount += voucher.getMaxDiscount();
        }
        Double total = request.getTotal() - finalDiscount;
        return SuccessResponse.builder()
                .addMessage("Sử dụng voucher thành công")
                .setData(new UseVoucherResponse(finalDiscount, total))
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreVouchers(String storeId) {
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null) {
            throw ServiceExceptionFactory.badRequest()
                    .addMessage("Không tìm thấy cửa hàng với id = "+storeId);
        }
        if (!store.isQualified())
            throw ServiceExceptionFactory.badRequest().addMessage("Bạn chưa cung cấp đủ thông tin cửa hàng");

        List<Voucher> vouchers = store.getVouchers();
        List<VoucherResponse> data = new ArrayList<>();
        for (Voucher voucher : vouchers) {
            VoucherResponse voucherResponse = new VoucherResponse(voucher);
            List<Product> products = this.voucherProductRepository.getProductByVoucher(voucher.getId());
            List<ShortProductResponse> productResponses = new ArrayList<>();
            for (Product product : products) {
                ShortProductResponse productResponse = new ShortProductResponse(product);
                productResponse.setThumbnail(this.fileStorageService.getImageUrl(productResponse.getThumbnail()));
                productResponses.add(productResponse);
            }
            voucherResponse.setProductList(productResponses);
            data.add(voucherResponse);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getVoucherByProduct(User buyer, String productId) {
        Product product = this.productRepository.getProductById(productId);
        if (product == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy sản phẩm với id = "+productId);
        Date givenDate = new Date();
        List<Voucher> vouchers = this.voucherProductRepository.getVoucherByProduct(productId, givenDate);
        List<VoucherResponse> data = new ArrayList<>();
        for (Voucher voucher : vouchers) {
            VoucherResponse voucherResponse = new VoucherResponse(voucher);
            voucherResponse.setUsable(isValidVoucher(voucher));
            VoucherUsage voucherUsage = this.voucherUsageRepository.getByVoucherIdAndBuyerId(voucher.getId(), buyer.getId());
            if (voucherUsage != null && voucherUsage.getRemainingTimes() <= 0)
                voucherResponse.setUsable(false);
            data.add(voucherResponse);
        }

        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }



    private boolean isValidVoucher(Voucher voucher) {
        if (voucher.isHidden())
            return false;
        if (voucher.getTotalTimes() <= 0)
            return false;
        Date currentDate = new Date();
        if (currentDate.after(voucher.getEndDate()) || currentDate.before(voucher.getStartDate()))
            return false;
        return true;
    }

    private void clearApplyProducts(String voucherId) {
        List<VoucherProduct> voucherProducts = this.voucherProductRepository.getVoucherProductByVoucherId(voucherId);
        this.voucherProductRepository.deleteAll(voucherProducts);
    }
}
