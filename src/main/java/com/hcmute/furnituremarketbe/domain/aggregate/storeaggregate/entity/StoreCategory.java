package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity;

import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Data
public class StoreCategory {
    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @ManyToOne
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JoinColumn(name = "store_id")
    private Store store;
}
