package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
@Data
@Table(name = "voucher_usage")
public class VoucherUsage {
    @Id
    private String id;

    @ManyToOne
    @JoinColumn(name = "voucher_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Voucher voucher;

    @ManyToOne
    @JoinColumn(name = "buyer_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private User buyer;

    @Column(name = "remaining_times")
    private Integer remainingTimes;
}
