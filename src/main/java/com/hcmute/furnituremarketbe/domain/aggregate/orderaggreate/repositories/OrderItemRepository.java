package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface OrderItemRepository extends JpaRepository<OrderItem, String> {
    @Query("SELECT o FROM OrderItem o WHERE o.id=:id")
    OrderItem getOrderItemById(@Param("id") String id);

    @Query("SELECT o FROM OrderItem o WHERE o.product.id=:id")
    OrderItem getOrderItemByProductId(@Param("id") String id);

    @Query("SELECT o FROM OrderItem o WHERE o.order.id=:id")
    List<OrderItem> getOrderItemByOrder(@Param("id") String id);
}
