package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Withdrawal;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.WithdrawStatus;
import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Data
public class WithdrawalResponse {
    private String id;
    private Double value;
    private WithdrawStatus status;
    private String accountNumber;
    private String bankName;
    private String ownerName;
    private String createdAt;
    private String updatedAt;

    public WithdrawalResponse(Withdrawal withdrawal) {
        this.id = withdrawal.getId();
        this.value = withdrawal.getValue();
        this.status = withdrawal.getStatus();
        this.accountNumber = withdrawal.getAccountNumber();
        this.bankName = withdrawal.getBankName();
        this.ownerName = withdrawal.getOwnerName();
        this.createdAt = zonedDateTimeToString(withdrawal.getCreatedAt());
        this.updatedAt = zonedDateTimeToString(withdrawal.getUpdatedAt());
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
