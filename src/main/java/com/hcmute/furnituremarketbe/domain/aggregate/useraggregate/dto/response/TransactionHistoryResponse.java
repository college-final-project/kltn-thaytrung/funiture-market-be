package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.TransactionHistory;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.TransactionType;
import lombok.Data;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

@Data
public class TransactionHistoryResponse {
    private String id;
    private String createdAt;
    private TransactionType type;
    private Double value;
    private String ownerName;

    public TransactionHistoryResponse(TransactionHistory history, User user) {
        this.id = history.getId();
        this.createdAt = zonedDateTimeToString(history.getCreatedAt());
        this.type = history.getType();
        this.value = history.getValue();
        this.ownerName = user.getFullName();
    }

    private String zonedDateTimeToString(ZonedDateTime date) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm dd-MM-yyyy");
        return date.format(formatter);
    }
}
