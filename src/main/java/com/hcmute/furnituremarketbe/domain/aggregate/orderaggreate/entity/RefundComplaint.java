package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.converter.ListStringConverter;
import jakarta.persistence.*;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class RefundComplaint {
    @Id
    private String id;

    @Column(name = "reason")
    private String reason;

    @Column(name = "done")
    private boolean done;

    @Convert(converter = ListStringConverter.class)
    @Column(name = "images", columnDefinition = "TEXT")
    private List<String> images = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "order_refund_id", referencedColumnName = "id")
    private OrderRefund orderRefund;
}
