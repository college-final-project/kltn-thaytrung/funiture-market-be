package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.ShopMarketing;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreIdentifierRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreInfoRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreTaxRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.AdminStoreResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.BuyerStoreResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response.SellerStoreResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.StoreRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.ConnectionRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreService;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
public class StoreServiceImpl implements StoreService {
    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ConnectionRepository connectionRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private FileStorageService storageService;

    @Override
    public ResponseBaseAbstract getSellerStoreInfo(User seller) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+seller.getId());
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(seller.getId());
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(seller.getId());
        String ownerName = seller.getFullName();
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(p -> p.getTotalReviewPoint()).sum();
        Integer reviewAmount = productList.stream().mapToInt(p -> p.getReviewAmount()).sum();
        Double avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        SellerStoreResponse response = new SellerStoreResponse(store, ownerName, numFollower,numFollowing,avgReviewStar,reviewAmount);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        List<String> identifier = response.getIdentifier();
        if (identifier.size() == 4)
        {
            identifier.set(2, this.storageService.getImageUrl(identifier.get(2)));
            identifier.set(3, this.storageService.getImageUrl(identifier.get(3)));
        }
        response.setIdentifier(identifier);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract viewStore(User user, String storeId) {
        Store store = this.storeRepository.getStoreById(storeId);
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+storeId);
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(storeId);
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(storeId);
        boolean followed = false;
        if (user != null) {
            followed = this.connectionRepository.existConnection(storeId, user.getId());
        }
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(Product::getTotalReviewPoint).sum();
        Integer reviewAmount = productList.stream().mapToInt(Product::getReviewAmount).sum();
        Double avgReviewStar = 0.0;
        if (reviewAmount != 0)
            avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        BuyerStoreResponse response = new BuyerStoreResponse(store, numFollower,numFollowing, avgReviewStar, reviewAmount, productList.size(),followed);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract updateStoreInfo(User seller, UpdateStoreInfoRequest request) throws IOException {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+seller.getId());
        store.setShopName(request.getShopName());
        store.setAddress(request.getAddress());
        if (request.getLogo() != null)
            store.setLogo(this.storageService.save(request.getLogo()));
        if (request.getTopBanner() != null)
            store.setTopBanner(this.storageService.save(request.getTopBanner()));
        if (request.getInfoBanner() != null)
            store.setInfoBanner(this.storageService.save(request.getInfoBanner()));
        store.setDescription(request.getDescription());
        this.storeRepository.save(store);
        seller.setFullName(request.getOwnerName());
        this.userRepository.save(seller);
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(seller.getId());
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(seller.getId());
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(p -> p.getTotalReviewPoint()).sum();
        Integer reviewAmount = productList.stream().mapToInt(p -> p.getReviewAmount()).sum();
        Double avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        SellerStoreResponse response = new SellerStoreResponse(store, seller.getFullName(), numFollower, numFollowing,avgReviewStar,reviewAmount);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        List<String> identifier = response.getIdentifier();
        if (identifier.size() == 4) {
            identifier.set(2, this.storageService.getImageUrl(identifier.get(2)));
            identifier.set(3, this.storageService.getImageUrl(identifier.get(3)));
        }
        response.setIdentifier(identifier);
        return SuccessResponse.builder()
                .addMessage("Cập nhật dữ liệu thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updateStoreTax(User seller, UpdateStoreTaxRequest request) {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+seller.getId());
        List<String> tax = new ArrayList<>(List.of(request.getTaxType(),request.getTaxCode()));
        store.setTax(tax);
        if (store.getIdentifier().size() == 4 && !store.isQualified())
            store.setQualified(true);
        this.storeRepository.save(store);
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(seller.getId());
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(seller.getId());
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(p -> p.getTotalReviewPoint()).sum();
        Integer reviewAmount = productList.stream().mapToInt(p -> p.getReviewAmount()).sum();
        Double avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        SellerStoreResponse response = new SellerStoreResponse(store, seller.getFullName(), numFollower, numFollowing, avgReviewStar,reviewAmount);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        List<String> identifier = response.getIdentifier();
        if (identifier.size() == 4) {
            identifier.set(2, this.storageService.getImageUrl(identifier.get(2)));
            identifier.set(3, this.storageService.getImageUrl(identifier.get(3)));
            response.setIdentifier(identifier);
        }
        if (!store.isQualified()) {
            return SuccessResponse.builder()
                    .addMessage("Bổ sung thông tin định danh để hoàn tất thông tin cửa hàng.")
                    .setData(response)
                    .returnUpdated();
        }
        return SuccessResponse.builder()
                .addMessage("Cập nhật thông tin thuế thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract updateStoreIdentifier(User seller, UpdateStoreIdentifierRequest request) throws IOException {
        Store store = this.storeRepository.getStoreById(seller.getId());
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+seller.getId());
        if (request.getIdentifierFrontImg() != null && request.getIdentifierBackImg() != null && request.getIdentifierCode() != null && request.getIdentifierFullName() != null) {
            String identifierFrontImg = this.storageService.save(request.getIdentifierFrontImg());
            String identifierBackImg = this.storageService.save(request.getIdentifierBackImg());
            String identifierCode = request.getIdentifierCode();
            String identifierFullName = request.getIdentifierFullName();
            List<String> identifier = new ArrayList<>(List.of(identifierCode, identifierFullName, identifierFrontImg, identifierBackImg));
            store.setIdentifier(identifier);
        }
        if (store.getTax().size() == 2 && !store.isQualified()) {
            store.setQualified(true);
        }
        this.storeRepository.save(store);
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(seller.getId());
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(seller.getId());
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(p -> p.getTotalReviewPoint()).sum();
        Integer reviewAmount = productList.stream().mapToInt(p -> p.getReviewAmount()).sum();
        Double avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        SellerStoreResponse response = new SellerStoreResponse(store, seller.getFullName(), numFollower, numFollowing, avgReviewStar,reviewAmount);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        List<String> identifier = response.getIdentifier();
        if (identifier.size() == 4) {
            identifier.set(2, this.storageService.getImageUrl(identifier.get(2)));
            identifier.set(3, this.storageService.getImageUrl(identifier.get(3)));
        }
        response.setIdentifier(identifier);
        if (!store.isQualified()) {
            return SuccessResponse.builder()
                    .addMessage("Bổ sung thông tin thuế để hoàn tất thông tin cửa hàng")
                    .setData(response)
                    .returnUpdated();
        }
        return SuccessResponse.builder()
                .addMessage("Cập nhật thông tin định danh thành công")
                .setData(response)
                .returnUpdated();
    }

    @Override
    public ResponseBaseAbstract marketingStore() {
        List<Store> storeList = this.storeRepository.findAll();
        List<ShopMarketing> marketingStoreList = storeList.stream().map(ShopMarketing::new).toList();
        System.out.println(marketingStoreList.get(1).getIncome() + " " + marketingStoreList.get(1).getAvgRating());
        List<ShopMarketing> qualifiedStoreList = marketingStoreList.stream()
                .filter(s -> s.getIncome() >= 10000000 && s.getAvgRating() >= 4.7)
                .collect(Collectors.toList());
        if (qualifiedStoreList != null && qualifiedStoreList.size() != 0)
            qualifiedStoreList.sort(Comparator.comparingDouble(ShopMarketing::getIncome).reversed());

        assert qualifiedStoreList != null;
        for (ShopMarketing store : qualifiedStoreList) {
            store.setLogo(this.storageService.getImageUrl(store.getLogo()));
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(qualifiedStoreList)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getAllStore() {
        List<Store> stores = this.storeRepository.findAll();
        List<AdminStoreResponse> responses = new ArrayList<>();
        for (Store store : stores) {
            User seller = this.userRepository.getUserById(store.getId());
            AdminStoreResponse response = getStoreResponse(seller, store);
            responses.add(response);
        }
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liêu thành công")
                .setData(responses)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract getStoreById(String id) {
        Store store = this.storeRepository.getStoreById(id);
        if (store == null)
            throw ServiceExceptionFactory.notFound()
                    .addMessage("Không tìm thấy cửa hàng với id = "+id);
        User seller = this.userRepository.getUserById(id);
        AdminStoreResponse response = getStoreResponse(seller, store);
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(response)
                .returnGetOK();
    }

    @Override
    public ResponseBaseAbstract searchFilterStore(Map<String, String> queries) {
        PagingAndSortingResponse<Store> storePage = this.storeRepository.searchStore(queries);
        List<Store> stores = storePage.getContent();
        List<AdminStoreResponse> responses = new ArrayList<>();
        for (Store store : stores) {
            User seller = this.userRepository.getUserById(store.getId());
            AdminStoreResponse response = getStoreResponse(seller, store);
            responses.add(response);
        }
        PagingAndSortingResponse<AdminStoreResponse> data = new PagingAndSortingResponse<>(
                responses,
                storePage.getCurrentPage(),
                storePage.getPageSize(),
                storePage.getTotalRecords(),
                storePage.getTotalPages()
        );
        return SuccessResponse.builder()
                .addMessage("Lấy dữ liệu thành công")
                .setData(data)
                .returnGetOK();
    }

    private AdminStoreResponse getStoreResponse(User seller, Store store) {
        Integer numFollower = this.connectionRepository.getNumOfFollowerOfUser(seller.getId());
        Integer numFollowing = this.connectionRepository.getNumOfFollowingAccount(seller.getId());
        List<Product> productList = store.getProducts();
        Integer totalReviewPoint = productList.stream().mapToInt(p -> p.getTotalReviewPoint()).sum();
        Integer reviewAmount = productList.stream().mapToInt(p -> p.getReviewAmount()).sum();
        Double avgReviewStar = totalReviewPoint.doubleValue()/reviewAmount;
        AdminStoreResponse response = new AdminStoreResponse(store, seller.getFullName(), numFollower, numFollowing, avgReviewStar,reviewAmount);
        if (response.getLogo() != null)
            response.setLogo(this.storageService.getImageUrl(response.getLogo()));
        if (response.getTopBanner() != null)
            response.setTopBanner(this.storageService.getImageUrl(response.getTopBanner()));
        if (response.getInfoBanner() != null)
            response.setInfoBanner(this.storageService.getImageUrl(response.getInfoBanner()));
        List<String> identifier = response.getIdentifier();
        if (identifier.size() == 4) {
            identifier.set(2, this.storageService.getImageUrl(identifier.get(2)));
            identifier.set(3, this.storageService.getImageUrl(identifier.get(3)));
        }
        response.setIdentifier(identifier);
        return response;
    }
}
