package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.Voucher;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.VoucherProduct;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface VoucherProductRepository extends JpaRepository<VoucherProduct, String> {
    @Query("SELECT v FROM VoucherProduct v WHERE v.voucher.id=:voucherId AND v.product.id=:productId")
    VoucherProduct getByVoucherIdAndProductId(@Param("voucherId") String voucherId, @Param("productId") String productId);

    @Query("SELECT v FROM VoucherProduct vp INNER JOIN Voucher v ON vp.voucher.id = v.id WHERE vp.product.id=:productId AND v.hidden=false" +
            " AND FUNCTION('DATE', v.startDate) <= FUNCTION('DATE', :givenDate) AND FUNCTION('DATE', v.endDate) >= FUNCTION('DATE', :givenDate)")
    List<Voucher> getVoucherByProduct(@Param("productId") String productId, @Param("givenDate")Date givenDate);

    @Query("SELECT v FROM VoucherProduct v WHERE v.voucher.id=:id")
    List<VoucherProduct> getVoucherProductByVoucherId(@Param("id") String id);

    @Query("SELECT p FROM VoucherProduct vp INNER JOIN Product p ON vp.product.id = p.id WHERE vp.voucher.id=:voucherId")
    List<Product> getProductByVoucher(@Param("voucherId") String voucherId);
}
