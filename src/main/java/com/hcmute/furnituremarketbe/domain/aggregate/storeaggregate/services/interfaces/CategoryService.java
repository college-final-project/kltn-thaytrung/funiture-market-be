package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

import java.io.IOException;

public interface CategoryService {
    ResponseBaseAbstract createStoreCategory(String storeId, CreateCategoryRequest request);

    ResponseBaseAbstract getAllStoreCategory(String storeId);

    ResponseBaseAbstract getStoreCategoryById(String storeId, String id);

    ResponseBaseAbstract updateStoreCategory(UpdateStoreCategoryRequest request);

    ResponseBaseAbstract deleteStoreCategory(String id);

    ResponseBaseAbstract createSystemCategory(CreateSystemCategoryRequest request) throws IOException;

    ResponseBaseAbstract getAllSystemCategory();

    ResponseBaseAbstract getSystemCategoryById(String id);

    ResponseBaseAbstract updateSystemCategory(UpdateSystemCategoryRequest request) throws IOException;

    ResponseBaseAbstract deleteSystemCategory(String id) throws IOException;
}
