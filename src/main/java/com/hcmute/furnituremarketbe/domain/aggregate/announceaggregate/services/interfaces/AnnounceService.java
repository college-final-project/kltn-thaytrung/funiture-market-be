package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.OrderResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.VoucherResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.OrderRefund;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Report;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Withdrawal;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface AnnounceService {
    void onUpdatedOrder(OrderResponse orderResponse);

    ResponseBaseAbstract getAnnounceByType(User user, AnnounceType type, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract markAsRead(String announceId);

    ResponseBaseAbstract markAsReadForAll(User user);

    void onCreatedVoucher(VoucherResponse voucherResponse, Store store);

    void onUpdatedUserAccount(User user, String message);

    void onProcessedReport(Report report);

    void onDoneReport(Report report, String message);

    void onUpdatedWithdrawal(Withdrawal withdrawal);

    void onDeclineOrderRefund(OrderRefund refund);
}
