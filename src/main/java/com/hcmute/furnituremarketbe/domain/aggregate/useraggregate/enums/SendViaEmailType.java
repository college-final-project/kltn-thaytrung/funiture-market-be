package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums;

public enum SendViaEmailType {
    REGISTER_USER,
    REGISTER_SELLER,
    RESET_PASSWORD
}
