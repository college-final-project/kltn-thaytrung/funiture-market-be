package com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.enums;

public enum ChatMessageOneToOneType {
    JOIN,
    MESSAGE,
    IMAGE,
    SEEN
}
