package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UseVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public interface VoucherService {
    ResponseBaseAbstract createVoucher(User seller, CreateVoucherRequest request);

    ResponseBaseAbstract updateVoucher(User seller, UpdateVoucherRequest request);

    ResponseBaseAbstract deleteVoucher(User seller, String voucherId);

    ResponseBaseAbstract useVoucher(User buyer, UseVoucherRequest request);

    ResponseBaseAbstract getStoreVouchers(String storeId);

    ResponseBaseAbstract getVoucherByProduct(User buyer, String productId);
}
