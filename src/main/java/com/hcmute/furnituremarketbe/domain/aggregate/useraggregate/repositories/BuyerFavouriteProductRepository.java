package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.buyer.BuyerFavouriteProduct;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface BuyerFavouriteProductRepository extends JpaRepository<BuyerFavouriteProduct, String> {
    @Query("SELECT p FROM BuyerFavouriteProduct b INNER JOIN Product p ON b.product.id = p.id WHERE b.buyer.id=:id")
    List<Product> getFavouriteProductByBuyerId(@Param("id") String id);

    @Query("SELECT CASE WHEN COUNT(b) > 0 THEN TRUE ELSE FALSE END FROM BuyerFavouriteProduct b WHERE b.product.id=:productId" +
            " AND b.buyer.id=:buyerId")
    boolean existByBuyerIdAndProductId(@Param("productId") String productId, @Param("buyerId") String buyerId);

    @Query("SELECT b FROM BuyerFavouriteProduct b WHERE b.product.id=:productId AND b.buyer.id=:buyerId")
    BuyerFavouriteProduct getByBuyerIdAndProductId(@Param("productId") String productId, @Param("buyerId") String buyerId);
}
