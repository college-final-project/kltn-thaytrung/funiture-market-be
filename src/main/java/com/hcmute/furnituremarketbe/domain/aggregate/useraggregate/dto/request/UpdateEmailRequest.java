package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request;

import lombok.Data;

@Data
public class UpdateEmailRequest {
    private String otpCode;
    private String newEmail;
}
