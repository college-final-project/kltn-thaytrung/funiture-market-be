package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend.StoreReviewExtendRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface StoreReviewRepository extends JpaRepository<StoreReview, String>, StoreReviewExtendRepository {
    @Query("SELECT s FROM StoreReview s WHERE s.reply is null")
    Page<StoreReview> getNotReplyReviews(Pageable pageable);

    @Query("SELECT s FROM StoreReview s WHERE s.reply is not null")
    Page<StoreReview> getReplyReviews(Pageable pageable);

    @Query("SELECT s FROM StoreReview s WHERE s.id=:id")
    StoreReview getStoreReviewById(@Param("id") String id);
}
