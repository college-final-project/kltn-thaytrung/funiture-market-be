package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.extend;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.StoreReview;
import com.hcmute.furnituremarketbe.domain.base.PagingAndSortingResponse;

import java.util.Map;

public interface StoreReviewExtendRepository {
    PagingAndSortingResponse<StoreReview> searchStoreReview(Map<String, String> queries);
}
