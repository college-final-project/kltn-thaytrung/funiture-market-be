package com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import lombok.Data;

@Data
public class WalletResponse {
    private String id;
    private Double value;

    public WalletResponse(Wallet wallet) {
        this.id = wallet.getId();
        this.value = wallet.getValue();
    }
}
