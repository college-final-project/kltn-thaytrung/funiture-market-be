package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.VoucherType;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import jakarta.persistence.*;
import lombok.Data;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import java.util.Date;

@Entity
@Data
public class Voucher {
    @Id
    private String id;

    @Column(name = "name")
    private String name;

    @Enumerated(EnumType.STRING)
    private VoucherType type;

    @Column(name = "code")
    private String code;

    @Column(name = "start_date")
    private Date startDate;

    @Column(name = "end_date")
    private Date endDate;

    @Column(name = "max_discount")
    private Double maxDiscount;

    @Column(name = "min_value")
    private Double minValue;

    @Column(name = "total_times")
    private Integer totalTimes;

    @Column(name = "buyer_limit")
    private Integer buyerLimit;

    @Column(name = "hidden")
    private boolean hidden;

    @ManyToOne
    @JoinColumn(name = "store_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private Store store;
}
