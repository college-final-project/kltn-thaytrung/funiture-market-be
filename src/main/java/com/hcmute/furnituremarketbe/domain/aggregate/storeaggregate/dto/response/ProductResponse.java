package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.response;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class ProductResponse {
    private String id;
    private String name;
    private Date createdAt;
    private boolean onDisplay;
    private boolean onSale;
    private boolean used;
    private boolean hidden;
    private Double price;
    private Double salePrice;
    private String description;
    private Integer weight;
    private Integer height;
    private Integer length;
    private Integer width;
    private String material;
    private Integer sold;
    private Integer inStock;
    private boolean featured;
    private String categoryName;
    private String thumbnail;
    private Integer reviewAmount;
    private Integer totalReviewPoint;
    private List<String> images;
    private List<String> storeCategories;
    private ShortStoreResponse storeInfo;
    private Boolean favourite;
    private Integer numFav;
    private Integer views;

    public ProductResponse(Product product) {
        this.id = product.getId();
        this.name = product.getName();
        this.createdAt = product.getCreatedAt();
        this.onDisplay = product.isOnDisplay();
        this.onSale = product.isOnSale();
        this.used = product.isUsed();
        this.hidden = product.isHidden();
        this.price = product.getPrice();
        this.salePrice = product.getSalePrice() != null ? product.getSalePrice() : null;
        this.description = product.getDescription();
        this.weight = product.getWeight();
        this.height = product.getHeight();
        this.length = product.getLength();
        this.width = product.getWidth();
        this.material = product.getMaterial();
        this.sold = product.getSold();
        this.inStock = product.getInStock();
        this.featured = product.isFeatured();
        this.categoryName = product.getCategory() != null ? product.getCategory().getName() : null;
        this.thumbnail = product.getThumbnail();
        this.reviewAmount = product.getReviewAmount();
        this.totalReviewPoint = product.getTotalReviewPoint();
        this.images = product.getImages();
        this.storeCategories = product.getStoreCategories();
        this.numFav = product.getBuyerFavouriteProducts() != null ? product.getBuyerFavouriteProducts().size() : 0;
    }
}
