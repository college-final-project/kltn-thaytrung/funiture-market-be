package com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.dto;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.WithdrawalResponse;
import lombok.Data;

@Data
public class UpdatedWithdrawalNotification {
    private WithdrawalResponse response;
    private String message;

    public UpdatedWithdrawalNotification(WithdrawalResponse response, String message) {
        this.response = response;
        this.message = message;
    }
}
