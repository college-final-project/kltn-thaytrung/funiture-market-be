package com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.AddToStoreCategoryListOfProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface ProductService {
    ResponseBaseAbstract createProduct(CreateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException;

    ResponseBaseAbstract getProductByStoreCategoryName(String categoryName);

    ResponseBaseAbstract updateProduct(UpdateProductRequest request, MultipartFile thumbnail, List<MultipartFile> images) throws IOException;

    ResponseBaseAbstract deleteProduct(String toCheckStoreId, String id) throws IOException;

    ResponseBaseAbstract getAllStoreProduct(String storeId);

    ResponseBaseAbstract getAllProduct();

    ResponseBaseAbstract getProductById(User buyer, String id);

    ResponseBaseAbstract getMostBuyingProducts(User buyer, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getMostFavouriteProducts(User buyer, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract getAllProductIsMarketing(User buyer, Integer currentPage, Integer pageSize);

    ResponseBaseAbstract searchAndFilterProduct(User buyer, Map<String, String> queries);

    ResponseBaseAbstract getRecommendationProducts(User buyer, Integer offset, Integer limit, boolean isExplicit);

    ResponseBaseAbstract addToStoreCategoryListOfProduct(AddToStoreCategoryListOfProductRequest request);
}
