package com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.response.ShippingFeeResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.DeliveryAddress;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.entity.District;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.DeliveryAddressRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.repositories.DistrictRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Product;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.entity.Store;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.repositories.ProductRepository;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class GHNService {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private DeliveryAddressRepository deliveryAddressRepository;

    @Autowired
    private DistrictRepository districtRepository;

    public ResponseBaseAbstract shippingFeeCalculate(ShippingFeeCalculateRequest request) throws JsonProcessingException {
        Long finalFee = 0L;
        DeliveryAddress deliveryAddress = this.deliveryAddressRepository.getDeliveryAddressById(request.getDeliveryAddressId());
        if (deliveryAddress == null)
            throw ServiceExceptionFactory.notFound().addMessage("Không tồn tại địa chỉ với id = " + request.getDeliveryAddressId());

        Integer serviceTypeId = 2;
        Integer toDistrictId = deliveryAddress.getTown().getDistrict().getGhnDistrictId();
        String toWardCode = deliveryAddress.getTown().getGhnWardCode();
        if (request.getOrderItemRequests() != null && request.getOrderItemRequests().size() > 0) {
            for (OrderItemRequest orderItemRequest : request.getOrderItemRequests()) {
                String productId = orderItemRequest.getProductId();
                Integer quantity = orderItemRequest.getQuantity();
                Product product = this.productRepository.getProductById(productId);
                if (product == null) {
                    throw ServiceExceptionFactory.notFound()
                            .addMessage("Không tìm thấy sản phẩm với id = "+productId);
                }
                Store store = product.getStore();
                String address = store.getAddress();
                String[] parts = address.split(", ");
                District district = this.districtRepository.getDistrictByName(parts[2]);
                Integer fromDistrictId = district.getGhnDistrictId();
                int insuranceValue = 1000000;
                int weight = product.getWeight() * quantity;
                int height = product.getHeight() * quantity;
                int length = product.getLength() * quantity;
                int width = product.getWidth() * quantity;
                GHNFeeCalculatePayload payload = new GHNFeeCalculatePayload();
                payload.setFromDistrictId(fromDistrictId);
                payload.setServiceTypeId(serviceTypeId);
                payload.setToDistrictId(toDistrictId);
                payload.setToWardCode(toWardCode);
                payload.setHeight(height);
                payload.setWeight(weight);
                payload.setLength(length);
                payload.setWidth(width);
                payload.setInsuranceValue(insuranceValue);
                Long fee = calculate(payload);
                finalFee += fee;
            }
        }
        return SuccessResponse.builder()
                .addMessage("Tính phí vận chuyển thành công")
                .setData(new ShippingFeeResponse(finalFee))
                .returnGetOK();
    }

    public Long calculate(GHNFeeCalculatePayload payload) throws JsonProcessingException {
        ObjectMapper objectMapper = new ObjectMapper();
        Long total=0L;
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set("Token", "81e2a47b-f3f4-11ee-aaec-aa8b07999c0d");
        String url = "https://online-gateway.ghn.vn/shiip/public-api/v2/shipping-order/fee";
        HttpEntity<GHNFeeCalculatePayload> requestEntity = new HttpEntity<>(payload, headers);
        ResponseEntity<String> responseEntity = restTemplate.exchange(url, HttpMethod.POST, requestEntity, String.class);
        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            String responseBody = responseEntity.getBody();
            JsonNode rootNode = objectMapper.readTree(responseBody);
            JsonNode dataNode = rootNode.path("data");
            total = dataNode.path("total").asLong();
        }
        return total;
    }
}
