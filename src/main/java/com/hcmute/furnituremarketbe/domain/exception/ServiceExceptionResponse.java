package com.hcmute.furnituremarketbe.domain.exception;


import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;

public class ServiceExceptionResponse extends ResponseBaseAbstract {
    public ServiceExceptionResponse() {
        super();
        this.setStatus("FAIL");
    }
}
