package com.hcmute.furnituremarketbe.file.storage;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.storage.Blob;
import com.google.cloud.storage.Bucket;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.StorageClient;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.domain.base.SuccessResponse;
import com.hcmute.furnituremarketbe.domain.exception.ServiceExceptionFactory;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Service
public class FileStorageServiceImpl implements FileStorageService{
    @Autowired
    private Properties properties;

    @EventListener
    public void init(ApplicationReadyEvent event) {
        try {
            ClassPathResource serviceAccount = new ClassPathResource("firebase.json");
            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount.getInputStream()))
                    .setStorageBucket(properties.getBucketName())
                    .build();
            FirebaseApp.initializeApp(options);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getImageUrl(String name) {
        return String.format(properties.imageUrl, name);
    }

    @Override
    public String save(MultipartFile file) throws IOException {
        if (!isValidImage(file)) {
            throw ServiceExceptionFactory.badRequest().addMessage("Sai định dạng hình");
        }
        Bucket bucket = StorageClient.getInstance().bucket();
        String name = generateFileName(file.getOriginalFilename());
        bucket.create(name, file.getBytes(), file.getContentType());

        return name;
    }

    @Override
    public ResponseBaseAbstract uploadImage(MultipartFile file) throws IOException {
        String name = save(file);
        Map<String, String> data = new HashMap<>();
        data.put("image-url", getImageUrl(name));
        return SuccessResponse.builder().addMessage("Upload thành công")
                .setData(data).returnCreated();
    }

    @Override
    public void delete(String name) throws IOException {
        Bucket bucket = StorageClient.getInstance().bucket();
        if (StringUtils.isEmpty(name)) {
            throw ServiceExceptionFactory.badRequest().addMessage("Tên file không hợp lệ");
        }
        Blob blob = bucket.get(name);
        if (blob == null) {
            throw ServiceExceptionFactory.notFound().addMessage("Không tìm thấy file");
        }
        blob.delete();
    }

    private String generateFileName(String originalName) {
        return UUID.randomUUID() + "." + getExtension(originalName);
    }

    private String getExtension(String originalName) {
        return StringUtils.getFilenameExtension(originalName);
    }

    private boolean isValidImage(MultipartFile image) {
        String[] validExtension = {"png", "jpg", "jpeg", "bmp"};
        String fileExtension = getExtension(image.getOriginalFilename()).toLowerCase();
        return Arrays.asList(validExtension).contains(fileExtension);
    }


    @Data
    @Configuration
    @ConfigurationProperties(prefix = "firebase")
    public class Properties {

        private String bucketName;

        private String imageUrl;
    }
}
