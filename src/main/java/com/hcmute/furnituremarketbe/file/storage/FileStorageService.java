package com.hcmute.furnituremarketbe.file.storage;

import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

public interface FileStorageService {
    String getImageUrl(String name);

    String save(MultipartFile file) throws IOException;

    ResponseBaseAbstract uploadImage(MultipartFile file) throws IOException;

    void delete(String name) throws IOException;
}
