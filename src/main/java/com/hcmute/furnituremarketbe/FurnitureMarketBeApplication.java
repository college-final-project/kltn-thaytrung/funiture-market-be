package com.hcmute.furnituremarketbe;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FurnitureMarketBeApplication {

    public static void main(String[] args) {
        SpringApplication.run(FurnitureMarketBeApplication.class, args);
    }

}
