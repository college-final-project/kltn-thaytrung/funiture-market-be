package com.hcmute.furnituremarketbe.configuration.security;

import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class CustomAccessDeniedHandler implements AccessDeniedHandler {

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
					   AccessDeniedException accessDeniedException) throws IOException, ServletException {
		response.resetBuffer();
		response.setStatus(HttpServletResponse.SC_FORBIDDEN);
		response.setHeader("Content-Type", "application/json");
		response.getOutputStream().write(String.format(
				"{\"message\":\"%s\",\"detail\":%s}", 
				"You do not have permission to access this resource",
				"\"FAIL\"").getBytes("UTF-8"));
		response.flushBuffer();
	}
}
