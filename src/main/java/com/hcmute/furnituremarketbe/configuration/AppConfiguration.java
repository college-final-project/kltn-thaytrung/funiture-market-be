package com.hcmute.furnituremarketbe.configuration;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.cache.VnpOtpCache;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.cache.OtpCache;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
    @Value("${com.hcmute.furnituremarketbe.security.otp.expired-after-mins}")
    private long expiredAfterMins;

    @Value("${com.hcmute.furnituremarketbe.security.otp.code-length}")
    private int otpLength;

    @Value("${com.hcmute.furnituremarketbe.security.vnp-otp.expired-after-mins}")
    private long vnpExpiredAfterMins;

    @Value("${com.hcmute.furnituremarketbe.security.vnp-otp.code-length}")
    private int vnpOtpLength;

    @Bean
    OtpCache otpCache() {
        return new OtpCache(expiredAfterMins, otpLength);
    }

    @Bean
    VnpOtpCache vnpOtpCache() {return new VnpOtpCache(vnpExpiredAfterMins, vnpOtpLength);}

}
