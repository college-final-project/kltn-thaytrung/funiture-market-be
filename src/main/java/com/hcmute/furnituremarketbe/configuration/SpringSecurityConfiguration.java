package com.hcmute.furnituremarketbe.configuration;



import com.hcmute.furnituremarketbe.configuration.security.CustomAccessDeniedHandler;
import com.hcmute.furnituremarketbe.configuration.security.CustomAuthenticationEntryPoint;
import com.hcmute.furnituremarketbe.controller.exception.CommonRuntimeException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.AuthenticationException;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.jwt.JwtAuthenticationFilter;
import com.hcmute.furnituremarketbe.oauth.CustomOAuthUserService;
import com.hcmute.furnituremarketbe.oauth.OAuthAuthenticationSuccessHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.servlet.util.matcher.MvcRequestMatcher;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.servlet.handler.HandlerMappingIntrospector;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
public class SpringSecurityConfiguration {

    @Autowired
    private CustomOAuthUserService oAuth2UserService;

    @Autowired
    private OAuthAuthenticationSuccessHandler oAuth2AuthenticationSuccessHandler;

    @Autowired
    @Lazy
    private UserRepository userRepository;

    @Autowired
    @Lazy
    private JwtAuthenticationFilter jwtAuthenticationFilter;

    @Autowired
    private CustomAuthenticationEntryPoint customAuthenticationEntryPoint;

    @Autowired
    private CustomAccessDeniedHandler customAccessDeniedHandler;

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity http, HandlerMappingIntrospector introspector) throws Exception {
        MvcRequestMatcher.Builder mvcMatcherBuilder = new MvcRequestMatcher.Builder(introspector);
        http
                .cors()
                .and()
                .csrf()
                .disable()
                .authorizeHttpRequests(rq -> rq
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/marketing-product/**")).hasAnyAuthority(UserRole.ADMIN_MARKETING.name(), UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/order/**")).hasAnyAuthority(UserRole.ADMIN_ORDER.name(), UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/report/**")).hasAnyAuthority(UserRole.ADMIN_REPORT.name(), UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/refund-complaint/**")).hasAnyAuthority(UserRole.ADMIN_CS.name(), UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/admin/**")).hasAuthority(UserRole.ADMIN.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/api/buyer/**")).hasAuthority(UserRole.BUYER.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("api/seller/**")).hasAnyAuthority(UserRole.SELLER.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("api/user/**")).hasAnyAuthority(UserRole.ADMIN.name(),UserRole.SELLER.name(),UserRole.BUYER.name())
                        .requestMatchers(mvcMatcherBuilder.pattern("/**")).permitAll())
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()
                .oauth2Login()
                .userInfoEndpoint().userService(oAuth2UserService)
                .and()
                .successHandler(oAuth2AuthenticationSuccessHandler)
                .and()
                .addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling(e -> {
                    e.authenticationEntryPoint(customAuthenticationEntryPoint);
                    e.accessDeniedHandler(customAccessDeniedHandler);
                });
        return http.build();
    }

    @Bean
    public UserDetailsService userDetailsService() {
        return new UserDetailsService() {
            @Override
            public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
                User userEntity = userRepository.getUserByEmail(email);
                if (userEntity == null) {
                    throw new UsernameNotFoundException("Không tìm thấy người dùng với email: " + email );
                }
                if (!userEntity.getEmailConfirmed())
                    throw new CommonRuntimeException("Vui lòng xác minh email để hoàn tất việc đăng ký");
                if (!userEntity.getActive())
                    throw new CommonRuntimeException("Tài khoản của bạn đã bị vô hiệu hóa");
                return userEntity;
            }
        };
    }

    @Bean
    protected PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    protected CorsFilter corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.setAllowedOriginPatterns(Arrays.asList(
                "http://localhost:5173",
                "http://localhost:3030",
                "https://furniture-market-admin-fe.vercel.app",
                "https://furniture-market-fe-latest.vercel.app",
                "https://www.fnest-admin.click",
                "https://www.fnest-mall.click"
        ));
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration("/**", config);
        return new CorsFilter(source);
    }
}
