package com.hcmute.furnituremarketbe.oauth;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.response.TokenResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.Wallet;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.enums.UserRole;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.UserRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.repositories.WalletRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.RefreshTokenService;
import com.hcmute.furnituremarketbe.jwt.JwtTokenProvider;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Random;
import java.util.UUID;

@Component
public class OAuthAuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	private final UserRepository userRepository;

	private final WalletRepository walletRepository;

	private final RefreshTokenService refreshTokenService;

	private static final Random rand = new Random();
	
	private final JwtTokenProvider jwtTokenProvider;
	
	@Value("${com.hcmute.furnituremarketdb.security.oauth2.fe-login-url}")
	String redirectUrl;

	@Value("${com.hcmute.furnituremarketbe.default-avatar}")
	private String defaultAvatar;
	
	public OAuthAuthenticationSuccessHandler(@Lazy UserRepository userRepository, @Lazy JwtTokenProvider jwtTokenProvider,
											 @Lazy WalletRepository walletRepository, @Lazy RefreshTokenService refreshTokenService) {
		this.userRepository = userRepository;
		this.jwtTokenProvider = jwtTokenProvider;
		this.walletRepository = walletRepository;
		this.refreshTokenService = refreshTokenService;
	}
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
										Authentication authentication) throws IOException, ServletException {
		
		OAuth2User user = (OAuth2User)authentication.getPrincipal();
		//CustomOAuthUser user = (CustomOAuthUser)oAuth2User;
		String email = user.getAttribute("email");
		System.out.println(email);
		User userEntity = new User();
		if (!this.userRepository.existsByEmail(email)) {
			userEntity.setId(UUID.randomUUID().toString());
			userEntity.setFullName(user.getAttribute("name"));
			userEntity.setEmail(email);
			userEntity.setRole(UserRole.BUYER);
			userEntity.setEmailConfirmed(true);
			userEntity.setActive(true);
			userEntity.setAvatar(this.defaultAvatar);

			this.userRepository.save(userEntity);
			Wallet wallet = new Wallet();
			wallet.setId(UUID.randomUUID().toString());
			wallet.setValue(0.0);
			wallet.setUser(userEntity);
			this.walletRepository.save(wallet);
		}
		else
			userEntity = this.userRepository.getUserByEmail(email);

		TokenResponse tokenResponse = this.refreshTokenService.generateLoginTokens(userEntity);

		
		getRedirectStrategy().sendRedirect(
				request, 
				response, 
				redirectUrl + "?token=" + tokenResponse.getAccessToken()+"&refresh-token="+tokenResponse.getRefreshToken());
		
		super.onAuthenticationSuccess(request, response, authentication);
	}
}
