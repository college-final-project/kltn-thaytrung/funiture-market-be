package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.enums.RepliedValue;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreReviewService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Map;

@RestController
@RequestMapping("api/seller/store")
public class SellerStoreController {
    @Autowired
    private StoreService storeService;

    @Autowired
    private StoreReviewService storeReviewService;

    @GetMapping("")
    public ResponseBaseAbstract getStoreInfo(@AuthenticationPrincipal User seller) {
        return this.storeService.getSellerStoreInfo(seller);
    }

    @PutMapping("/info")
    public ResponseBaseAbstract updateStoreInfo(@AuthenticationPrincipal User seller,
                                                @ModelAttribute UpdateStoreInfoRequest request) throws IOException {
        return this.storeService.updateStoreInfo(seller, request);
    }

    @PutMapping("/tax")
    public ResponseBaseAbstract updateStoreTax(@AuthenticationPrincipal User seller,
                                               @RequestBody UpdateStoreTaxRequest request) {
        return this.storeService.updateStoreTax(seller, request);
    }

    @PutMapping("/identifier")
    public ResponseBaseAbstract updateStoreIdentifier(@AuthenticationPrincipal User seller,
                                                      @ModelAttribute UpdateStoreIdentifierRequest request) throws IOException {
        return this.storeService.updateStoreIdentifier(seller, request);
    }

    @GetMapping("/review")
    public ResponseBaseAbstract searchAndFilterReview(@AuthenticationPrincipal User seller,
                                                      @RequestParam Map<String, String> queries) {
        queries.put("store.equals","id,"+seller.getId());
        return this.storeReviewService.searchFilterStoreReview(queries);
    }

    @GetMapping("/review-by-reply")
    public ResponseBaseAbstract getListReviewByReply(@RequestParam(defaultValue = "TRUE")RepliedValue isReply,
                                                     @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                     @RequestParam(required = false, defaultValue = "12") Integer pageSize,
                                                     @RequestParam(required = false) String sort) {
        return this.storeReviewService.getListReviewByReply(isReply, currentPage, pageSize, sort);
    }

    @PutMapping("/review-reply")
    public ResponseBaseAbstract replyReview(@RequestBody ReplyReviewRequest request) {
        return this.storeReviewService.replyReview(request);
    }
}
