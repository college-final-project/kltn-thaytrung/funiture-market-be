package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateRefundComplaintRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.RefundComplaintService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/seller/refund-complaint")
public class SellerRefundComplaintController {
    @Autowired
    private RefundComplaintService refundComplaintService;

    @PostMapping("")
    public ResponseBaseAbstract createRefundComplaint(@RequestPart("info") CreateRefundComplaintRequest request,
                                                      @RequestPart("images")List<MultipartFile> images) {
        return this.refundComplaintService.createRefundComplaint(request, images);
    }
}
