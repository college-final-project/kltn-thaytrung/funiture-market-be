package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StatisticService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/seller/statistic")
public class SellerStatisticController {
    @Autowired
    private StatisticService statisticService;

    @GetMapping("")
    public ResponseBaseAbstract getStoreStatistic(@AuthenticationPrincipal User seller,
                                                  @RequestParam(required = false) Integer month,
                                                  @RequestParam(required = false) Integer year) {
        return this.statisticService.getStoreStatistic(seller.getId(), month, year);
    }

    @GetMapping("/order-income")
    public ResponseBaseAbstract getOrderIncomeStatistic(@AuthenticationPrincipal User seller,
                                                        @RequestParam(required = false) Integer month,
                                                        @RequestParam(required = false) Integer year) {
        return this.statisticService.getStoreOrderIncome(seller.getId(), month, year);
    }
}
