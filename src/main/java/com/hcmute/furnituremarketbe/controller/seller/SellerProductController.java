package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.AddToStoreCategoryListOfProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("api/seller/product")
public class SellerProductController {
    @Autowired
    private ProductService productService;

    @PostMapping("")
    public ResponseBaseAbstract createProduct(@AuthenticationPrincipal User user,
                                              @RequestPart("info") CreateProductRequest request,
                                              @RequestPart("thumbnail") MultipartFile thumbnail,
                                              @RequestPart("images") List<MultipartFile> images) throws IOException {
        request.setStoreId(user.getId());
        return this.productService.createProduct(request, thumbnail, images);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllStoreProduct(@AuthenticationPrincipal User user) {
        return this.productService.getAllStoreProduct(user.getId());
    }

    @PatchMapping("/store-categories")
    public ResponseBaseAbstract AddToStoreCategoryListOfProduct(@RequestBody AddToStoreCategoryListOfProductRequest request) {
        return this.productService.addToStoreCategoryListOfProduct(request);
    }

    @GetMapping("/store-category")
    public ResponseBaseAbstract getProductByStoreCategory(@RequestParam String categoryName) {
        return this.productService.getProductByStoreCategoryName(categoryName);
    }


    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteProduct(@AuthenticationPrincipal User seller,
                                              @PathVariable String id) throws IOException {
        return this.productService.deleteProduct(seller.getId(),id);
    }

    @PatchMapping("/{id}")
    public ResponseBaseAbstract updateProduct(@AuthenticationPrincipal User seller,
                                              @PathVariable String id,
                                              @RequestPart(value = "info") UpdateProductRequest request,
                                              @RequestPart(value = "thumbnail", required = false) MultipartFile thumbnail,
                                              @RequestPart(value = "images", required = false) List<MultipartFile> images) throws IOException {
        request.setId(id);
        request.setStoreId(seller.getId());
        return this.productService.updateProduct(request, thumbnail, images);
    }
}
