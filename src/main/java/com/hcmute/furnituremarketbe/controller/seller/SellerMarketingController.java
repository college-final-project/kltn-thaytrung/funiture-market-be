package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateMarketingProductRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.MarketingProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/seller/marketing-product")
public class SellerMarketingController {
    @Autowired
    private MarketingProductService marketingProductService;

    @PostMapping("")
    public ResponseBaseAbstract createMarketingProduct(@AuthenticationPrincipal User seller,
                                                       @RequestBody CreateMarketingProductRequest request) {
        return this.marketingProductService.createMarketingProduct(seller, request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllMarketingProducts(@AuthenticationPrincipal User seller) {
        return this.marketingProductService.getStoreMarketingProducts(seller);
    }
}
