package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateOrderStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/seller/order")
public class SellerOrderController {
    @Autowired
    private OrderService orderService;

    @PatchMapping("/status")
    public ResponseBaseAbstract updateOrderStatus(@RequestBody UpdateOrderStatusRequest request) {
        return this.orderService.updateOrderStatus(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllOrderByStore(@AuthenticationPrincipal User seller) {
        return this.orderService.getAllOrderByStore(seller.getId());
    }
}
