package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportExplanationRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportExplanationService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/seller/report")
public class SellerReportController {
    @Autowired
    private ReportService reportService;

    @Autowired
    private ReportExplanationService reportExplanationService;

    @GetMapping("/product")
    private ResponseBaseAbstract getAllProductReport(@AuthenticationPrincipal User seller) {
        return this.reportService.getAllStoreProductReport(seller);
    }

    @PostMapping("/explanation")
    public ResponseBaseAbstract createReportExplanation(@RequestPart("info") CreateReportExplanationRequest request,
                                                        @RequestPart("images") List<MultipartFile> images) {
        return this.reportExplanationService.createReportExplanation(request, images);
    }
}
