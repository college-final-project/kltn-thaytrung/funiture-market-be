package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateStoreCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.CategoryService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("api/seller/category")
public class SellerCategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping("")
    public ResponseBaseAbstract createCategory(@AuthenticationPrincipal User user,
                                               @RequestBody CreateCategoryRequest request){
        return this.categoryService.createStoreCategory(user.getId(), request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateCategory(@PathVariable String id,
                                               @RequestBody UpdateStoreCategoryRequest request) {
        request.setId(id);
        return this.categoryService.updateStoreCategory(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteCategory(@PathVariable String id) {
        return this.categoryService.deleteStoreCategory(id);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllStoreCategory(@AuthenticationPrincipal User user) {
        return this.categoryService.getAllStoreCategory(user.getId());
    }
}
