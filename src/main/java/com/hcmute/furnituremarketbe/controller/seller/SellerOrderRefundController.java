package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderRefundService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/seller/order-refund")
public class SellerOrderRefundController {
    @Autowired
    private OrderRefundService orderRefundService;

    @PutMapping("/accepted/{id}")
    public ResponseBaseAbstract acceptOrderRefund(@PathVariable String id,
                                                  @RequestParam(required = true) Boolean accepted) {
        return this.orderRefundService.doneOrderRefund(id, accepted);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllOrderRefunds(@AuthenticationPrincipal User seller) {
        return this.orderRefundService.getOrderRefundByStore(seller.getId());
    }

    @GetMapping("/by-accepted")
    public ResponseBaseAbstract getStoreOrderRefundByAccepted(@AuthenticationPrincipal User seller,
                                                              @RequestParam(required = true) Boolean accepted) {
        return this.orderRefundService.getStoreOrderRefundByAccepted(seller.getId(), accepted);
    }
}
