package com.hcmute.furnituremarketbe.controller.seller;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.VoucherService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/seller/voucher")
public class SellerVoucherController {
    @Autowired
    private VoucherService voucherService;

    @GetMapping("")
    public ResponseBaseAbstract getAllStoreVoucher(@AuthenticationPrincipal User seller) {
        return this.voucherService.getStoreVouchers(seller.getId());
    }

    @PostMapping("")
    public ResponseBaseAbstract createVoucher(@AuthenticationPrincipal User seller,
                                              @RequestBody CreateVoucherRequest request) {
        return this.voucherService.createVoucher(seller, request);
    }

    @PatchMapping("/{id}")
    public ResponseBaseAbstract updateVoucher(@AuthenticationPrincipal User seller,
                                              @PathVariable String id,
                                              @RequestBody UpdateVoucherRequest request) {
        request.setId(id);
        return this.voucherService.updateVoucher(seller, request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteVoucher(@AuthenticationPrincipal User seller,
                                              @PathVariable String id) {
        return this.voucherService.deleteVoucher(seller, id);
    }
}
