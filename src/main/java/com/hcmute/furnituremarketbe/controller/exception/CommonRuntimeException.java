package com.hcmute.furnituremarketbe.controller.exception;

import javax.naming.AuthenticationException;

public class CommonRuntimeException extends RuntimeException {
	private static final long serialVersionUID = -189232057513615751L;

	public CommonRuntimeException(String message) {
		super(message);
	}

}
