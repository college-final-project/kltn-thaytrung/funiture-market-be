package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateOrderRefundRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderRefundService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("api/buyer/order-refund")
public class BuyerOrderRefundController {
    @Autowired
    private OrderRefundService orderRefundService;

    @PostMapping("")
    public ResponseBaseAbstract createOrderRefund(@RequestPart("info")CreateOrderRefundRequest request,
                                                  @RequestPart("images")List<MultipartFile> images) {
        return this.orderRefundService.createOrderRefund(request, images);
    }
}
