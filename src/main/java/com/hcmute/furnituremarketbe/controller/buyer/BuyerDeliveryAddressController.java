package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDefaultAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateDeliveryAddressRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.DeliveryAddressService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/buyer/delivery-address")
public class BuyerDeliveryAddressController {
    @Autowired
    private DeliveryAddressService deliveryAddressService;

    @GetMapping("")
    public ResponseBaseAbstract getAllDeliveryAdress(@AuthenticationPrincipal User user) {
        return this.deliveryAddressService.getAllDeliveryAddressByUser(user);
    }

    @PostMapping("")
    public ResponseBaseAbstract createDeliveryAddress(@AuthenticationPrincipal User user,
                                                      @RequestBody CreateDeliveryAddressRequest request) {
        return this.deliveryAddressService.createDeliveryAddress(user, request);
    }

    @PatchMapping("/default")
    public ResponseBaseAbstract updateDefaultAddress(@AuthenticationPrincipal User user,
                                                     @RequestBody UpdateDefaultAddressRequest request) {
        return this.deliveryAddressService.updateDefaultAddress(user, request);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateDeliveryAddress(@PathVariable String id,
                                                      @AuthenticationPrincipal User user,
                                                      @RequestBody UpdateDeliveryAddressRequest request) {
        request.setId(id);
        return this.deliveryAddressService.updateDeliveryAddress(user, request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteDeliveryAddress(@AuthenticationPrincipal User user,
                                                      @PathVariable String id) {
        return this.deliveryAddressService.deleteDeliveryAddress(user, id);
    }
}
