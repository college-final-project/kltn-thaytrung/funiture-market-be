package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UseVoucherRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.VoucherService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/buyer/voucher")
public class BuyerVoucherController {
    @Autowired
    private VoucherService voucherService;

    @PostMapping("/use")
    public ResponseBaseAbstract useVoucher(@AuthenticationPrincipal User buyer,
                                           @RequestBody UseVoucherRequest request) {
        return this.voucherService.useVoucher(buyer, request);
    }

    @GetMapping("/by-product/{id}")
    private ResponseBaseAbstract getAllVoucherByProductId(@AuthenticationPrincipal User buyer,
                                                          @PathVariable String id) {
        return this.voucherService.getVoucherByProduct(buyer, id);
    }
}
