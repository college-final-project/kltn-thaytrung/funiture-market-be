package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.BuyerFavouriteProductService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/buyer/favourite-product")
public class BuyerFavouriteProductController {
    @Autowired
    private BuyerFavouriteProductService buyerFavouriteProductService;

    @GetMapping("")
    public ResponseBaseAbstract getWishList(@AuthenticationPrincipal User buyer) {
        return this.buyerFavouriteProductService.getWishListByBuyer(buyer.getId());
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract toggleFavouriteProduct(@AuthenticationPrincipal User buyer,
                                                       @PathVariable String id) {
        return this.buyerFavouriteProductService.toggleFavouriteProduct(buyer.getId(), id);
    }
}
