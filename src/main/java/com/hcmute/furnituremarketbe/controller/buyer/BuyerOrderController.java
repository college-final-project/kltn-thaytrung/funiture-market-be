package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.BuyerOrderRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.CreateOrderRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.enums.OrderStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("api/buyer/order")
public class BuyerOrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("")
    public ResponseBaseAbstract getOrderByUser(@AuthenticationPrincipal User user,
                                               @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                               @RequestParam(required = false, defaultValue = "12") Integer pageSize)
    {
        return this.orderService.getAllOrderByUserId(user, currentPage, pageSize);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getOrderDetail(@PathVariable String id) {
        return this.orderService.getOrderDetail(id);
    }

    @PostMapping("")
    public ResponseBaseAbstract createOrder(@AuthenticationPrincipal User buyer,
                                            @RequestBody BuyerOrderRequest request) {
        return this.orderService.createOrder(buyer, request);
    }

    @PatchMapping("/{id}")
    public ResponseBaseAbstract cancelOrder(@PathVariable String id) {
        return this.orderService.cancelOrder(id);
    }

    @GetMapping("/by-status")
    public ResponseBaseAbstract getOrderByStatus(@AuthenticationPrincipal User user,
                                                 @RequestParam OrderStatus status,
                                                 @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                 @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.orderService.getByOrderStatus(user, status, currentPage, pageSize);
    }

    @PutMapping("/pay")
    public ResponseBaseAbstract pay(@AuthenticationPrincipal User buyer,
                                    @RequestParam String orderId) {
        return this.orderService.payAddition(buyer, orderId);
    }
}
