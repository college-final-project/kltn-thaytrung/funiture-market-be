package com.hcmute.furnituremarketbe.controller.buyer;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReviewRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreReviewService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/buyer/review")
public class BuyerReviewController {
    @Autowired
    private StoreReviewService storeReviewService;

    @PostMapping("")
    public ResponseBaseAbstract createReview(@AuthenticationPrincipal User buyer,
                                             @RequestBody CreateReviewRequest request) {
        return this.storeReviewService.createReview(buyer,request);
    }
}
