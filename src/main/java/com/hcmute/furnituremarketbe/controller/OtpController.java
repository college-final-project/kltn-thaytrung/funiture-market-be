package com.hcmute.furnituremarketbe.controller;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.SendOtpViaEmailRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.OtpService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/otp")
public class OtpController {
    @Autowired
    private OtpService otpService;

    @PostMapping("")
    public ResponseBaseAbstract sendOtpViaEmail(@RequestBody SendOtpViaEmailRequest request) {
        return otpService.sendOtpViaEmail(request);
    }
}
