package com.hcmute.furnituremarketbe.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.GHNFeeCalRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.GHNFeeCalculatePayload;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.ShippingFeeCalculateRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.GHNService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/shipping-fee")
public class ShippingFeeController {
    @Autowired
    private GHNService ghnService;

    @Autowired
    private OrderService orderService;

    @PostMapping("")
    public ResponseBaseAbstract shippingFeeCalculate(@RequestBody ShippingFeeCalculateRequest request) throws JsonProcessingException {
        return this.ghnService.shippingFeeCalculate(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllOrder() {
        return this.orderService.getAllOrder();
    }
}
