package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.ConnectionService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user/connection")
public class ConnectionController {
    @Autowired
    private ConnectionService connectionService;

    @GetMapping("/followers")
    public ResponseBaseAbstract getFollowers(@AuthenticationPrincipal User user) {
        return this.connectionService.getFollowers(user.getId());
    }

    @GetMapping("/following-people")
    public ResponseBaseAbstract getFollowingPeople(@AuthenticationPrincipal User user) {
        return this.connectionService.getFollowingPeople(user.getId());
    }

    @PostMapping("")
    public ResponseBaseAbstract toggle(@AuthenticationPrincipal User user,
                                       @RequestParam String id) {
        return this.connectionService.toggle(user, id);
    }
}
