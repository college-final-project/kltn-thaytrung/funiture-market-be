package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdateEmailRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdatePasswordRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdateProfileRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.UserService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api/user")
public class UserController {
    @Autowired
    private UserService userService;

    @GetMapping("")
    public ResponseBaseAbstract getUserProfile(@AuthenticationPrincipal User user) {
        return this.userService.getUserProfile(user);
    }

    @PatchMapping("")
    public ResponseBaseAbstract updateProfile(@AuthenticationPrincipal User user,
                                              @RequestBody UpdateProfileRequest request) {
        return this.userService.updateProfile(user, request);
    }

    @PatchMapping("/password")
    public ResponseBaseAbstract changePassword(@AuthenticationPrincipal User user,
                                               @RequestBody UpdatePasswordRequest request) {
        return this.userService.changePassword(user, request);
    }

    @PatchMapping("/avatar")
    public ResponseBaseAbstract updateAvatar(@AuthenticationPrincipal User user,
                                             @RequestParam("image") MultipartFile image) throws IOException {
        return this.userService.updateAvatar(user, image);
    }

    @PatchMapping("/email")
    public ResponseBaseAbstract updateEmail(@AuthenticationPrincipal User user,
                                            @RequestBody UpdateEmailRequest request) {
        return this.userService.updateEmail(user, request);
    }
}
