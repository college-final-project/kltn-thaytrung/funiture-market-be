package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.TransactionHistoryService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user/transaction-history")
public class TransactionHistoryController {
    @Autowired
    private TransactionHistoryService transactionHistoryService;

    @GetMapping("")
    public ResponseBaseAbstract getAllByUser(@AuthenticationPrincipal User user) {
        return this.transactionHistoryService.getAllTransactionHistoryByUser(user);
    }
}
