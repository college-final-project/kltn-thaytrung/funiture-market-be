package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/user/report")
public class ReportController {
    @Autowired
    private ReportService reportService;

    @PostMapping("")
    public ResponseBaseAbstract createReport(@RequestBody CreateReportRequest request) {
        return this.reportService.createReport(request);
    }
}
