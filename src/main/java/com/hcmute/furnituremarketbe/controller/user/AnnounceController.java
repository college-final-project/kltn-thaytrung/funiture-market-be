package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.enums.AnnounceType;
import com.hcmute.furnituremarketbe.domain.aggregate.announceaggregate.services.interfaces.AnnounceService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user/announce")
public class AnnounceController {
    @Autowired
    private AnnounceService announceService;

    @GetMapping("")
    public ResponseBaseAbstract getAnnounceByType(@AuthenticationPrincipal User user,
                                                  @RequestParam(defaultValue = "ORDER",required = false) AnnounceType type,
                                                  @RequestParam(defaultValue = "0", required = false) Integer currentPage,
                                                  @RequestParam(defaultValue = "12", required = false) Integer pageSize) {
        return this.announceService.getAnnounceByType(user, type, currentPage, pageSize);
    }

    @PatchMapping("/mark-as-read/{id}")
    public ResponseBaseAbstract markAsRead(@PathVariable String id) {
        return this.announceService.markAsRead(id);
    }

    @PatchMapping("/mark-as-read")
    public ResponseBaseAbstract markAsReadForAll(@AuthenticationPrincipal User user) {
        return this.announceService.markAsReadForAll(user);
    }
}
