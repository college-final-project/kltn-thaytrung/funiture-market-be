package com.hcmute.furnituremarketbe.controller.user;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.CreateWalletRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.ChargeMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.WithdrawMoneyRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.WalletService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/user/wallet")
public class WalletController {
    @Autowired
    private WalletService walletService;

    @PostMapping("")
    public ResponseBaseAbstract createWallet(@AuthenticationPrincipal User user,
                                              @RequestBody CreateWalletRequest request) {
        return this.walletService.createWallet(user, request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getWallet(@AuthenticationPrincipal User user) {
        return this.walletService.getWallet(user);
    }

    @PutMapping("/charge")
    public ResponseBaseAbstract chargeMoney(@AuthenticationPrincipal User user,
                                             @RequestBody ChargeMoneyRequest request) {
        return this.walletService.chargeMoney(user, request);
    }
}
