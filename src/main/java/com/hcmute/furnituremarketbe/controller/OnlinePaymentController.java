package com.hcmute.furnituremarketbe.controller;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.VNPaymentService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.UnsupportedEncodingException;

@RestController
@RequestMapping("api/payment")
public class OnlinePaymentController {
    @Autowired
    private VNPaymentService vnPaymentService;

    @GetMapping("/create-payment")
    public ResponseBaseAbstract createPayment(@AuthenticationPrincipal User user,
                                              HttpServletRequest req,
                                              @RequestParam long amount,
                                              @RequestParam(required = false, defaultValue = "NCB") String bankCode) throws UnsupportedEncodingException {
        return this.vnPaymentService.createPayment(req, amount, bankCode, user != null ? user.getEmail() : "nguyenngocduy");
    }
}
