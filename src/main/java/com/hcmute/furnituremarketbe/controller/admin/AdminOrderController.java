package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.dto.request.UpdateOrderStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/admin/order")
public class AdminOrderController {
    @Autowired
    private OrderService orderService;

    @GetMapping("")
    public ResponseBaseAbstract getAllOrders() {
        return this.orderService.getAllOrder();
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getOrderById(@PathVariable String id) {
        return this.orderService.getOrderDetail(id);
    }

    @GetMapping("/search")
    public ResponseBaseAbstract searchOrder(@RequestParam Map<String, String> queries) {
        return this.orderService.searchOrder(queries);
    }
}
