package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.UserService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/user")
public class AdminUserController {
    @Autowired
    private UserService userService;

    @PutMapping("/status/{id}")
    public ResponseBaseAbstract toggleUserStatus(@PathVariable String id) {
        return this.userService.toggleUserStatus(id);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllUser() {
        return this.userService.getAllUsers();
    }

    @GetMapping("/roles")
    public ResponseBaseAbstract getAdminRoles() {
        return this.userService.getListRole();
    }
}
