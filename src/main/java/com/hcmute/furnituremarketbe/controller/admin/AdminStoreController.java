package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderService;
import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.VoucherService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/admin/store")
public class AdminStoreController {
    @Autowired
    private StoreService storeService;

    @Autowired
    private OrderService orderService;

    @Autowired
    private VoucherService voucherService;

    @Autowired
    private ProductService productService;

    @GetMapping("")
    public ResponseBaseAbstract getAllStore() {
        return this.storeService.getAllStore();
    }

    @GetMapping("/search")
    public ResponseBaseAbstract searchStore(@RequestParam Map<String, String> queries) {
        return this.storeService.searchFilterStore(queries);
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getStoreById(@PathVariable String id) {
        return this.storeService.getStoreById(id);
    }

    @GetMapping("/{id}/order")
    public ResponseBaseAbstract getOrdersByStore(@PathVariable String id) {
        return this.orderService.getAllOrderByStore(id);
    }

    @GetMapping("/{id}/product")
    public ResponseBaseAbstract getProductsByStore(@PathVariable String id) {
        return this.productService.getAllStoreProduct(id);
    }

    @GetMapping("/{id}/voucher")
    public ResponseBaseAbstract getVoucherByStore(@PathVariable String id) {
        return this.voucherService.getStoreVouchers(id);
    }
}
