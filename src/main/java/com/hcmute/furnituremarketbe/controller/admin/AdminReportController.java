package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateReportStatusRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportExplanationService;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/report")
public class AdminReportController {
    @Autowired
    private ReportService reportService;

    @Autowired
    private ReportExplanationService reportExplanationService;

    @PutMapping("/status/{id}")
    public ResponseBaseAbstract updateReportStatus(@PathVariable String id,
                                                   @RequestBody UpdateReportStatusRequest request) {
        request.setId(id);
        return this.reportService.updateReportStatus(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllReports() {
        return this.reportService.getAllReports();
    }
}
