package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.OrderRefundService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/order-refund")
public class AdminOrderRefundController {
    @Autowired
    private OrderRefundService orderRefundService;

    @PutMapping("/accepted/{id}")
    public ResponseBaseAbstract acceptOrderRefund(@PathVariable String id,
                                                  @RequestParam(required = true) Boolean accepted) {
        return this.orderRefundService.doneOrderRefund(id, accepted);
    }
}
