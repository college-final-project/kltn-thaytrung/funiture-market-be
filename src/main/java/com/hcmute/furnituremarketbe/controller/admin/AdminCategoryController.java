package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.UpdateSystemCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.CategoryService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

@RestController
@RequestMapping("api/admin/category")
public class AdminCategoryController {
    @Autowired
    private CategoryService categoryService;

    @PostMapping("")
    public ResponseBaseAbstract createSystemCategory(@ModelAttribute CreateSystemCategoryRequest request) throws IOException {
        return this.categoryService.createSystemCategory(request);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllSystemCategories() {
        return this.categoryService.getAllSystemCategory();
    }

    @GetMapping("/{id}")
    public ResponseBaseAbstract getSystemCategoryById(@PathVariable String id) {
        return this.categoryService.getSystemCategoryById(id);
    }

    @PutMapping("/{id}")
    public ResponseBaseAbstract updateSystemCategory(@PathVariable String id,
                                                     @ModelAttribute UpdateSystemCategoryRequest request) throws IOException {
        request.setId(id);
        return this.categoryService.updateSystemCategory(request);
    }

    @DeleteMapping("/{id}")
    public ResponseBaseAbstract deleteSystemCategory(@PathVariable String id) throws IOException {
        return this.categoryService.deleteSystemCategory(id);
    }
}
