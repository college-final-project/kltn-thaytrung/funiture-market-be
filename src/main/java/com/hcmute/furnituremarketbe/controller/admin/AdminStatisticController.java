package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StatisticService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/admin/statistic")
public class AdminStatisticController {
    @Autowired
    private StatisticService statisticService;

    @GetMapping("")
    public ResponseBaseAbstract getAdminStatistic(@RequestParam(required = false) Integer month,
                                                  @RequestParam(required = false) Integer year) {
        return this.statisticService.getAdminStatistic(month, year);
    }
}
