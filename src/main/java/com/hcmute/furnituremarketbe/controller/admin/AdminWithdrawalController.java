package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.UpdateWithdrawMoneyStatus;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.WithdrawalService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/withdrawal")
public class AdminWithdrawalController {
    @Autowired
    private WithdrawalService withdrawalService;

    @GetMapping("")
    public ResponseBaseAbstract getAllWithdrawal() {
        return this.withdrawalService.getAllWithdrawal();
    }

    @PutMapping("/status/{id}")
    public ResponseBaseAbstract updateWithdrawalStatus(@PathVariable String id,
                                                       @RequestBody UpdateWithdrawMoneyStatus request) {
        request.setId(id);
        return this.withdrawalService.updateWithdrawalStatus(request);
    }
}
