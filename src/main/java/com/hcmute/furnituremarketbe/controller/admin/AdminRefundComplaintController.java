package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.RefundComplaintService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/admin/refund-complaint")
public class AdminRefundComplaintController {
    @Autowired
    private RefundComplaintService refundComplaintService;

    @GetMapping("")
    public ResponseBaseAbstract getAllRefundComplaints() {
        return this.refundComplaintService.getAllRefundComplaints();
    }

    @GetMapping("/by-done")
    public ResponseBaseAbstract getRefundComplaintsByDone(@RequestParam(required = true) Boolean done) {
        return this.refundComplaintService.getRefundByDone(done);
    }

    @PutMapping("/done/{id}")
    public ResponseBaseAbstract markAsDoneRefundComplaint(@PathVariable String id) {
        return this.refundComplaintService.markAsDone(id);
    }
}
