package com.hcmute.furnituremarketbe.controller.admin;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.MarketingProductService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/admin/marketing-product")
public class AdminMarketingProductController {
    @Autowired
    private MarketingProductService marketingProductService;

    @GetMapping("")
    public ResponseBaseAbstract getAllMarketingProduct(@RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                       @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.marketingProductService.getAllMarketingProduct(currentPage, pageSize);
    }
}
