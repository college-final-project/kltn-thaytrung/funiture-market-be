package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.services.interfaces.MessageService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/common/message")
public class CommonMessageController {

    @Autowired
    private MessageService messageService;

    public CommonMessageController() {

    }

//    @GetMapping("")
//    @ResponseStatus(HttpStatus.OK)
//    public ResponseBaseAbstract searchMessage(
//            @RequestParam Map<String, String> queries) {
//        ResponseBaseAbstract listMessageResponse = this.messageService.searchMessages(queries);
//        return listMessageResponse;
//    }

    @DeleteMapping("{id}/delete")
    @ResponseStatus(HttpStatus.OK)
    public ResponseBaseAbstract deleteMessage(
            @PathVariable String id) {
        ResponseBaseAbstract updateMessageResponse = this.messageService.deleteMessage(id);
        return updateMessageResponse;
    }


    @PostMapping("list-message")
    public ResponseBaseAbstract getListMessageWithOnePerson(@AuthenticationPrincipal User user,
                                                            @RequestParam String opponentId) {
        return this.messageService.getListMessageWithOnePerson(user, opponentId);

    }

    @GetMapping("{userId}/opponents")
    @ResponseStatus(HttpStatus.OK)
    public ResponseBaseAbstract getAllOpponents(
            @PathVariable String userId) {
        return this.messageService.getAllOpponents(userId);
    }
}
