package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateReportRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ReportService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/common/report")
public class CommonReportController {
    @Autowired
    private ReportService reportService;

    @GetMapping("/{id}")
    public ResponseBaseAbstract getReportById(@PathVariable String id) {
        return this.reportService.getReportById(id);
    }
}
