package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.dto.request.CreateCategoryRequest;
import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.CategoryService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/category")
public class CommonCategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/system")
    private ResponseBaseAbstract getAllSystemCategory() {
        return this.categoryService.getAllSystemCategory();
    }

    @GetMapping("/store/{id}")
    private ResponseBaseAbstract getAllCategoryOfStore(@PathVariable String id) {
        return this.categoryService.getAllStoreCategory(id);
    }
}
