package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/store")
public class CommonStoreController {
    @Autowired
    private StoreService storeService;

    @GetMapping("/{id}")
    public ResponseBaseAbstract viewStore(@AuthenticationPrincipal User user,
                                          @PathVariable String id) {
        return this.storeService.viewStore(user,id);
    }

    @GetMapping("/marketing")
    public ResponseBaseAbstract marketingStore() {
        return this.storeService.marketingStore();
    }
}
