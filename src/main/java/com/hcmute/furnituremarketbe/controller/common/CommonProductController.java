package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.ProductService;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("api/product")
public class CommonProductController {
    @Autowired
    private ProductService productService;

    @GetMapping("/{id}")
    public ResponseBaseAbstract getProductById(@AuthenticationPrincipal User buyer,
                                               @PathVariable String id) {
        return this.productService.getProductById(buyer, id);
    }

    @GetMapping("")
    public ResponseBaseAbstract getAllProduct() {
        return this.productService.getAllProduct();
    }

    @GetMapping("/search-filter")
    public ResponseBaseAbstract searchAndFilterProduct(@AuthenticationPrincipal User buyer,
                                                       @RequestParam Map<String, String> queries) {
        return this.productService.searchAndFilterProduct(buyer, queries);
    }

    @GetMapping("/recommend")
    public ResponseBaseAbstract getRecommendationProducts(@AuthenticationPrincipal User buyer,
                                                          @RequestParam(required = true) Boolean isExplicit,
                                                          @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                          @RequestParam(required = false, defaultValue = "10") Integer pageSize) {
        return this.productService.getRecommendationProducts(buyer, currentPage, pageSize, isExplicit);
    }

    @GetMapping("/marketing")
    public ResponseBaseAbstract getProductsIsMarketing(@AuthenticationPrincipal User buyer,
                                                       @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                       @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.productService.getAllProductIsMarketing(buyer, currentPage, pageSize);
    }

    @GetMapping("/most-buying")
    public ResponseBaseAbstract getMostBuyingProducts(@AuthenticationPrincipal User buyer,
                                                      @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                      @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.productService.getMostBuyingProducts(buyer, currentPage, pageSize);
    }

    @GetMapping("/most-favourite")
    public ResponseBaseAbstract getMostFavouriteProducts(@AuthenticationPrincipal User buyer,
                                                         @RequestParam(required = false, defaultValue = "0") Integer currentPage,
                                                         @RequestParam(required = false, defaultValue = "12") Integer pageSize) {
        return this.productService.getMostFavouriteProducts(buyer, currentPage, pageSize);
    }
}
