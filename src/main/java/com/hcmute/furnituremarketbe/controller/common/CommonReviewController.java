package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.storeaggregate.services.interfaces.StoreReviewService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@RestController
@RequestMapping("api/review")
public class CommonReviewController {
    @Autowired
    private StoreReviewService storeReviewService;

    @GetMapping("/search")
    public ResponseBaseAbstract getReview(@RequestParam Map<String, String> queries) {
        return this.storeReviewService.searchFilterProductReview(queries);
    }
}
