package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.orderaggreate.services.interfaces.DeliveryAddressService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/address")
public class CommonAddressController {
    @Autowired
    private DeliveryAddressService deliveryAddressService;

    @GetMapping("/province-city")
    public ResponseBaseAbstract getAllProvinceCity() {
        return this.deliveryAddressService.getAllProvinceCity();
    }

    @GetMapping("/district")
    public ResponseBaseAbstract getDistrictByProvince(@RequestParam Integer provinceId) {
        return this.deliveryAddressService.getDistrictByProvince(provinceId);
    }

    @GetMapping("/commune-ward-town")
    public ResponseBaseAbstract getWardByDistrict(@RequestParam Integer districtId) {
        return this.deliveryAddressService.getWardByDistrict(districtId);
    }
}
