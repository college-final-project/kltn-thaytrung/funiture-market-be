package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.FurnitureMarketBeApplication;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.enums.ChatMessageOneToOneType;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageOneToOne;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.model.ChatMessageResponse;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.repositories.MessageRepository;
import com.hcmute.furnituremarketbe.domain.aggregate.messageaggregate.services.interfaces.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

@Controller
public class ChatMessageController {
    @Autowired
    private MessageService messageService;
    @Autowired
    private MessageRepository messageRepository;

    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;

    private static final Logger logger = LoggerFactory.getLogger(FurnitureMarketBeApplication.class);
    @MessageMapping("/ws/secured/messenger")
    public void sendMessageOneToOne(@Payload ChatMessageOneToOne msg) {
        String sender = msg.getSenderId();
        String receiver = msg.getReceiverId();
        if(msg.getType() == ChatMessageOneToOneType.SEEN)
        {
            msg.setMessageAsRead();
            this.messageService.storeMessage(msg);
        }
        if (msg.getType() == ChatMessageOneToOneType.MESSAGE) {
            String message = msg.getMessage();
            msg.setCreatedAt(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
            ChatMessageResponse resp = this.messageService.storeMessage(msg);
            logger.info(String.format("WS-INFO: %s send to %s: %s", sender, receiver, message));

            String receiverEndPoint = "/ws/secured/messenger/user/" + receiver;
            simpMessagingTemplate.convertAndSend(receiverEndPoint,resp);

            String senderEndPoint = "/ws/secured/messenger/user/" + sender;
            simpMessagingTemplate.convertAndSend(senderEndPoint, resp);
        }
        else if (msg.getType() == ChatMessageOneToOneType.IMAGE) {
            // Send image
            String image = msg.getImage();
            msg.setCreatedAt(ZonedDateTime.now(ZoneId.of("Asia/Ho_Chi_Minh")));
            ChatMessageResponse resp = this.messageService.storeMessage(msg);

            logger.info(String.format("WS-INFO: %s send to %s: %s", sender, receiver, image));

            String receiverEndPoint = "/ws/secured/messenger/user/" + receiver;
            simpMessagingTemplate.convertAndSend(receiverEndPoint, resp);

            String senderEndPoint = "/ws/secured/messenger/user/" + sender;
            simpMessagingTemplate.convertAndSend(senderEndPoint,resp);
        }
    }
}
