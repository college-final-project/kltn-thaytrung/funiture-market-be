package com.hcmute.furnituremarketbe.controller.common;

import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.dto.request.*;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.entity.User;
import com.hcmute.furnituremarketbe.domain.aggregate.useraggregate.services.interfaces.UserService;
import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.*;

@RequestMapping("api/auth")
@RestController
public class AuthenticationController {
    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public ResponseBaseAbstract login(@RequestBody LoginRequest request, HttpServletResponse response) {
        return this.userService.login(request, response);
    }

    @PostMapping("/logout")
    public ResponseBaseAbstract logout(@RequestParam(required = false) String refreshTokenId, HttpServletRequest request) {
        return this.userService.logout(refreshTokenId, request);
    }

    @PostMapping("/refresh")
    public ResponseBaseAbstract refresh(@RequestParam(required = false) String refreshTokenId,
                                        HttpServletRequest request, HttpServletResponse response) {
        return this.userService.refresh(refreshTokenId, request, response);
    }

    @PostMapping("/create-admin")
    public ResponseBaseAbstract createAdminAccount(@RequestBody CreateAdminAccountRequest request) {
        return this.userService.createAdminAccount(request);
    }

    @PostMapping("/register-buyer")
    public ResponseBaseAbstract registerBuyer(@RequestBody RegisterBuyerRequest request) {
        return this.userService.registerBuyerAccount(request);
    }

    @PostMapping("/register-seller")
    public ResponseBaseAbstract registerSeller(@RequestBody RegisterSellerRequest request) {
        return this.userService.registerSellerAccount(request);
    }

    @PatchMapping("/email-confirmation")
    public ResponseBaseAbstract confirmUserEmail(@RequestBody EmailConfirmationRequest request) {
        return this.userService.confirmUserEmail(request);
    }

    @PatchMapping("/restore-password")
    public ResponseBaseAbstract restorePassword(@RequestBody EmailConfirmationRequest request) {
        return this.userService.restorePassword(request);
    }
}
