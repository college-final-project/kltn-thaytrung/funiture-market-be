package com.hcmute.furnituremarketbe.controller;

import com.hcmute.furnituremarketbe.domain.base.ResponseBaseAbstract;
import com.hcmute.furnituremarketbe.file.storage.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("api/upload")
public class UploadFileController {
    @Autowired
    private FileStorageService fileStorageService;

    @PostMapping("")
    public ResponseBaseAbstract uploadImage(@RequestPart("image")MultipartFile image) throws IOException {
        return this.fileStorageService.uploadImage(image);
    }
}
